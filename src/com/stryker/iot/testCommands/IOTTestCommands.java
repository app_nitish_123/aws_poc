package com.stryker.iot.testCommands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.poi.poifs.property.Parent;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.appirio.automation.commands.TestCommands;
import com.appirio.automation.configuration.Configuration;
import com.appirio.automation.configuration.ShareableDataManager;
import com.appirio.automation.exception.TestScriptException;
import com.appirio.automation.result.ScreenShot;
import com.google.common.collect.Ordering;
import com.steadystate.css.format.CSSFormat;

import bsh.EvalError;
import bsh.Interpreter;

/**
 * This class is used to capture all the custom commands developed for Stryker IOT application
 *
 */

public class IOTTestCommands extends TestCommands {

	private Configuration config;
	private static String testStep[];

	public static final String KEY_FILE = "./Password Encryption/public.key";
	public static final String AES = "AES";

	public Configuration getStrykerIOTConfig() {
		return config;
	}

	public void setStrykerIOTConfig(Configuration config) {
		this.config = config;
		TestCommands.setTestCommandsConfig(config);
	}

	/**
	 * Method to parse provided Product Group Loaner Information from String to
	 * HashMap with each Product Grp as key.
	 * 
	 * @param loanerInformation
	 *            Loaner information in the specific format. For eg,
	 *            Handpieces(21,4,0);Chargers(10,6,6)
	 * @return HashMap 
	 *            HashMap with each Product Grp as key		  
	 * @throws TestScriptException 
	 */
	private HashMap<String, HashMap<String, Integer>> parseLoanerInfo(String loanerInformation) throws TestScriptException {
		HashMap<String, HashMap<String, Integer>> allProductGroupLoanerData = new HashMap<String, HashMap<String, Integer>>();
		try{
		String[] inputData = loanerInformation.split(";");
		for (int dataCount = 0; dataCount < inputData.length; dataCount++) {

			String productGrpName = inputData[dataCount].substring(0, inputData[dataCount].indexOf("(")).toUpperCase();
			String expectedLoanerData = inputData[dataCount].substring(inputData[dataCount].indexOf("(") + 1,
					inputData[dataCount].indexOf(")"));
			String[] loanerInfo = expectedLoanerData.split(",");
			List<String> loanerInfoList = Arrays.asList(loanerInfo);
			HashMap<String, Integer> eachProductGrpLoanerData = new HashMap<String, Integer>();
			eachProductGrpLoanerData.put("Count", Integer.parseInt(loanerInfoList.get(0)));
			eachProductGrpLoanerData.put("Due", Integer.parseInt(loanerInfoList.get(1)));
			eachProductGrpLoanerData.
			put("Soon", Integer.parseInt(loanerInfoList.get(2)));

			allProductGroupLoanerData.put(productGrpName, eachProductGrpLoanerData);
		 }
		} catch (Exception e) {
			e.printStackTrace();
			throw new TestScriptException("Loaner information cannot be parsed because of an exception.");
		}
		
		return allProductGroupLoanerData;
	}

	/**
	 * Method to capture Device Graph Down Values and assign weight to it
	 * 
	 * @param graphDropDown
	 *            UI identifier for the drop down on the Device Usage graph.
	 * @return HashMap
	 *            Contains graph dropdown values
	 * @throws TestScriptException
	 */
	private HashMap<Integer, String> deviceGraphDropDownValues(String graphDropDown) throws TestScriptException {
		try {
			WebElement wElement = findElement(getLocator(graphDropDown));
			wElement.click();
			List<WebElement> dropdownValues = wElement.findElements(By.xpath("..//li"));
			HashMap<Integer, String> graphDropDownValues = new HashMap<Integer, String>();
			int weight = 0;
			for (WebElement dropdownValue : dropdownValues) {
				graphDropDownValues.put(weight, dropdownValue.getText());
				weight++;
			}

			wElement.click();
			return graphDropDownValues;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TestScriptException("Graph Drop Down values cannot be captured because of an exception.");
		}
	}

	/**
	 * Method to calculate the sum of bars for Uses Per Day GraphValues
	 * 
	 * @param element
	 *            UI identifier for the bar shown in the Device Usage Graph
	 * @return int
	 *            Sum of Usage per Day graph bar values
	 * @throws TestScriptException
	 */
	private int usesPerDayGraphValuesSum(String barIdentifier) throws TestScriptException {
		try {
			List<WebElement> graphBarValues = findElements(getLocator(barIdentifier));
			int sumOfValues = 0;
			for (WebElement graphBarValue : graphBarValues) {
				String exactValue = graphBarValue.getText();
				Double exactDoubleValue = Double.parseDouble(exactValue);
				sumOfValues = sumOfValues + exactDoubleValue.intValue();
			}
			return sumOfValues;
		} //Adding NoSuchElementException because there might be chance that graph will not appear due to 0 devices
		catch(NoSuchElementException nsee){
			return 0;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new TestScriptException("Graph values cannot be accessed because of an exception.");
		}

	}
	
	/**
	 * Method to calculate the sum of min and max value for Uses Per Device Graph Bars
	 * 
	 * @param minValueIdentifier
	 *            UI identifier for the min value shown in the Device Usage Graph
	 * @param maxValueIdentifier
	 *            UI identifier for the max value shown in the Device Usage Graph           
	 * @return int
	 *            Sum of Usage per Device graph values
	 * @throws TestScriptException
	 */
	private int usesPerDeviceGraphValuesSum(String minValueIdentifier, String maxValueIdentifier) throws TestScriptException {
		try {
			WebElement minElement = findElement(getLocator(minValueIdentifier));
			WebElement maxElement = findElement(getLocator(maxValueIdentifier));
			int sumOfValues = 0;
			String minValue = minElement.getText();
			Double minDoubleValue = Double.parseDouble(minValue);
			String maxValue = maxElement.getText();
			Double maxtDoubleValue = Double.parseDouble(maxValue);
			sumOfValues = minDoubleValue.intValue() + maxtDoubleValue.intValue();
			return sumOfValues;
		} //Adding NoSuchElementException because there might be chance that graph will not appear due to 0 devices
		catch(NoSuchElementException nsee){
			return 0;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new TestScriptException("Graph values cannot be accessed because of an exception.");
		}

	}	

	/**
	 * Method for selecting the value from the dropdown of the graph.
	 * 
	 * @param graphDropDown
	 *            UI identifier for the drop down on the Device Usage graph.
	 * @param weightForValueToBeSelected
	 *            Index for the dropdown value that needs to be selected for
	 *            dropdown on Device Usage graph.
	 * @throws TestScriptException
	 */

	private void usesPerDayGraphSelection(String graphDropDown, int weightForValueToBeSelected)
			throws TestScriptException {
		try{
			WebElement wElement = findElement(getLocator(graphDropDown));
			wElement.click();
			int dropDownValueToBeSelected = weightForValueToBeSelected + 1;
			waitExplicit(1000);
			WebElement newWeekSelection = wElement.findElement(By.xpath("..//li[" + dropDownValueToBeSelected + "]"));
			newWeekSelection.click();
			waitExplicit(2000);
		}catch(Exception e){
			e.printStackTrace();
			throw new TestScriptException("Graph values cannot be accessed because of an exception.");			
		}
	}

	/**
	 * Method to verify Uses Per Day Graph Functionality
	 * 
	 * @param usesPerDayGraphIdentifier
	 *            UI identifier for the usesPerDayGraph
	 * @param graphDropDown
	 *            UI identifier for the drop down on the Device Usage graph.
	 * @param graphBarValue
	 *            UI identifier for the bar shown in the Device Usage Graph
	 * @return String[]
	 *            Returns the result of the execution           
	 * @throws TestScriptException
	 */	
	
	public String[] verifyUsesPerDayGraph(String usesPerDayGraphIdentifier, String graphDropDown, String graphBarValue)
			throws TestScriptException {
		String [] testStep = getStepInstance();
		testStep[0] = "Verify that Uses per Day graph is working properly";
		testStep[1] = "Uses per Day graph should work properly";
		testStep[2] = "Uses per Day graph is working properly. Please refer screenshots for evidence";
		testStep[4] = null;
		String passScreenshots = null;
		String status = "Pass";
		int newSumOfValues1 = 0;
		int newSumOfValues2 = 0;
		int weightForSelectedWeek = -1;
		try {

			WebElement usesPerDayGraph = findElement(getLocator(usesPerDayGraphIdentifier));

			//Below we are saving default graph value first, so we can restore it to default again after validation
			//Below weightForSelectedWeek value represent the selected default graph value.
			HashMap<Integer, String> graphWeekSelector = deviceGraphDropDownValues(graphDropDown);
			String defautlWeekSelected = findElement(getLocator(graphDropDown)).getText().trim();
			for (Integer i : graphWeekSelector.keySet()) {
				String valueLoop = graphWeekSelector.get(i);

				if (valueLoop.equals(defautlWeekSelected)) {
					weightForSelectedWeek = i;
					break;
				}

			}
			//Select First option from graph drop down to select the first graph.
			usesPerDayGraphSelection(graphDropDown, 0);
			newSumOfValues1 = usesPerDayGraphValuesSum(graphBarValue);
			usesPerDayGraph = findElement(getLocator(usesPerDayGraphIdentifier));
			
			new Actions(driver).moveToElement(usesPerDayGraph).perform();
			passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						
			//Select Second option form graph drop down to select the Second graph.
			usesPerDayGraphSelection(graphDropDown, 1);
			newSumOfValues2 = usesPerDayGraphValuesSum(graphBarValue);
			usesPerDayGraph = findElement(getLocator(usesPerDayGraphIdentifier));
			
			new Actions(driver).moveToElement(usesPerDayGraph).perform();
			passScreenshots = passScreenshots + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						
			//Validation
			if(newSumOfValues1 == newSumOfValues2){
				testStep[2] = "Uses per graph is not functioning properly. Please refer screenshot folder for evidence.";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2];
				usesPerDayGraphSelection(graphDropDown,weightForSelectedWeek);
				throw new Exception();
			}
			
			//Selecting the graph to it default value
			usesPerDayGraphSelection(graphDropDown,weightForSelectedWeek);
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Uses per Day Graph cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Method to verify Uses Per Device Graph Functionality
	 * 
	 * @param usesPerDeviceGraphIdentifier
	 *            UI identifier for the usesPerDayGraph
	 * @param graphDropDown
	 *            UI identifier for the drop down on the Device Usage graph.
	 * @param minValueIdentifier
	 *            UI identifier for the min value shown in the Device Usage Graph
	 * @param maxValueIdentifier
	 *            UI identifier for the max value shown in the Device Usage Graph           
	 * @return String[]
	 *            Returns the result of the execution           
	 * @throws TestScriptException
	 */
	
	public String[] verifyUsesPerDeviceGraph(String usesPerDeviceGraphIdentifier, String graphDropDown, String minValueIdentifier, String maxValueIdentifier)
			throws TestScriptException {
		String [] testStep = getStepInstance();
		testStep[0] = "Verify that Uses per Device graph is working properly";
		testStep[1] = "Uses per Device graph should work properly";
		testStep[2] = "Uses per Device graph is working properly. Please refer screenshots for evidence";
		testStep[4] = null;
		String passScreenshots = null;
		String status = "Pass";
		int newSumOfValues1 = 0;
		int newSumOfValues2 = 0;
		int weightForSelectedWeek = -1;
		int lastDropDownValue = -1;
		try {

			WebElement usesPerDayGraph = findElement(getLocator(usesPerDeviceGraphIdentifier));

			//Below we are saving default graph value first, so we can restore it to default again after validation
			//Below weightForSelectedWeek value represent the selected default graph value.
			HashMap<Integer, String> graphWeekSelector = deviceGraphDropDownValues(graphDropDown);
			String defautlWeekSelected = findElement(getLocator(graphDropDown)).getText().trim();
			for (Integer i : graphWeekSelector.keySet()) {
				String valueLoop = graphWeekSelector.get(i);

				if (valueLoop.equals(defautlWeekSelected)) {
					weightForSelectedWeek = i;
					break;
				}

			}
			for(Integer i:graphWeekSelector.keySet()){
				lastDropDownValue++;
			}
			//Select First option from graph drop down to select the first graph.
			usesPerDayGraphSelection(graphDropDown, 0);
			
			newSumOfValues1 = usesPerDeviceGraphValuesSum(minValueIdentifier, maxValueIdentifier);
			usesPerDayGraph = findElement(getLocator(usesPerDeviceGraphIdentifier));
			
			new Actions(driver).moveToElement(usesPerDayGraph).perform();
			passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						
			//Select Last option form graph drop down to select the Second graph.
			usesPerDayGraphSelection(graphDropDown, lastDropDownValue);
			newSumOfValues2 = usesPerDeviceGraphValuesSum(minValueIdentifier, maxValueIdentifier);
			usesPerDayGraph = findElement(getLocator(usesPerDeviceGraphIdentifier));
			
			new Actions(driver).moveToElement(usesPerDayGraph).perform();
			passScreenshots = passScreenshots + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						
			//Validation
			if(newSumOfValues1 == newSumOfValues2){
				testStep[2] = "Uses per Device graph is not functioning properly. Please refer screenshot folder for evidence.";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2];
				usesPerDayGraphSelection(graphDropDown,weightForSelectedWeek);
				throw new Exception();
			}
			
			//Selecting the graph to it default value
			usesPerDayGraphSelection(graphDropDown,weightForSelectedWeek);
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Uses per Device Graph cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Method to select the value for app settings drop down.
	 * 
	 * @param dropDownValue
	 *            UI identifier for the drop down on the Device Usage graph.
	 * @param variable
	 *            Value of the app settings drop down value that is selected via
	 *            this method
	 * @throws TestScriptException
	 */
	public void appSettingsDropDownSelection(String dropDownValue, String variable) throws TestScriptException {
		String value = "";
		try {
			HashMap<Integer, String> graphWeekSelector = deviceGraphDropDownValues(dropDownValue);
			String defautlWeekSelected = findElement(getLocator(dropDownValue)).getText().trim();
			int weightForNewWeekToBeSelected = -1;
			int weightForSelectedWeek = -1;

			for (Integer i : graphWeekSelector.keySet()) {
				String valueLoop = graphWeekSelector.get(i);

				if (valueLoop.equals(defautlWeekSelected)) {
					weightForSelectedWeek = i;
					break;
				}
			}

			if (weightForSelectedWeek == graphWeekSelector.size() - 1) {
				weightForNewWeekToBeSelected = weightForSelectedWeek - 1;
				usesPerDayGraphSelection(dropDownValue, weightForNewWeekToBeSelected);
			} else {
				weightForNewWeekToBeSelected = weightForSelectedWeek + 1;
				usesPerDayGraphSelection(dropDownValue, weightForNewWeekToBeSelected);
			}
			value = graphWeekSelector.get(weightForNewWeekToBeSelected);
			config.setMapGlobalVariable(variable, value);
		} catch (Exception e) {
			throw new TestScriptException(e.getMessage());
		}
	}

	/**
	 * Method to login SEM Application as Stryker or Customer
	 * 
	 * @param username
	 *            Input for Username
	 * @param password
	 *            Input for password
	 * @param loginType
	 *            Stryker - Specify to login as Internal User Customer - Specify
	 *            to login as External User
	 * @param userType
	 *            Unused - Disclaimer will not appear Used - Disclaimer will
	 *            appear
	 * @return String[]
	 *            Returns the result of the execution  
	 * @throws TestScriptException
	 */
	public String[] semAppLogin(String username, String password, String loginType, String userType)
			throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that User is able to login SEM Application";
		testStep[1] = "Uses should be able to Login the SEM Application";
		testStep[2] = loginType + " user is logged in to the SEM Application";
		testStep[4] = null;
		String status = "Pass";
		String url = ShareableDataManager.getDataMap().get("URL");
		try {
			openUrl(url);
			if (loginType.equalsIgnoreCase("Stryker")) {
				waitForElementToBeVisible(60, "StrykerLoginButton");
				clickElement("StrykerLoginButton", "Button");
				waitExplicit(3000);
				if (isElementExists("//*[@id='otherTile']")) {
					clickElement("LoginAsAnotherUser", "Link");
					waitForElementToBeVisible(60, "MicrosoftLogin");
				}
				inputData("MicrosoftLogin", "Text", username);
				clickElement("MicrosoftNext", "Button");
				waitForElementToBeVisible(60, "MicrosoftPassword");
				inputData("MicrosoftPassword", "Text", password);
				clickElement("MicrosoftSignIn", "Button");
				waitForElementToBeVisible(60, "SFDC_TabContainer");
				clickElement("SFDC_CommunitySwitcher", "Link");
				waitForElementToBeVisible(60, "SFDC_SemCommunity");
				clickElement("SFDC_SemCommunity", "Link");
			} else if (loginType.equalsIgnoreCase("Customer")) {
				waitForElementToBeVisible(60, "CustomerLoginButton");
				clickElement("CustomerLoginButton", "Button");
				waitExplicit(3000);
				if (isElementExists("//*[@id='otherTile']")) {
					clickElement("LoginAsAnotherUser", "Link");
					waitForElementToBeVisible(60, "MicrosoftLogin");
				}
				inputData("MicrosoftLogin", "Text", username);
				clickElement("MicrosoftNext", "Button");
				waitForElementToBeVisible(60, "MicrosoftPassword");
				inputData("MicrosoftPassword", "Text", password);
				clickElement("MicrosoftSignIn", "Button");
			} else {
				testStep[2] = "Please enter correct Login Type (Stryker/Customer)";
				status = "Fail";
				methodFailureMessage = testStep[2];
				testStep[4] = "";
				throw new Exception();
			}
			if (userType.equalsIgnoreCase("Unused")) {
				waitForElementToBeVisible(60, "SEM_AckOKBtn");
			} else if (userType.equalsIgnoreCase("Used")) {
				waitForElementToBeVisible(60, "SEM_HomeLink");
				waitForElementToBeInvisible("SEM_Loading");
			} else {
				testStep[2] = "Please enter correct User Type (Used/Unused)";
				status = "Fail";
				methodFailureMessage = testStep[2];
				testStep[4] = "";
				throw new Exception();
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "User is unable to login the SEM Application because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to verify whether authenticated user is navigated to the bookmarked screen on clicking the Bookmarks on the browser
	 * 
	 * @param screenName
	 *            Input for ScreenName e.g.ProductGroupOverview, ProductType, ProductTypeSummary,Loaner, Device, MaintenanceCart, SearchResult, AboutVersionScreen, DisclaimerScreen, HelpScreen, SettingScreen, ContactScreen
	 * @param searchKey
	 *           Input for Device Id search
	 *           otherwise N, if not applicable
	 * @return String[]
	 *            Returns the result of the execution  
	 * @throws TestScriptException
	 */
	public String[] verifyBookmarksForAuthenticatedUser (String screenName,String searchKey) throws TestScriptException
	{
	String[] testStep = getStepInstance();
	testStep[0] = "Verify that user is navigated to the bookmarked screen with screen name as " + screenName + " on clicking the bookmarks on the browser";
	testStep[1] = "User should be redirected to the bookmarked screen "+ screenName;
	testStep[2] = "User is navigated to the bookmarked screen for screen name " + screenName;
	testStep[4] = null;
	String status = "Pass";
	
	if (searchKey.equals("N") && screenName.equalsIgnoreCase("SearchResult")) {
		searchKey = "100";
	}
	
	try 
	 {
			
	   if (screenName.equalsIgnoreCase("ProductGroupOverview"))
	   {	
		 String linkForProductGroupScreen= driver.getCurrentUrl();
		driver.navigate().to(linkForProductGroupScreen);
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGrpName");
		verifyPresence("SEM_ProductGrpName");
		}
		
	else if (screenName.equalsIgnoreCase("ProductType") || screenName.equalsIgnoreCase("ProductTypeSummary")) 
	{
		clickElement("SEM_ProductGroupLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "Pg_Name");
		String linkForProductGroupTypeScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForProductGroupTypeScreen);
		waitForElementToBeVisible(60, "Pg_Name");
		verifyPresence("Pg_Name");
	
		if (screenName.equalsIgnoreCase("ProductTypeSummary")) {
			clickElement("Pg_Name", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "Pt_Name");
			String linkForProductGroupTypeSummaryScreen= driver.getCurrentUrl();
			clickElement("SEM_HomeLink", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
			waitExplicit(3000);
			driver.navigate().to(linkForProductGroupTypeSummaryScreen);
			verifyPresence("Pt_Name");
		}
		}
	
	else if (screenName.equalsIgnoreCase("Loaner")) {
		waitForElementToBeVisible(60, "Pg_LoanerLabel");
		clickElement("Pg_LoanerLabel", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "LoanerSerialNoValue");
		String linkForLoanerScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForLoanerScreen);
		verifyPresence("LoanerSerialNoValue");
		} 
	else if (screenName.equalsIgnoreCase("Device")) {
		waitForElementToBeVisible(60, "ListViewLink");
		clickElement("ListViewLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SerialNoValue");
		String linkForDeviceListScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForDeviceListScreen);
		verifyPresence("SerialNoValue");
	}
	else if (screenName.equalsIgnoreCase("MaintenanceCart")) {
		waitForElementToBeVisible(60, "ListViewLink");
		if (!isElementExists("//*[@id='Home_CartIcon_Active']")) {
			clickElement("ListViewLink", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "FirstCartEnabled");
			clickElement("FirstCartEnabled", "Link");
			waitForElementToBeVisible(60, "AMC_RecommBtn");
			clickElement("AMC_RecommBtn", "Link");
			waitForElementToBeVisible(60, "SEM_MaintCartIconActive");
	}
		clickElement("SEM_MaintCartIconActive", "Link");
		waitForElementToBeVisible(60, "MaintCartSubmitBtn");
		
		String linkForMaintenanceCartSubmitRequestScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForMaintenanceCartSubmitRequestScreen);
		waitForElementToBeVisible(60, "MaintCart_PageLabel");
		verifyPresence("MaintCartSubmitBtn");
	} 
	else if (screenName.equalsIgnoreCase("SearchResult")) {
		waitForElementToBeVisible(60, "SEM_SearchBox");
		inputData("SEM_SearchBox", "Text", searchKey);
		clickElement("SEM_SearchBoxIcon", "Button");
		waitForElementToBeVisible(60, "DeviceSearchResultLabel");
		String linkForSearchResultScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForSearchResultScreen);
		waitForElementToBeVisible(60, "DeviceSearchResultLabel");
		verifyPresence("DeviceSearchResultLabel");
	}
	else if (screenName.equalsIgnoreCase("AboutVersionScreen")) {
		waitForElementToBeVisible(60, "SEM_About");
		clickElement("SEM_About", "Link");
		waitForElementToBeVisible(60, "AboutVersionLabel");
		String linkForAboutSEMScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForAboutSEMScreen);
		verifyPresence("AboutVersionLabel");
	}
	else if (screenName.equalsIgnoreCase("DisclaimerScreen")) {
		waitForElementToBeVisible(60, "SEM_Disclaimer");
		clickElement("SEM_Disclaimer", "Link");
		waitForElementToBeVisible(60, "DisclaimerLabel");
		String linkForDisclaimerSEMScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForDisclaimerSEMScreen);
		verifyPresence("DisclaimerLabel");
	}
	else if (screenName.equalsIgnoreCase("HelpScreen")) {
		waitForElementToBeVisible(60, "SEM_Help");
		clickElement("SEM_Help", "Link");
		waitForElementToBeVisible(60, "HelpLabel");
		String linkForHelpSEMScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForHelpSEMScreen);
		verifyPresence("HelpLabel");
	}
	else if (screenName.equalsIgnoreCase("SettingScreen")) {
		waitForElementToBeVisible(60, "SEM_Setting");
		clickElement("SEM_Setting", "Link");
		waitForElementToBeVisible(60, "SettingLabel");
		String linkForSettingSEMScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForSettingSEMScreen);
		verifyPresence("SettingLabel");
	}
	else if (screenName.equalsIgnoreCase("ContactScreen")) {
		waitForElementToBeVisible(60, "SEM_Contact");
		clickElement("SEM_Contact", "Link");
		waitForElementToBeVisible(60, "ContactLabel");
		String linkForContactSEMScreen= driver.getCurrentUrl();
		clickElement("SEM_HomeLink", "Link");
		waitForElementToBeInvisible("SEM_Loading");
		waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
		waitExplicit(3000);
		driver.navigate().to(linkForContactSEMScreen);
		verifyPresence("ContactLabel");
	}
	else {
		testStep[2] = "Please enter correct screen name";
		status = "Fail";
		methodFailureMessage = testStep[2];
		testStep[4] = "";
		throw new Exception();
	}
	testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
	}
	catch (Exception e) {
		if (testStep[4] == null) {
			testStep[2] =screenName+ " could not be selected because of some exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		}
		}
		 finally {
				testStep[3] = status;
		 }
	if (status.equals("Fail")) {
		config.getLogger().error(methodFailureMessage);
		throw new TestScriptException(methodFailureMessage, testStep);
	}
				
		return testStep;
	}
	
	/**
	 * Method to verify whether unauthenticated user is navigated to the Splash screen on clicking the Bookmarks on the browser
	 * 
	 * @param screenName
     *          Input for ScreenName e.g.ProductGroupOverview, ProductType, ProductTypeSummary,Loaner, Device, MaintenanceCart, SearchResult, AboutVersionScreen, DisclaimerScreen, HelpScreen, SettingScreen, ContactScreen
	 * @param Link
	 *           Input for the bookmarked link for respective screens
	 * @return String[]
	 *            Returns the result of the execution  
	 * @throws TestScriptException
	 */
	
	public String[] verifyBookmarksforUnAuthenticatedUser(String screenName, String Link) throws TestScriptException
	{String[] testStep = getStepInstance();
	testStep[0] = "Verify that unauthenticated user is landed to the splash screen on hitting bookmarks on the browser for screen "+ screenName;
	testStep[1] = "Unauthenticated user should be landed to Splash screen on hitting bookmarks for screen "+ screenName;
	testStep[2] = "Unauthenticated user is landed to Splash screen on hitting bookmarks for screen "+ screenName;
	testStep[4] = null;
	String status = "Pass";
		try
		{
			if (screenName.equalsIgnoreCase("ProductGroupOverview") || screenName.equalsIgnoreCase("ProductType") || screenName.equalsIgnoreCase("ProductTypeSummary") || screenName.equalsIgnoreCase("Loaner")|| screenName.equalsIgnoreCase("Device")|| screenName.equalsIgnoreCase("MaintenanceCart")|| screenName.equalsIgnoreCase("SearchResult")|| screenName.equalsIgnoreCase("AboutVersionScreen")|| screenName.equalsIgnoreCase("DisclaimerScreen")|| screenName.equalsIgnoreCase("HelpScreen")|| screenName.equalsIgnoreCase("SettingScreen")|| screenName.equalsIgnoreCase("ContactScreen")) {
				openURLInTab(Link);
				waitForElementToBeVisible(60, "StrykerLoginButton");
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
			
			else {
				testStep[2] = "Please enter correct screen name";
				status = "Fail";
				methodFailureMessage = testStep[2];
				testStep[4] = "";
				throw new Exception();
			}
		}
		
		catch (Exception e)
		{
			if (testStep[4] == null) 
			{
				testStep[2] = screenName+ " could not be selected because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		}
		 finally {
				testStep[3] = status;
		        }
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
			
		return testStep;
	}

	/**
	 * Method to select Facility in SEM Application
	 * 
	 * @param facilityName
	 *            Name of Facility to be selected Variable can contain more than
	 *            one Facility name All - If User want to select all the
	 *            facility
	 * @param selectType
	 *            Default - Select Facility using Scroll & 
	 *            Select Search - Select Facility using Search & Select 
	 *            Deselect - Deselect the previous Facility and select the mentioned Facility
	 * @return String[]
	 *            Returns the result of the execution 
	 * @throws TestScriptException
	 */
	public String[] selectFacility(String facilityName, String selectType) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that " + facilityName + " is selected";
		testStep[1] = facilityName + " should be selected";
		testStep[2] = facilityName + " is selected";
		testStep[4] = null;
		String status = "Pass";
		String[] facility = facilityName.split(",");
		try {
			if (selectType.equalsIgnoreCase("Search") && facility.length > 0) {
				testStep[2] = "Please enter only one Facility name, if you want to Search & Select";
				status = "Fail";
				methodFailureMessage = testStep[2];
				testStep[4] = "";
				throw new Exception();
			} else if (facilityName.equalsIgnoreCase("All") && !selectType.equalsIgnoreCase("Default")) {
				testStep[2] = "Select Type should be Default for Selecting all the Facilities";
				status = "Fail";
				methodFailureMessage = testStep[2];
				testStep[4] = "";
				throw new Exception();
			}
			clickElement("SEM_FacilitySelection", "Button");
			waitForElementToBeVisible(60, "SearchFacility");

			if (facilityName.equalsIgnoreCase("All")) {
				if (isElementExists("//*[@id='LocModal_UnselectAll_Label' and not(contains(@class,'hide'))]")) {
					clickElement("FacilityUnselect", "Button");
				}
				waitForElementToBeVisible(60, "FacilitySelectAll");
				clickElement("FacilitySelectAll", "Button");
				waitForElementToBeVisible(60, "FacilityUnselect");
			} else if (selectType.equalsIgnoreCase("Search")) {
				config.setMapGlobalVariable("FacilityName", facilityName);
				inputData("SearchFacility", "Text", facilityName);
				clickElement("SearchFacilityIcon", "Button");
				waitForElementToBeVisible(60, "FacilityToBeSelected");
				clickElement("FacilityToBeSelected", "Link");
			} else if (selectType.equalsIgnoreCase("Deselect") || selectType.equalsIgnoreCase("Default")) {
				if (selectType.equalsIgnoreCase("Deselect")) {
					if (isElementExists("//*[@id='LocModal_SelectAll_Label' and not(contains(@class,'hide'))]")) {
						clickElement("FacilitySelectAll", "Button");
					}
					waitForElementToBeVisible(60, "FacilityUnselect");
					clickElement("FacilityUnselect", "Button");
					waitForElementToBeVisible(60, "FacilitySelectAll");
				}

				for (int i = 0; i < facility.length; i++) {
					config.setMapGlobalVariable("FacilityName", facility[i]);
					waitForElementToBeVisible(60, "FacilityToBeSelected");
					mouseHover("FacilityToBeSelected");

					By webEle = getLocator("FacilityToBeSelected");
					WebElement webElement = findElement(webEle);
					mouseHover("FacilityToBeSelected");

					String attval = webElement.getAttribute("class");
					if (!attval.contains("unSelected")) {
						testStep[2] = facilityName + " is already selected for the logged in User";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}

					clickElement("FacilityToBeSelected", "Link");

					webEle = getLocator("FacilityToBeSelected");
					webElement = findElement(webEle);
					mouseHover("FacilityToBeSelected");
					attval = webElement.getAttribute("class");
					if (attval.contains("unSelected")) {
						testStep[2] = facilityName + " is not selected for the logged in User";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
				}
			} else {
				testStep[2] = "Please enter correct Select Type";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			clickElement("FacilitySaveBtn", "Button");
			waitForElementToBeInvisible("FacilitySaveBtn");
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = facilityName + " could not be selected because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to navigate to different screens in SEM Application
	 * 
	 * @param screenName
	 *            ProductGroup - Navigate to Product Group Overview Screen.
	 *            ProductType - Navigate to Product Type Overview Screen.
	 *            ProductTypeSummary - Navigate to Product Type Summary Screen.
	 *            Loaner - Navigate to Loaner List Screen. 
	 *            Device - Navigate to  Device List Screen.
	 *            MaintenanceCart - Navigate to Maintenance Cart Screen. 
	 *            SearchResult - Navigate to Search Results Screen.
	 *            DeviceSummary - Navigate to Device Summary Screen.
	 *            LoanerDeviceSummary - Navigate to Device Summary Screen for Loaner Device
	 * @param productGrp
	 *            Product Group Name
	 * @param productType
	 *            Product Type Name
	 * @param searchKey
	 *            Text User want to search in Search Box N - if User don't want
	 *            to Search
	 * @return String[]
	 *            Returns the result of the execution              
	 * @throws TestScriptException
	 */
	public String[] navigateToScreen(String screenName, String productGrp, String productType, String searchKey)
			throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that User is navigated to " + screenName + " Screen";
		testStep[1] = "User should be navigated to " + screenName + " Screen";
		testStep[2] = "User is navigated to " + screenName + " Screen";
		testStep[4] = null;
		String status = "Pass";
		if (searchKey.equals("N") && screenName.equalsIgnoreCase("SearchResult")) {
			searchKey = "100";
		}
		config.setMapGlobalVariable("ProductGroup", productGrp);
		config.setMapGlobalVariable("ProductType", productType);
		waitExplicit(1000);			
		try {
			clickElement("SEM_HomeLink", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
			if (screenName.equalsIgnoreCase("ProductType") || screenName.equalsIgnoreCase("ProductTypeSummary")) {
				clickElement("SEM_ProductGroupLink", "Link");
				waitForElementToBeInvisible("SEM_Loading");
				waitForElementToBeVisible(60, "Pg_Name");
				if (screenName.equalsIgnoreCase("ProductTypeSummary")) {
					clickElement("Pg_Name", "Link");
					waitForElementToBeInvisible("SEM_Loading");
					waitForElementToBeVisible(60, "Pt_Name");
				}
			} else if (screenName.equalsIgnoreCase("Loaner")) {
				waitForElementToBeVisible(60, "Pg_LoanerLabel");
				clickElement("Pg_LoanerLabel", "Link");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
				waitForElementToBeInvisible("SEM_Loading");
			} else if (screenName.equalsIgnoreCase("Device")) {
				waitForElementToBeVisible(60, "ListViewLink");
				clickElement("ListViewLink", "Link");
				waitForElementToBeVisible(60, "SerialNoValue");
				waitForElementToBeInvisible("SEM_Loading");
			} else if (screenName.equalsIgnoreCase("MaintenanceCart")) {
				waitForElementToBeVisible(60, "ListViewLink");
				if (!isElementExists("//*[@id='Home_CartIcon_Active']")) {
					clickElement("ListViewLink", "Link");
					waitForElementToBeVisible(60, "FirstCartEnabled");
					waitForElementToBeInvisible("SEM_Loading");
					clickElement("FirstCartEnabled", "Link");
					waitForElementToBeVisible(60, "AMC_RecommBtn");
					clickElement("AMC_RecommBtn", "Link");
					waitForElementToBeVisible(60, "SEM_MaintCartIconActive");
				}
				clickElement("SEM_MaintCartIconActive", "Link");
				waitIfSpinnerDisplays();
			
			waitForElementToBeVisible(60, "MaintCartSubmitBtn");
			} else if (screenName.equalsIgnoreCase("SearchResult")) {
				waitForElementToBeVisible(60, "SEM_SearchBox");
				inputData("SEM_SearchBox", "Text", searchKey);
				clickElement("SEM_SearchBoxIcon", "Button");
				waitForElementToBeVisible(60, "DeviceSearchResultLabel");
			} else if (screenName.equalsIgnoreCase("DeviceSummary")) {
				waitForElementToBeVisible(60, "SEM_SearchBox");
				inputData("SEM_SearchBox", "Text", searchKey);
				waitForElementToBeVisible(60, "DS_DeviceIcon");
			} else if (screenName.equalsIgnoreCase("LoanerDeviceSummary")) {
				waitForElementToBeVisible(60, "SEM_SearchBox");
				inputData("SEM_SearchBox", "Text", searchKey);
				waitForElementToBeVisible(60, "DS_LoanerDeviceIcon");
			} else if (!screenName.equalsIgnoreCase("ProductGroup")) {
				testStep[2] = "Please enter correct Screen Name";
				status = "Fail";
				methodFailureMessage = testStep[2];
				testStep[4] = "";
				throw new Exception();
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "User is not navigated to " + screenName + " Screen because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verify that the Facility is selected or not
	 * 
	 * @param facilityName
	 *            Name of Facility that is already selected
	 * @return String[]
	 *            Returns the result of the execution     
	 * @throws TestScriptException
	 */
	public String[] verifyFacilityIsSelected(String facilityName) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that " + facilityName + " is selected for the logged in User";
		testStep[1] = facilityName + " should be selected for the logged in User";
		testStep[2] = facilityName + " is selected for the logged in User";
		testStep[4] = null;
		String passScreenshots = null;
		String failScreenshots = null;
		String status = "Pass";
		String[] facilitiesToBeSelected = facilityName.split(",");
		List<String> facilitiesNotSelected = new ArrayList<String>();
		try {
			waitForElementToBeVisible(60, "SEM_FacilitySelection");
			clickElement("SEM_FacilitySelection", "Link");
			waitForElementToBeVisible(60, "SearchFacility");

			for (String facName : facilitiesToBeSelected) {
				inputData("SearchFacility", "Text", facName);
				clickElement("SearchFacilityIcon", "Link");
				config.setMapGlobalVariable("FacilityName", facName);
				waitForElementToBeVisible(60, "FacilityToBeSelected");

				By webEle = getLocator("FacilityToBeSelected");
				WebElement webElement = findElement(webEle);
				mouseHover("FacilityToBeSelected");
				String attval = webElement.getAttribute("class");
				if (attval.contains("unSelected")) {
					facilitiesNotSelected.add(facName);
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				} else {
					if (passScreenshots == null) {
						passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						passScreenshots = passScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}
			}
			if (facilitiesNotSelected.size() > 0) {
				testStep[2] = facilitiesNotSelected.toString() + " is not selected for the logged in User";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
			clickElement("FacilityCancelBtn", "Button");
			waitForElementToBeInvisible("FacilityCancelBtn");
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification cannot be done because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/** Method to decrypt the password used for login to SEMAPP
	 * @param ciphertext
	 * 		  Encrypted text that needs to be decrypted
	 * @return String
	 * 		Returns the decrypted password.
	 * @throws TestScriptException 
	 */
	private static String decrypt(String ciphertext) throws TestScriptException {
		String OriginalPassword = null;
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(KEY_FILE));
			final String tempkey = (String) inputStream.readObject();
			byte[] bytekey = hexStringToByteArray(tempkey);
			SecretKeySpec sks = new SecretKeySpec(bytekey, AES);
			Cipher cipher = Cipher.getInstance(AES);
			cipher.init(Cipher.DECRYPT_MODE, sks);
			byte[] decrypted = cipher.doFinal(hexStringToByteArray(ciphertext));
			OriginalPassword = new String(decrypted);
		} catch (Exception e) {
			throw new TestScriptException(e.getMessage());
		}
		return OriginalPassword;
	}

	/** Method to convert the byte array to hex string
	 * @param b
	 * 		  Byte array to be converted
	 * @return String
	 * 		  Converted hex string
	 * @throws TestScriptException 
	 */
	private static String byteArrayToHexString(byte[] b) throws TestScriptException {
		StringBuffer sb = new StringBuffer(b.length * 2);
		try {
		for (int i = 0; i < b.length; i++) {
			int v = b[i] & 0xff;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		} catch (Exception e) {
			throw new TestScriptException(e.getMessage());
		}
		return sb.toString().toUpperCase();
	}

	/** Method to convert the byte array to hex string
	 * @param s
	 * 		  Hex String to be converted
	 * @return byte[]
	 * 		  Converted byte[]
	 * @throws TestScriptException 
	 */
	private static byte[] hexStringToByteArray(String s) throws TestScriptException {
		byte[] b = new byte[s.length() / 2];
		try {
		for (int i = 0; i < b.length; i++) {
			int index = i * 2;
			int v = Integer.parseInt(s.substring(index, index + 2), 16);
			b[i] = (byte) v;
		}
		}catch (Exception e) {
			throw new TestScriptException(e.getMessage());
		}
		return b;
	}

	/**
	 * Verify that the Organization and its Facilities are in Hierarchy on
	 * Customer Selection Screen
	 * 
	 * @param orgName
	 *            Name of Organization
	 * @param facilityName
	 *            Name of Facilities under the above mentioned Organization
	 * @return String[]
	 *            Returns the result of the execution 
	 * @throws TestScriptException
	 */
	public String[] verifyCustomerSelectionHierarchy(String orgName, String facilityName) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the " + orgName + " and " + facilityName + " have hierarchical structure";
		testStep[1] = orgName + " and " + facilityName + " should have hierarchical structure";
		testStep[2] = orgName + " and " + facilityName + " have hierarchical structure";
		String status = "Pass";
		String[] facility = facilityName.split(",");
		try {
			config.setMapGlobalVariable("OrgName", orgName);
			if (!isElementExists("//*[@id='LocModal_SearchIcon']")) {
				clickElement("SEM_HomeLink", "Link");
				waitForElementToBeVisible(60, "SEM_FacilitySelection");
				clickElement("SEM_FacilitySelection", "Link");
				waitForElementToBeVisible(60, "SearchFacility");
			}

			for (int i = 0; i < facility.length; i++) {
				config.setMapGlobalVariable("FacilityName", facility[i]);
				verifyPresence("HierarchyTest");
			}

			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			clickElement("FacilityCancelBtn", "Button");
			waitForElementToBeInvisible("FacilityCancelBtn");
		} catch (Exception e) {
			testStep[2] = orgName + " and " + facilityName + " don't have hierarchical structure. Please Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verify that the Organizations are sorted
	 * 
	 * @param orgsName
	 *            Name of all the Organization that the User can access
	 * @param sortingOrder
	 *            A-Z for ascending order Z-A for descending order
	 * @param orgName
	 *            Name of Organization
	 * @param facilityName
	 *            Name of Facilities under the above mentioned Organization
	 * @return String[]
	 *            Returns the result of the execution            
	 * @throws TestScriptException
	 */
	public String[] verifyCustomerSelectionSorting(String orgsName, String sortingOrder, String orgName,
			String facilityName) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that:- " + orgsName + " are sorted within their hierarchical structure";
		testStep[1] = orgsName + " should be sorted within their hierarchical structure";
		testStep[2] = orgsName + " are sorted within their hierarchical structure";
		String status = "Pass";
		testStep[4] = null;
		final String xpath = "(//*[@id='LocModal_OrgName'])";
		String temp = "";
		String[] org = orgsName.split(",");
		for (int i = 0; i < org.length; i++)
			org[i] = org[i].toLowerCase();
		try {
			clickElement("SEM_HomeLink", "Link");
			waitForElementToBeVisible(60, "SEM_FacilitySelection");
			clickElement("SEM_FacilitySelection", "Link");

			if (sortingOrder.equals("A-Z") || sortingOrder.equals("Z-A")) {
				config.setMapGlobalVariable("SortOrder", "Sort " + sortingOrder);
				if (sortingOrder.equals("A-Z")) {
					Arrays.sort(org);
					try {
						verifyElementContainsText("SortingOrderBtn", sortingOrder);
					} catch (Exception e) {
						testStep[2] = "Default Sorting order is not A-Z";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2] + " " + e.getMessage();
						throw new Exception();
					}
				} else {
					Arrays.sort(org, Collections.reverseOrder());
					waitForElementToBeVisible(60, "SortingOrderBtn");
					clickElement("SortingOrderBtn", "Link");
					waitForElementToBeVisible(60, "SortValue");
					clickElement("SortValue", "Link");
					waitForElementToBeInvisible("SortValue");
				}

				WebElement webElement = null;
				for (int i = 1; i <= org.length; i++) {
					temp = xpath + "[" + i + "]";
					try {
						webElement = findElement(By.xpath(temp));
					} catch (Exception e) {
						testStep[2] = "User don't have access to " + org[i - 1]
								+ " Organization, Please verify all the Organizations & try again";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2] + " " + e.getMessage();
						throw new Exception();
					}
					Actions action = new Actions(driver);
					action.moveToElement(webElement).build().perform();
					String attval = webElement.getAttribute("title");
					attval = attval.toLowerCase();
					if (!attval.equals(org[i - 1])) {
						testStep[2] = orgsName + " are not sorted A-Z within their hierarchical structure";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
				}
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				try {
					verifyCustomerSelectionHierarchy(orgName, facilityName);
				} catch (Exception e) {
					testStep[2] = orgName + " and " + facilityName + " don't have hierarchical structure";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2] + " " + e.getMessage();
					throw new Exception();
				}
			} else {
				testStep[2] = "Please enter correct sorting order (A-Z or Z-A)";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Sorting cannot be verified because of an Exception. Please refer Logs/info.log for more details. ";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verify the following:- 
	 * If an Organization is selected then all its Facilities gets selected 
	 * If an Organization is deselected then all its Facilities gets deselected
	 * 
	 * @param orgName
	 *            Name of Organization
	 * @param allFacility
	 *            Name of all the Facilities under the above mentioned
	 *            Organization
	 * @param action
	 *            Select - It selects the Organization Deselect - It deselects
	 *            the Organization
	 * @return String[]
	 *            Returns the result of the execution   
	 * @throws TestScriptException
	 */
	public String[] verifyOrgHierarchySelection(String orgName, String allFacility, String action)
			throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that if " + orgName + " is " + action + "ed then " + allFacility + " are " + action
				+ "ed";
		testStep[1] = orgName + " is " + action + "ed then " + allFacility + " should be " + action + "ed";
		testStep[2] = orgName + " is " + action + "ed then " + allFacility + " are " + action + "ed";
		testStep[4] = null;
		String status = "Pass";
		String[] facility = allFacility.split(",");
		try {
			if (action.equalsIgnoreCase("Select") || action.equalsIgnoreCase("Deselect")) {
				config.setMapGlobalVariable("OrgName", orgName);
				waitForElementToBeVisible(60, "SEM_FacilitySelection");
				clickElement("SEM_FacilitySelection", "Link");
				waitForElementToBeVisible(60, "FacilitySelectAll");
				clickElement("FacilitySelectAll", "Button");
				waitForElementToBeVisible(60, "FacilityUnselect");
				clickElement("FacilityUnselect", "Button");
				waitForElementToBeVisible(60, "FacilitySelectAll");

				waitForElementToBeVisible(60, "OrgToBeSelected");
				mouseHover("OrgToBeSelected");
				clickElement("OrgToBeSelected", "Link");
				By webEle = getLocator("OrgToBeSelected");
				WebElement webElement = findElement(webEle);
				mouseHover("OrgToBeSelected");

				String attval = webElement.getAttribute("class");
				if (attval.contains("unSelected")) {
					testStep[2] = "Unable to select the Organization - " + orgName;
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				for (int i = 0; i < facility.length; i++) {
					config.setMapGlobalVariable("FacilityName", facility[i]);
					waitForElementToBeVisible(60, "FacilityToBeSelected");
					mouseHover("FacilityToBeSelected");

					webEle = getLocator("FacilityToBeSelected");
					webElement = findElement(webEle);
					mouseHover("FacilityToBeSelected");

					attval = webElement.getAttribute("class");
					if (attval.contains("unSelected")) {
						testStep[2] = facility[i] + " is not selected";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
				}
				if (action.equalsIgnoreCase("Deselect")) {
					mouseHover("OrgToBeSelected");
					clickElement("OrgToBeSelected", "Link");
					webEle = getLocator("OrgToBeSelected");
					webElement = findElement(webEle);
					mouseHover("OrgToBeSelected");

					attval = webElement.getAttribute("class");
					if (!attval.contains("unSelected")) {
						testStep[2] = "Unable to deselect the Organization - " + orgName;
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}

					for (int i = 0; i < facility.length; i++) {
						config.setMapGlobalVariable("FacilityName", facility[i]);
						waitForElementToBeVisible(60, "FacilityToBeSelected");
						mouseHover("FacilityToBeSelected");

						webEle = getLocator("FacilityToBeSelected");
						webElement = findElement(webEle);
						mouseHover("FacilityToBeSelected");

						attval = webElement.getAttribute("class");
						if (!attval.contains("unSelected")) {
							testStep[2] = facility[i] + " is selected";
							status = "Fail";
							testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
							methodFailureMessage = testStep[2];
							throw new Exception();
						}
					}
				}
			} else {
				testStep[2] = "Please select the correct action (Select/Deselect)";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			clickElement("FacilityCancelBtn", "Button");
			waitForElementToBeInvisible("FacilityCancelBtn");
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = orgName + " is " + action + "ed then " + allFacility + " are not " + action + "ed. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verify that the Customer Selection Screen is able to accommodate names
	 * that are longer than the available screen space
	 * 
	 * @param orgName
	 *            Name of Organization
	 * @return String[]
	 *            Returns the result of the execution 
	 * @throws TestScriptException
	 */
	public String[] verifyOrgNameOverflow(String orgName) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the Customer Selection Screen can accommodate " + orgName;
		testStep[1] = "The Customer Selection Screen should be able to accommodate " + orgName;
		testStep[2] = "The Customer Selection Screen can accommodate " + orgName;
		testStep[4] = null;
		String passScreenshots = null;
		String failScreenshots = null;
		String status = "Pass";
		List<String> orgNameNotOverflow = new ArrayList<String>();
		String[] org = orgName.split(",");
		try {
			if (!isElementExists("//*[@id='LocModal_SearchIcon']")) {
				clickElement("SEM_HomeLink", "Link");
				waitForElementToBeVisible(60, "SEM_FacilitySelection");
				clickElement("SEM_FacilitySelection", "Link");
				waitForElementToBeVisible(60, "SearchFacility");
			}

			for (int i = 0; i < org.length; i++) {
				config.setMapGlobalVariable("OrgName", org[i]);
				waitForElementToBeVisible(60, "OrgName");
				By webEle = getLocator("OrgName");
				WebElement webElement = findElement(webEle);
				String txtoverflow = webElement.getCssValue("text-overflow");
				String overflow = webElement.getCssValue("overflow");
				mouseHover("OrgName");
				if (!(txtoverflow.equalsIgnoreCase("ellipsis") && overflow.equalsIgnoreCase("hidden"))) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
					orgNameNotOverflow.add(org[i]);
				} else {
					if (passScreenshots == null) {
						passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						passScreenshots = passScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}

				if (orgNameNotOverflow.size() > 0) {
					testStep[2] = "The Customer Selection Screen cannot accommodate " + orgNameNotOverflow.toString();
					status = "Fail";
					testStep[4] = failScreenshots;
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}
			
			testStep[4] = passScreenshots;
			clickElement("FacilityCancelBtn", "Button");
			waitForElementToBeInvisible("FacilityCancelBtn");
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification that the Customer Selection Screen is able to accommodate names that are longer than the available screen space cannot be done because of an Exception.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to validate that each Product Group have Name and Image
	 * 
	 * @param productGroupTile
	 *            - UI identifier for Product Group Identifier
	 * @param productGroupName
	 *            - UI identifier for Product Group Name
	 * @param productGroupImage
	 *            - UI identifier for Product Group Image
	 * @return String[]
	 *            Returns the result of the execution    
	 * @throws TestScriptException
	 */
	public String[] verifyProductGrpNameImage() throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the all the product group have name and image";
		testStep[1] = "Displayed product group should have name and image";
		testStep[2] = "Displayed product group have name and image";
		testStep[4] = null;
		String status = "Pass";
		String passScreenshots = null;
		String imgFailScreenshots = null;
		String nameFailScreenshots = null;
		boolean ProductGrpNoName = false;
		List<String> ProductGrpNoImg = new ArrayList<String>();
		try {
			waitForElementToBeVisible(60, "SEM_ProductGrpTile");
			List<WebElement> productGroupTiles = findElements(getLocator("SEM_ProductGrpTile"));
			for (WebElement wElement : productGroupTiles) {
				WebElement productGrpName = wElement.findElement(getLocator("SEM_ProductGrpName"));
				WebElement productGrpImage = wElement.findElement(getLocator("SEM_ProductGrpImg"));

				if (!productGrpName.getText().isEmpty() && productGrpImage.getAttribute("src").toLowerCase()
						.contains(productGrpName.getText().toLowerCase())) {

					new Actions(driver).moveToElement(productGrpName).perform();
					
					if (passScreenshots == null) {
						passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						passScreenshots = passScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				} else {
					new Actions(driver).moveToElement(wElement).perform();
					if (productGrpName.getText().isEmpty()) {
						ProductGrpNoName = true;
						if (nameFailScreenshots == null) {
							nameFailScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							nameFailScreenshots = nameFailScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
					} else {
						ProductGrpNoImg.add(productGrpName.getText());
						if (imgFailScreenshots == null) {
							imgFailScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							imgFailScreenshots = imgFailScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
					}
				}
			}

			if (ProductGrpNoName && ProductGrpNoImg.size() > 0) {
				testStep[2] = "Please check the Products with no Image and Name";
				status = "Fail";
				testStep[4] = imgFailScreenshots + "," + nameFailScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (ProductGrpNoName) {
				testStep[2] = "Please check the Products with no Name";
				status = "Fail";
				testStep[4] = nameFailScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (ProductGrpNoImg.size() > 0) {
				testStep[2] = "Please check the Products with no Image for " + ProductGrpNoImg.toString();
				status = "Fail";
				testStep[4] = imgFailScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification that the all the product group have name and image cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to validate the Product Group Access as per the Device Population
	 * 
	 * @param productGroups
	 *            - List of product groups that should be accessible.
	 * @return String[]
	 *            Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyPopulatedProductGroups(String productGroups) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that application displays all product group associated with device population";
		testStep[1] = "Application should display all product group associated with device population";
		testStep[2] = "Application displays all product group associated with device population";
		String status = "Pass";
		String extraProductGrpScreenshots = null;
		String missingProductGrpScreenshots = null;
		productGroups = productGroups.toUpperCase();
		String[] expectedproductGrp = productGroups.split(",");
		List<String> displayedProductGrp = new ArrayList<String>();
		try {
			List<WebElement> productGroupTiles = findElements(getLocator("SEM_ProductGrpTile"));
			for (WebElement wElement : productGroupTiles) {
				WebElement productGrpName = wElement.findElement(getLocator("SEM_ProductGrpName"));
				displayedProductGrp.add(productGrpName.getText().trim().toUpperCase());
			}
			String missing = "";
			String extra = "";
			for (String name : expectedproductGrp) {
				if (!displayedProductGrp.contains(name)) {
					if (missingProductGrpScreenshots == null) {
						missingProductGrpScreenshots = ScreenShot.takeBrowserSnapShot(driver,
								config.getSnapshotFolder());
						missing = name;
					} else {
						missingProductGrpScreenshots = missingProductGrpScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						missing = missing + "," + name;
					}
				}
			}
			List<String> expectedGrp = Arrays.asList(expectedproductGrp);
			for (String name : displayedProductGrp) {
				if (!expectedGrp.contains(name)) {
					if (extraProductGrpScreenshots == null) {
						extraProductGrpScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						extra = name;
					} else {
						extraProductGrpScreenshots = extraProductGrpScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						extra = extra + "," + name;
					}
				}
			}
			if (missingProductGrpScreenshots != null && extraProductGrpScreenshots != null) {
				testStep[2] = missing + " are absent and " + extra + " are additional in the application";
				status = "Fail";
				testStep[4] = missingProductGrpScreenshots + "," + extraProductGrpScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (missingProductGrpScreenshots != null) {
				testStep[2] = missing + " are absent in the application";
				status = "Fail";
				testStep[4] = missingProductGrpScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (extraProductGrpScreenshots != null) {
				testStep[2] = extra + " are additional in the application";
				status = "Fail";
				testStep[4] = extraProductGrpScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else {
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification that application displays all product group associated with device population cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verifies the Loaner Information for the Product Groups
	 * 
	 * @param loanerInformation
	 *            - Loaner for each product group separated by ;. Eg -
	 *            Handpieces(21,4,0);Chargers(10,6,6)
	 * @param productGrp
	 *            - Product Group with NO loaners
	 * @return String[]
	 *            Returns the result of the execution 
	 * @throws TestScriptException
	 */
	public String[] verifyPopulatedLoaner(String loanerInformation, String productGrp) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that application displays the loaner information as per the device population";
		testStep[1] = "Application should display all the loaner information as per the device population";
		testStep[2] = "Application displays all the loaner information as per the device population";
		String status = "Pass";
		String failScreenshots = null;
		String passScreenshots = null;
		HashMap<String, HashMap<String, Integer>> allDisplayedProductGroupLoanerData = new HashMap<String, HashMap<String, Integer>>();
		List<String> displayedProductGrp = new ArrayList<String>();
		List<String> grp = Arrays.asList(productGrp.toUpperCase().split(","));
		try {
			HashMap<String, HashMap<String, Integer>> allExpectedProductGroupLoanerData = parseLoanerInfo(
					loanerInformation);

			List<WebElement> productGroupTiles = findElements(getLocator("SEM_ProductGrpTile"));
			for (WebElement wElement : productGroupTiles) {
				WebElement productGrpName = wElement.findElement(getLocator("SEM_ProductGrpName"));
				String grpName = productGrpName.getText().trim().toUpperCase();

				if (grp.contains(grpName)) {
					continue;
				}

				String elementLoanerCount = wElement.findElement(getLocator("Pg_TotalLoanerCount")).getText().trim();
				String elementLoanerDue = wElement.findElement(getLocator("Pg_DueLoanerCount")).getText().trim();
				elementLoanerDue = elementLoanerDue.substring(0, elementLoanerDue.indexOf(" "));
				String elementLoanerSoon = wElement.findElement(getLocator("Pg_SoonLoanerCount")).getText().trim();
				elementLoanerSoon = elementLoanerSoon.substring(0, elementLoanerSoon.indexOf(" "));

				displayedProductGrp.add(grpName);
				HashMap<String, Integer> productGrpLoanerData = new HashMap<String, Integer>();
				productGrpLoanerData.put("Count", Integer.parseInt(elementLoanerCount));
				productGrpLoanerData.put("Due", Integer.parseInt(elementLoanerDue));
				productGrpLoanerData.put("Soon", Integer.parseInt(elementLoanerSoon));
				allDisplayedProductGroupLoanerData.put(grpName, productGrpLoanerData);
			}

			String absentProductGrp = null;
			String wrongcountProductGrp = null;
			String expectedwrongcountProductGrp = null;

			for (String name : displayedProductGrp) {
				if (!allExpectedProductGroupLoanerData.containsKey(name)) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						absentProductGrp = name;
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						absentProductGrp = absentProductGrp + "," + name;
					}
				}

				HashMap<String, Integer> expectedproductGrpLoanerData = new HashMap<String, Integer>();
				expectedproductGrpLoanerData = allExpectedProductGroupLoanerData.get(name);
				int expectedLoanerCount = expectedproductGrpLoanerData.get("Count");
				int expectedLoanerDue = expectedproductGrpLoanerData.get("Due");
				int expectedLoanerSoon = expectedproductGrpLoanerData.get("Soon");

				HashMap<String, Integer> displayedproductGrpLoanerData = new HashMap<String, Integer>();
				displayedproductGrpLoanerData = allDisplayedProductGroupLoanerData.get(name);
				int displayedLoanerCount = displayedproductGrpLoanerData.get("Count");
				int displayedLoanerDue = displayedproductGrpLoanerData.get("Due");
				int displayedLoanerSoon = displayedproductGrpLoanerData.get("Soon");

				if (expectedLoanerCount != displayedLoanerCount || expectedLoanerDue != displayedLoanerDue
						|| expectedLoanerSoon != displayedLoanerSoon) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						wrongcountProductGrp = name + "(" + displayedLoanerCount + "," + displayedLoanerDue + ","
								+ displayedLoanerSoon + ")";
						expectedwrongcountProductGrp = name + "(" + expectedLoanerCount + "," + expectedLoanerDue + ","
								+ expectedLoanerSoon + ")";
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						wrongcountProductGrp = wrongcountProductGrp + ";" + name + "(" + displayedLoanerCount + ","
								+ displayedLoanerDue + "," + displayedLoanerSoon + ")";
						expectedwrongcountProductGrp = expectedwrongcountProductGrp + ";" + name + "("
								+ expectedLoanerCount + "," + expectedLoanerDue + "," + expectedLoanerSoon + ")";
					}
				}
				if (passScreenshots == null) {
					passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
			}
			if (absentProductGrp != null && wrongcountProductGrp != null) {
				testStep[2] = wrongcountProductGrp + " are displayed and expected count was "
						+ expectedwrongcountProductGrp + " and please enter Loaner count for " + absentProductGrp
						+ " product groups";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongcountProductGrp != null) {
				testStep[2] = wrongcountProductGrp + " are displayed and expected count was "
						+ expectedwrongcountProductGrp;
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (absentProductGrp != null) {
				testStep[2] = absentProductGrp
						+ " are absent in the Application, Please enter Loaner count for these product groups";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else {
				testStep[4] = passScreenshots;
			}
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification that application displays all product group associated with device population cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verifies the Status based Device on Listview Screen
	 * 
	 * @param grpOrType
	 *            Product Group - For Devices in the Product Group 
	 *            Product Type - For Device in the Product Type
	 * @param grpTypeName
	 *            Name of Product Group/Product Type
	 * @param deviceStatus
	 *            This can be DUE,SOON,OUT,Loaner
	 * @param deviceSerialNo
	 *            Device Serial Number as per the Status
	 * @return String[]
	 *            Returns the result of the execution   
	 * @throws TestScriptException
	 */
	public String[] verifyStatusBasedDeviceList(String grpOrType, String grpTypeName, String deviceStatus,
			String deviceSerialNo) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the Status based Device on Listview Screen";
		testStep[1] = "Application should display the Status based Device on Listview Screen";
		testStep[2] = "Application displays the Status based Device on Listview Screen";
		String status = "Pass";
		String failScreenshots = null;
		String wrongSerialNo = "";
		String[] grp = grpTypeName.split(",");
		List<String> serialNo = Arrays.asList(deviceSerialNo.split(","));
		List<WebElement> no = null;
		try {
			if (grpOrType.equalsIgnoreCase("Product Group") || grpOrType.equalsIgnoreCase("Product Type")) {
				config.setMapGlobalVariable("ProductGroup", grp[0]);
				clickElement("SEM_HomeLink", "Link");
				waitForElementToBeInvisible("SEM_Loading");
				waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
				if (grpOrType.equalsIgnoreCase("Product Type")) {
					if (!(grp.length == 2)) {
						testStep[2] = "If you are choosing Product Type, please group as well (Group,Type)";
						status = "Fail";
						testStep[4] = "";
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					config.setMapGlobalVariable("ProductType", grp[1]);
					clickElement("SEM_ProductGroupLink", "Link");
					waitForElementToBeInvisible("SEM_Loading");
					waitForElementToBeVisible(60, "Pg_Name");
				}
				if (deviceStatus.equalsIgnoreCase("DUE")) {
					if (grpOrType.equalsIgnoreCase("Product Group")) {
						clickElement("Pg_DueLabel", "Link");
					} else {
						clickElement("Pt_DueLabel", "Link");
					}
					waitForElementToBeVisible(60, "SerialNoValue");
					waitForElementToBeInvisible("SEM_Loading");
					no = findElements(getLocator("SerialNoValue"));
				} else if (deviceStatus.equalsIgnoreCase("SOON")) {
					if (grpOrType.equalsIgnoreCase("Product Group")) {
						clickElement("Pg_SoonLabel", "Link");
					} else {
						clickElement("Pt_SoonLabel", "Link");
					}
					waitForElementToBeVisible(60, "SerialNoValue");
					waitForElementToBeInvisible("SEM_Loading");
					no = findElements(getLocator("SerialNoValue"));
				} else if (deviceStatus.equalsIgnoreCase("OUT")) {
					if (grpOrType.equalsIgnoreCase("Product Group")) {
						clickElement("Pg_OutLabel", "Link");
					} else {
						clickElement("Pt_OutLabel", "Link");
					}
					waitForElementToBeVisible(60, "SerialNoValue");
					waitForElementToBeInvisible("SEM_Loading");
					no = findElements(getLocator("SerialNoValue"));
				} else if (deviceStatus.equalsIgnoreCase("Loaner")) {
					if (grpOrType.equalsIgnoreCase("Product Group")) {
						clickElement("Pg_LoanerLabel", "Link");
					} else {
						clickElement("Pt_LoanerLabel", "Link");
					}
					waitForElementToBeVisible(60, "LoanerSerialNoValue");
					waitForElementToBeInvisible("SEM_Loading");
					no = findElements(getLocator("LoanerSerialNoValue"));
				} else {
					testStep[2] = "Please enter specify correct device status i.e DUE,SOON,OUT,Loaner";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
				if (!(no.size() == serialNo.size())) {
					testStep[2] = "Displayed device in the listview are more than the expected devices";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
				for (WebElement wElement : no) {
					if (serialNo.contains(wElement.getText().trim())) {
						continue;
					} else {
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
							wrongSerialNo = wElement.getText().trim();
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
							wrongSerialNo = wrongSerialNo + "," + wElement.getText().trim();
						}
					}
				}
			} else {
				testStep[2] = "Please enter specify Product Group/Product Type";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			if (failScreenshots != null) {
				testStep[2] = wrongSerialNo + "are displayed in the listview and these were not the expected";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			clickElement("OverviewLink", "Link");
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification that application displays the Status based Device on Listview Screen cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}


	/**
	 * Method to validate that each Product Type have Name and Image
	 * 
	 * @param productGrp
	 *            - Product Group Name whose Product Type needs to be verified
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyProductTypeNameImage(String productGrp) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the all the product type(s) have name and image";
		testStep[1] = "Displayed product type(s) should have name and image";
		testStep[2] = "Displayed product type(s) have name and image";
		testStep[4] = null;
		String status = "Pass";
		String passScreenshots = null;
		String imgFailScreenshots = null;
		String nameFailScreenshots = null;
		boolean ProductTypeNoName = false;
		List<String> ProductTypeNoImg = new ArrayList<String>();
		try {
			config.setMapGlobalVariable("ProductGroup", productGrp);
			clickElement("SEM_HomeLink", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
			clickElement("SEM_ProductGroupLink", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "Pg_Name");
			List<WebElement> productTypeTiles = findElements(getLocator("ProductTypeTile"));
			for (WebElement wElement : productTypeTiles) {
				WebElement productTypeName = wElement.findElement(getLocator("ProductName"));
				WebElement productTypeImage = wElement.findElement(getLocator("ProductImage"));

				if (!productTypeName.getText().isEmpty()
						&& productTypeImage.getAttribute("src").toLowerCase().contains(productGrp.toLowerCase())) {

					new Actions(driver).moveToElement(productTypeName).perform();
					if (passScreenshots == null) {
						passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						passScreenshots = passScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				} else {

					new Actions(driver).moveToElement(wElement).perform();
					if (productTypeName.getText().isEmpty()) {
						ProductTypeNoName = true;
						if (nameFailScreenshots == null) {
							nameFailScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							nameFailScreenshots = nameFailScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
					} else {
						ProductTypeNoImg.add(productTypeName.getText());
						if (imgFailScreenshots == null) {
							imgFailScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							imgFailScreenshots = imgFailScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
					}
				}
			}
			if (ProductTypeNoName && ProductTypeNoImg.size() > 0) {
				testStep[2] = "Please check the product type(s)  with no Image and Name";
				status = "Fail";
				testStep[4] = imgFailScreenshots + "," + nameFailScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (ProductTypeNoName) {
				testStep[2] = "Please check the product type(s) with no Name";
				status = "Fail";
				testStep[4] = nameFailScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (ProductTypeNoImg.size() > 0) {
				testStep[2] = "Please check the product type(s) with no Image for " + ProductTypeNoImg.toString();
				status = "Fail";
				testStep[4] = imgFailScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification of all the product type(s) for name and image cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to validate that Maintenance Summary Section is present for each
	 * product group
	 * 
	 * @param ProductGrpInfo
	 *            - Information for the things need to be verified for a Product
	 *            Group for eg,
	 *            Handpieces(DUE,SOON,OUT,Loaner);Chargers(OUT,Loaner)
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyProductGrpMaintenanceSummarySection(String ProductGrpInfo) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that Maintenance Summary Section is present for each product group";
		testStep[1] = "Maintenance Summary Section should be present for each product group";
		testStep[2] = "Maintenance Summary Section is present for each product group";
		testStep[4] = null;
		String failScreenshots = null;
		String status = "Pass";

		List<String> errorProductGrp = new ArrayList<String>();
		try {
			String[] inputData = ProductGrpInfo.split(";");
			HashMap<String, List<String>> allProductGroupData = new HashMap<String, List<String>>();
			for (int dataCount = 0; dataCount < inputData.length; dataCount++) {
				String productGroupName = inputData[dataCount].substring(0, inputData[dataCount].indexOf("("))
						.toUpperCase();
				String presentElementDetail = inputData[dataCount].substring(inputData[dataCount].indexOf("(") + 1,
						inputData[dataCount].indexOf(")"));
				List<String> presentElementList = Arrays.asList(presentElementDetail.split(","));
				allProductGroupData.put(productGroupName, presentElementList);
			}

			List<WebElement> productGroupTiles = findElements(getLocator("SEM_ProductGrpTile"));
			for (WebElement wElement : productGroupTiles) {
				String productGrpName = wElement.findElement(getLocator("SEM_ProductGrpName")).getText().trim();
				config.setMapGlobalVariable("ProductGroup", productGrpName);

				List<String> ElementList = allProductGroupData.get(productGrpName.toUpperCase());
				List<String> errorMsg = new ArrayList<String>();

				if (ElementList.contains("OUT")) {
					if (!findElement(getLocator("Pg_OutLabel")).isDisplayed())
						errorMsg.add("OutLabel is absent");
					if (!findElement(getLocator("Pg_OutCount")).isDisplayed())
						errorMsg.add("OutCount is absent");
				}

				if (ElementList.contains("SOON")) {
					if (!findElement(getLocator("Pg_SoonLabel")).isDisplayed())
						errorMsg.add("SoonLabel is absent");
					if (!findElement(getLocator("Pg_SoonCount")).isDisplayed())
						errorMsg.add("SoonCount is absent");
				}

				if (ElementList.contains("DUE")) {
					if (!findElement(getLocator("Pg_DueLabel")).isDisplayed())
						errorMsg.add("DueLabel is absent");
					if (!findElement(getLocator("Pg_DueCount")).isDisplayed())
						errorMsg.add("DueCount is absent");
				}

				if (ElementList.contains("Loaner")) {
					if (!findElement(getLocator("Pg_LoanerLabel")).isDisplayed())
						errorMsg.add("LoanerLabel is absent");
					if (!findElement(getLocator("Pg_LoanerCount")).isDisplayed())
						errorMsg.add("LoanerCount is absent");
					if (!wElement.findElement(getLocator("Pg_DueLoanerCount")).isDisplayed())
						errorMsg.add("DueLoanerCount is absent");
					if (!wElement.findElement(getLocator("Pg_SoonLoanerCount")).isDisplayed())
						errorMsg.add("SoonLoanerCount is absent");
				}

				if (!findElement(getLocator("Pg_TotalDeviceCount")).isDisplayed())
					errorMsg.add("TotalDeviceCount is absent");
				if (!findElement(getLocator("Pg_TotalDeviceLabel")).isDisplayed())
					errorMsg.add("TotalDeviceLabel is absent");

				if (errorMsg.size() > 0) {
					errorProductGrp.add(productGrpName);
					methodFailureMessage = methodFailureMessage + productGrpName + "[" + errorMsg.toString() + "];";
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}
			}

			if (failScreenshots != null) {
				testStep[2] = "Please check " + errorProductGrp.toString() + " and go through logger for more details";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for productGroup Maintenance Summary Section cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to validate that Maintenance Summary Section is present for each
	 * product type
	 * 
	 * @param productGrp
	 *            - Product Group Name whose Product type(s) needs to be
	 *            verified for eg, Handpieces(DUE,SOON,OUT,Loaner)
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyProductTypeMaintenanceSummarySection(String productGrpInfo) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that Maintenance Summary Section is present for each product Type";
		testStep[1] = "Maintenance Summary Section should be present for each product Type";
		testStep[2] = "Maintenance Summary Section is present for each product Type";
		testStep[4] = null;
		String failScreenshots = null;
		String passScreenshots = null;
		String status = "Pass";

		List<String> errorProductType = new ArrayList<String>();
		try {
			String productGroupName = productGrpInfo.substring(0, productGrpInfo.indexOf("("));
			String presentElementDetail = productGrpInfo.substring(productGrpInfo.indexOf("(") + 1,
					productGrpInfo.indexOf(")"));

			config.setMapGlobalVariable("ProductGroup", productGroupName);

			clickElement("SEM_HomeLink", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "SEM_ProductGroupIcon");
			clickElement("SEM_ProductGroupLink", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "Pg_Name");
			List<WebElement> productTypeTiles = findElements(getLocator("ProductTypeTile"));
			for (WebElement wElement : productTypeTiles) {
			//	String productTypeName = wElement.findElement(getLocator("ProductName")).getText().trim();
				
				String productTypeName = wElement.findElement(getLocator("ProductName")).getAttribute("innerText").trim();
				config.setMapGlobalVariable("ProductType", productTypeName);

				List<String> ElementList = Arrays.asList(presentElementDetail.split(","));
				List<String> errorMsg = new ArrayList<String>();

				if (ElementList.contains("OUT")) {
					if (!findElement(getLocator("Pt_OutLabel")).isDisplayed())
						errorMsg.add("OutLabel is absent");
					if (!findElement(getLocator("Pt_OutCount")).isDisplayed())
						errorMsg.add("OutCount is absent");
				}

				if (ElementList.contains("SOON")) {
					if (!findElement(getLocator("Pt_SoonLabel")).isDisplayed())
						errorMsg.add("SoonLabel is absent");
					if (!findElement(getLocator("Pt_SoonCount")).isDisplayed())
						errorMsg.add("SoonCount is absent");
				}

				if (ElementList.contains("DUE")) {
					if (!findElement(getLocator("Pt_DueLabel")).isDisplayed())
						errorMsg.add("DueLabel is absent");
					if (!findElement(getLocator("Pt_DueCount")).isDisplayed())
						errorMsg.add("DueCount is absent");
				}

				if (ElementList.contains("Loaner")) {
					if (!findElement(getLocator("Pt_LoanerLabel")).isDisplayed())
						errorMsg.add("LoanerLabel is absent");
					if (!findElement(getLocator("Pt_LoanerCount")).isDisplayed())
						errorMsg.add("LoanerCount is absent");
					if (!wElement.findElement(getLocator("Pt_DueLoanerCount")).isDisplayed())
						errorMsg.add("DueLoanerCount is absent");
					if (!wElement.findElement(getLocator("Pt_SoonLoanerCount")).isDisplayed())
						errorMsg.add("SoonLoanerCount is absent");
				}

				if (!findElement(getLocator("Pt_TotalDeviceCount")).isDisplayed())
					errorMsg.add("TotalDeviceCount is absent");
				if (!findElement(getLocator("Pt_TotalDeviceLabel")).isDisplayed())
					errorMsg.add("TotalDeviceLabel is absent");

				mouseHover("Pt_TotalDeviceCount");
				if (errorMsg.size() > 0) {
					errorProductType.add(productTypeName);
					methodFailureMessage = methodFailureMessage + productTypeName + "[" + errorMsg.toString() + "];";
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}
				else{
										if (passScreenshots == null) {
											passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
									} else {
											passScreenshots = passScreenshots + ","
													+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
									}
				
								}
			}

			if (failScreenshots != null) {
				testStep[2] = "Please check " + errorProductType.toString() + " and go through logger for more details";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;	
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for productType Maintenance Summary Section cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	/**
	 * Method to verify Device Search Results are sorted based on Part number
	 * and Serial number
	 * 
	 * @param searchKey
	 *            - Search Key input to Search box
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifySearchResultSorting(String searchKey) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that Device Search Results are sorted based on Part number and Serial number";
		testStep[1] = "Device Search Results shoud be sorted based on Part number and Serial number";
		testStep[2] = "Device Search Results are sorted based on Part number and Serial number";
		testStep[4] = null;
		String status = "Pass";
		List<String> partnoList = new ArrayList<String>();
		LinkedHashMap<String, List<String>> sortingList = new LinkedHashMap<String, List<String>>();
		try {
			waitForElementToBeVisible(60, "SEM_SearchBox");
			inputData("SEM_SearchBox", "Text", searchKey);
			clickElement("SEM_SearchBoxIcon", "Button");
			waitForElementToBeVisible(60, "DeviceSearchResultLabel");
			waitForElementToBeVisible(60, "SearchResultTile");			
		
			List<WebElement> result = findElements(getLocator("SearchResultTile"));
			for (WebElement wElement : result) {
				String partno = wElement.findElement(getLocator("DeviceSearchResultPartNo")).getText().trim();
				partno = partno.substring(partno.lastIndexOf(" "));
				String serialno = wElement.findElement(getLocator("DeviceSearchResultSerialNo")).getText().trim();
				serialno = serialno.substring(serialno.lastIndexOf(" "));
				partnoList.add(partno);
				if (sortingList.containsKey(partno)) {
					sortingList.get(partno).add(serialno);
				} else {
					List<String> serialnoList = new ArrayList<String>();
					serialnoList.add(serialno);
					sortingList.put(partno, serialnoList);
				}
			}
			List<String> tempPartnoList = partnoList;
			Collections.sort(tempPartnoList);
			if (!tempPartnoList.equals(partnoList)) {
				testStep[2] = "Primary sorting on part number is not as expected";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			for (int i = 0; i < sortingList.size(); i++) {
				String temp = sortingList.keySet().iterator().next();
				List<String> tempSerialnoList = sortingList.get(temp);
				Collections.sort(tempSerialnoList);
				if (!tempSerialnoList.equals(sortingList.get(temp))) {
					testStep[2] = "Secondary sorting on serial number is not as expected";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}
			
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		 			
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Device search result sorting cannot be done because of an Exception. Please refer screenshot for error";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to verify order of Footer Quicklinks
	 * 
	 * @param ordering
	 *            - Footer link names in the expected Order
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyFooterLinksOrder(String ordering) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify Footer Quicklinks are displayed in expected order.";
		testStep[1] = "Order of Footer links should be like :- " + ordering;
		testStep[2] = "Order of Footer links is as expected";
		testStep[4] = null;
		String status = "Pass";
		String[] order = ordering.split(",");
		int i = 0;
		try {
			List<WebElement> footerlinks = findElements(getLocator("FooterQuickLinks"));
			for (WebElement wElement : footerlinks) {
				String link = wElement.getText().trim();
				if (!link.equals(order[i++])) {
					testStep[2] = "Order of Footer links is not as expected";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Order of Footer Quicklinks cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to verify logged in User name contains firstname, lastname in the
	 * expected format and fits in allotted space
	 * 
	 * @param firstName
	 *            - First name of User
	 * @param lastName
	 *            - Last name of User
	 * @param overflowName
	 *            - Long Username with length such that it does not fits in the
	 *            allocated space. For eg, Abcdefghijkl Satyanarayan Choudhary.
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyLoggedInUsername(String firstName, String lastName, String overflowName)
			throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that username is displayed in correct format and fits in allotted space";
		testStep[1] = "Username should be displayed in correct format and fits in allotted space";
		testStep[2] = "Username is displayed in correct format and fits in allotted space";
		testStep[4] = null;
		String passScreenshots = null;
		String status = "Pass";
		try {
			WebElement element = findElement(getLocator("SEM_Username"));
			String actualUsername = element.getText();
			String[] username = actualUsername.trim().split(" ");
			if (username.length != 2) {
				testStep[2] = "Username does not contains Firstname and Lastname";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			if (!firstName.equals(username[0]) || !lastName.equals(username[1])) {
				testStep[2] = "Username is not in correct format";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

			passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].innerText='" + overflowName + "';", element);
			verifyElementContainsText("SEM_Username", overflowName);

			WebElement parent = element.findElement(By.xpath(".."));
			String txtoverflow = parent.getCssValue("text-overflow");
			String overflow = parent.getCssValue("overflow");
			if (!(txtoverflow.equalsIgnoreCase("ellipsis") && overflow.equalsIgnoreCase("hidden"))) {
				testStep[2] = "Username cannot fit in allotted space";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

			passScreenshots = passScreenshots + ","
					+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			executor.executeScript("arguments[0].innerText='" + actualUsername + "';", element);
			verifyElementContainsText("SEM_Username", actualUsername);

			passScreenshots = passScreenshots + ","
					+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for correct format of Logged in Username cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to verify filters functionality on Device List Screen for Status tab
	 * 
	 * @param columnName
	 *            - List of all the columns to be tested
	 * @param columnValue
	 *            - List of all columns values for which column needs to be
	 *            filtered. 
	 *            N - No need to enter value for Last Connected Date
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewStatusFilters(String columnName, String columnValue) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify Add/Remove filters to/from the each column for Status tab on Device List Screen";
		testStep[1] = "ListView Filters Functionality should be as expected";
		testStep[2] = "ListView Filters Functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<String> columnValueList = Arrays.asList(columnValue.split(","));
		List<WebElement> column = new ArrayList<WebElement>();
		List<String> wrongValueColumn = new ArrayList<String>();
		String failScreenshots = null;
		String passScreenshots = null;
		String lastConnectedValue = null;
		int count = 0;
		try {
			if (columnNameList.size() != columnValueList.size()) {
				testStep[2] = "Please enter the values for corresponding to the column name";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			// Count of result before adding filter
			waitForElementToBeVisible(60, "SerialNoValue");
			column = findElements(getLocator("SerialNoValue"));
			count = column.size();

			// Adding Filter
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				if (columnNameList.get(i).equals("Status")) {
					inputData("Status Input", "text", columnValueList.get(i));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					List<WebElement> LastConnectedColumn = findElements(getLocator("LastConnected"));

					for (int k = 0; k < LastConnectedColumn.size(); k++) {
						lastConnectedValue = LastConnectedColumn.get(k).getText().trim();
						if (lastConnectedValue.length() > 5) {
							lastConnectedValue = lastConnectedValue.substring(0, lastConnectedValue.indexOf(" "));
							break;
						}
					}
					inputData("ColumnInput", "text", lastConnectedValue);
				} else {
					inputData("ColumnInput", "text", columnValueList.get(i));
				}

				if (columnNameList.get(i).equals("Description")) {
					column = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					column = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					column = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					column = findElements(getLocator("LastSeen"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					column = findElements(getLocator("LastConnected"));
				} else if (columnNameList.get(i).equals("Status")) {
					column = findElements(getLocator("Status Value"));
				} else {
					testStep[2] = "Please enter the column name in the input";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				if (column.size() == 0) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
					wrongValueColumn.add(columnNameList.get(i));
				}
				for (WebElement wElement : column) {
					if (columnNameList.get(i).equals("Last Connected")
							&& wElement.getText().contains(lastConnectedValue)) {

						continue;
					} else if (!wElement.getText().contains(columnValueList.get(i))) {
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						wrongValueColumn.add(columnNameList.get(i));
						break;
					}
				}
				if (passScreenshots == null) {
					passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				// Removing Filter
				clickElement("ClearFilter");
				waitExplicit(3000);
				column = findElements(getLocator("SerialNoValue"));
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

				if (count != column.size()) {
					testStep[2] = "Removing Filter functionality is not working";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}

			if (failScreenshots != null) {
				testStep[2] = wrongValueColumn.toString() + "don't have appropriate filter result";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Add/Remove filters to/from the each column on Device List Screen cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Method to verify filter functionality for Due Status Loaner and Non-Loaner Devices on Device List Screen 
	 * @param typeDevice
	 *            - Device Type e.g. Loaner, NonLoaner
	 * @param columnValue
	 *            - columnValue(Status column) to filter columns for Non-Loaner Device e.g.NOW
	 *            N - No need to enter value for Loaner Device
	 * @param columnValueLoaner
	 *  			- columnValue(Status column)  to filter columns for Loaner Device e.g.Due
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewStatusFiltersForDueDevices(String typeDevice, String columnValue,String columnValueLoaner) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify Add/Remove filters to/from the each column for Status tab on Device List Screen";
		testStep[1] = "ListView Filters Functionality should be as expected";
		testStep[2] = "ListView Filters Functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
	
		List<WebElement> column = new ArrayList<WebElement>();
		List<WebElement> columnAfterFilter = new ArrayList<WebElement>();
		
		List<String> wrongValueColumn = new ArrayList<String>();
		List<WebElement> statusValue = new ArrayList<WebElement>();
		String failScreenshots = null;
		String passScreenshots = null;
		
		int count = 0;
		int countDueNonLoanerDevices=0;
		int countDueLoanerDevices=0;
		try {
			if(typeDevice.equalsIgnoreCase("NonLoaner") && columnValue.equalsIgnoreCase("NOW"))
			{
			// Count of result before adding filter
			waitForElementToBeVisible(60, "SerialNoValue");
			column = findElements(getLocator("SerialNoValue"));
			count = column.size();
			inputData("Status Input", "text", columnValue);
			waitExplicit(2000);
			waitForElementToBeVisible(60, "Status Value");
		columnAfterFilter= findElements(getLocator("Status Value"));
		countDueNonLoanerDevices= columnAfterFilter.size();
		for(WebElement we: columnAfterFilter)
		{
			if (we.getText().equalsIgnoreCase("NOW")) {
				config.getLogger().info("Verified text for non loaner devices for Due Status. It is as expected : NOW");
			} 
			else
			{
				config.getLogger().info("Verified text for non loaner devices for Due Status. It is not as expected : NOW");
			}
			
		}
			//clickElement("ClearFilter");
			waitExplicit(3000);
		}
			else if(typeDevice.equalsIgnoreCase("Loaner")) {
				clickElement("SEM_HomeLink", "Link");
				waitForElementToBeInvisible("SEM_Loading");
				waitForElementToBeVisible(60, "SEM_ProductGrpName");
				
					clickElement("Pg_LoanerLabel", "Link");
					waitForElementToBeVisible(60, "LoanerSerialNoValue");
					inputData("Status Input", "text", columnValueLoaner);
					waitForElementToBeVisible(60, "Status Value");
					columnAfterFilter= findElements(getLocator("Status Value"));
					countDueLoanerDevices= columnAfterFilter.size();
					for(WebElement we: columnAfterFilter)
					{
						if (we.getText().contains("DUE:")) {
							config.getLogger().info("Verified text for loaner devices for Due Status. It is as expected : DUE:");
						} 
						else
						{
							config.getLogger().info("Verified text for loaner devices for Due Status. It is not as expected : DUE:");
						}
						
					}
					
				//	clickElement("LoanerViewClearFilter");
					waitExplicit(3000);
					}
					else {
					testStep[2] = "Please enter the column name in the input";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				if (passScreenshots == null) {
					passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} 
				
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[4] = passScreenshots;
		}
	catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Add/Remove filters to/from the each column on Device List Screen cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify filters functionality for Usage tab on Device List Screen
	 * 
	 * @param columnName
	 *            - List of all the columns to be tested
	 * @param columnValue
	 *            - List of all columns values for which column needs to be
	 *            filtered. N - Specify No need to enter value.
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewUsageFilters(String columnName, String columnValue) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify Add/Remove filters to/from the each column for Usage tab on Device List Screen";
		testStep[1] = "ListView Filters Functionality should be as expected";
		testStep[2] = "ListView Filters Functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<String> columnValueList = Arrays.asList(columnValue.split(","));
		List<WebElement> column = new ArrayList<WebElement>();
		List<String> wrongValueColumn = new ArrayList<String>();
		String failScreenshots = null;
		String passScreenshots = null;
		String lastConnectedValue = null;
		String usesXdaysValue = null;
		String ageValue = null;
		int count = 0;
		try {
			if (columnNameList.size() != columnValueList.size()) {
				testStep[2] = "Please enter the values for corresponding to the column name";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			// Count of result before adding filter
			waitForElementToBeVisible(60, "SerialNoValue");
			column = findElements(getLocator("SerialNoValue"));
			count = column.size();

			// Adding Filter
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				if (columnNameList.get(i).equals("Last Connected")) {
					List<WebElement> LastConnectedColumn = findElements(getLocator("LastConnected"));

					for (int k = 0; k < LastConnectedColumn.size(); k++) {
						lastConnectedValue = LastConnectedColumn.get(k).getText().trim();
						if (lastConnectedValue.length() > 5) {
							lastConnectedValue = lastConnectedValue.substring(0, lastConnectedValue.indexOf(" "));
							break;
						}
					}
					inputData("ColumnInput", "text", lastConnectedValue);
				} else if (columnNameList.get(i).equals("Uses Last X days")) {
					List<WebElement> usesXdaysColumn = findElements(getLocator("LastXdaysValue"));

					for (int k = 0; k < usesXdaysColumn.size(); k++) {
						usesXdaysValue = usesXdaysColumn.get(k).getText().trim();
						if (usesXdaysValue.length() > 0) {
							break;
						}
					}
					inputData("UsesLastXdaysInput", "text", usesXdaysValue);
				} else if (columnNameList.get(i).equals("Age (Years)")) {
					List<WebElement> ageColumn = findElements(getLocator("AgeValue"));

					for (int k = 0; k < ageColumn.size(); k++) {
						ageValue = ageColumn.get(k).getText().trim();
						if (ageValue.length() > 0) {
							break;
						}
					}
					inputData("ColumnInput", "text", ageValue);
				} else {
					inputData("ColumnInput", "text", columnValueList.get(i));
				}

				if (columnNameList.get(i).equals("Description")) {
					column = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					column = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					column = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					column = findElements(getLocator("LastConnected"));
				} else if (columnNameList.get(i).equals("Uses Last X days")) {
					column = findElements(getLocator("LastXdaysValue"));
				} else if (columnNameList.get(i).equals("Age (Years)")) {
					column = findElements(getLocator("AgeValue"));
				} else {
					testStep[2] = "Please enter the column name in the input";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				if (column.size() == 0) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
					wrongValueColumn.add(columnNameList.get(i));
				}
				for (WebElement wElement : column) {
					if ((columnNameList.get(i).equals("Last Connected")
							&& wElement.getText().contains(lastConnectedValue))
							|| (columnNameList.get(i).equals("Uses Last X days")
									&& wElement.getText().contains(usesXdaysValue))
							|| (columnNameList.get(i).equals("Age (Years)") && wElement.getText().contains(ageValue))) {
						continue;
					} else if (!wElement.getText().contains(columnValueList.get(i))) {
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						wrongValueColumn.add(columnNameList.get(i));
						break;
					}
				}
				if (passScreenshots == null) {
					passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				// Removing Filter
				clickElement("ClearFilter");
				waitExplicit(3000);
				column = findElements(getLocator("SerialNoValue"));
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

				if (count != column.size()) {
					testStep[2] = "Removing Filter functionality is not working";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}

			if (failScreenshots != null) {
				testStep[2] = wrongValueColumn.toString() + "don't have appropriate filter result";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Add/Remove filters to/from the each column on Device List Screen cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to verify filters functionality for History tab on Device List Screen
	 * 
	 * @param columnName
	 *            - List of all the columns to be tested
	 * @param columnValue
	 *            - List of all columns values for which column needs to be
	 *            filtered. - N - Specify No need to enter value.
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewHistoryFilters(String columnName, String columnValue) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify Add/Remove filters to/from the each column for History tab on Device List Screen";
		testStep[1] = "ListView Filters Functionality should be as expected";
		testStep[2] = "ListView Filters Functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<String> columnValueList = Arrays.asList(columnValue.split(","));
		List<WebElement> column = new ArrayList<WebElement>();
		List<String> wrongValueColumn = new ArrayList<String>();
		String failScreenshots = null;
		String passScreenshots = null;
		String startDateValue = null;
		String endDateValue = null;
		int count = 0;
		try {
			if (columnNameList.size() != columnValueList.size()) {
				testStep[2] = "Please enter the values for corresponding to the column name";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			// Count of result before adding filter
			waitForElementToBeVisible(60, "SerialNoValue");
			column = findElements(getLocator("SerialNoValue"));
			count = column.size();

			// Adding Filter
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				if (columnNameList.get(i).equals("Start")) {
					List<WebElement> date = findElements(getLocator("StartDateValue"));

					for (int k = 0; k < date.size(); k++) {
						startDateValue = date.get(k).getText().trim();
						if (startDateValue.length() > 5) {
							break;
						}
					}
					inputData("ColumnInput", "text", startDateValue);
				} else if (columnNameList.get(i).equals("End")) {
					List<WebElement> date = findElements(getLocator("EndDateValue"));

					for (int k = 0; k < date.size(); k++) {
						endDateValue = date.get(k).getText().trim();
						if (endDateValue.length() > 5) {
							break;
						}
					}
					inputData("ColumnInput", "text", endDateValue);
				} else {
					inputData("ColumnInput", "text", columnValueList.get(i));
				}

				if (columnNameList.get(i).equals("Description")) {
					column = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					column = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					column = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Workorder #")) {
					column = findElements(getLocator("WorkorderValue"));
				} else if (columnNameList.get(i).equals("Start")) {
					column = findElements(getLocator("StartDateValue"));
				} else if (columnNameList.get(i).equals("End")) {
					column = findElements(getLocator("EndDateValue"));
				} else if (columnNameList.get(i).equals("Summary")) {
					column = findElements(getLocator("SummaryValue"));
				} else {
					testStep[2] = "Please enter the column name in the input";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				if (column.size() == 0) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
					wrongValueColumn.add(columnNameList.get(i));
				}
				for (WebElement wElement : column) {
					if ((columnNameList.get(i).equals("Start") && wElement.getText().contains(startDateValue))
							|| (columnNameList.get(i).equals("End") && wElement.getText().contains(endDateValue))) {
						continue;
					} else if (!wElement.getText().contains(columnValueList.get(i))) {
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						wrongValueColumn.add(columnNameList.get(i));
						break;
					}
				}
				if (passScreenshots == null) {
					passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				// Removing Filter
				clickElement("ClearFilter");
				waitExplicit(3000);
				column = findElements(getLocator("SerialNoValue"));
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

				if (count != column.size()) {
					testStep[2] = "Removing Filter functionality is not working";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}

			if (failScreenshots != null) {
				testStep[2] = wrongValueColumn.toString() + "don't have appropriate filter result";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Add/Remove filters to/from the each column on Device List Screen cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Method to verify that the datetime field on the page has the expected
	 * value in the provided format
	 * 
	 * @param format
	 *            - Expected format for datetime
	 * @param value
	 *            - Expected value for the datetime field
	 * @return String[] Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyDateTime(String format, String value) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the mentioned date time is in provided format";
		String status = "Pass";
		testStep[1] = "Mentioned date time should be in provided format";
		testStep[2] = "Mentioned date time is in provided format";
		testStep[4] = null;

		try {						
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		    dateFormat.setLenient(false);
		    dateFormat.parse(value.trim());
		    testStep[2] = "Mentioned date time" + value + " is in provided format -" + format;
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
				testStep[2] = "Mentioned date time format cannot be verified because of some error. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}	
	
	/**
	 * Verifies whether all the Events have Date and Description
	 * 
	 * @return String[] - Test Result for the method execution
	 * @throws TestScriptException
	 */
	public String[] verifyEventDateDescNotBlank() throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that all the events have date and description";
		testStep[1] = "Displayed events should have date and description";
		testStep[2] = "Displayed events have date and description";
		testStep[4] = null;
		String status = "Pass";
		String passScreenshots = null;
		String failScreenshots = null;
		List<String> faultyEvents = new ArrayList<String>();
		try {
			List<WebElement> events = findElements(getLocator("DS_Events"));
			for (int i = 0; i < events.size(); i++) {
				WebElement event = events.get(i);
				String eventDate = event.findElement(getLocator("DS_Date_ListofEvents")).getText();
				String eventDescription = event.findElement(getLocator("DS_Description_ListofEvents")).getText();
				if (eventDate.isEmpty() || eventDescription.isEmpty()) {
					new Actions(driver).moveToElement(event).perform();
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
					faultyEvents.add("Event " + i);
				} else {
					if (passScreenshots == null) {
						passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						passScreenshots = passScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}
			}
			if (faultyEvents.size() > 0) {
				testStep[2] = faultyEvents.toString() + " do not have proper Event Date / Description";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Events cannot be verified because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Method to verify that the datetime field on the page has the expected
	 * value in the provided format
	 * 
	 * @param format
	 *            - Expected format for datetime
	 * @param ObjectElement
	 *            - Actual format for the datetime field
	 * @return String[] Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyDateTimeFormat(String ObjectElement, String format) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the mentioned date time is in provided format";
		String status = "Pass";
		testStep[1] = "Mentioned date time should be in provided format";
		testStep[2] = "Mentioned date time is in provided format";
		testStep[4] = null;

		try {
			List<WebElement> listofElements = findElements(getLocator(ObjectElement));
			for (int i = 0; i < listofElements.size(); i++) {
				Date date = null;
				// WebElement webele=findElement(getLocator(ObjectElement));
				String value = listofElements.get(i).getText().toString().trim();
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				date = sdf.parse(value);
				if (sdf.format(date) != null) {
					testStep[2] = "Mentioned date time is in provided format -" + format;
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					testStep[2] = "Mentioned date " + value + " time is not in the provided format -" + format;
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					status = "Fail";
					methodFailureMessage = testStep[2];
				}
			}

		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Mentioned date time format cannot be verified because of some error. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}


	/** Method to verify the Exported Device List
	 * @param path_filename
	 *        - Complete path along with the csv filename for the exported device list file in the format path,filename.
	 *        For eg, C:\Users\ABC\Downloads,Stryker_SEM_Device_List_export        
	 * @param expectedColumnNames
	 *        - List of the expected column names separated by comma in the exported device list.
	 *        For eg Product Group,Product Type.
	 * @param filteredColumn
	 *        - Column name for which the Device List is filtered.
	 *        N - if device list was not filtered before export operation.
	 * @param filterCritieria
	 *        - Value for the column name for which Device list is filtered
	 *        N - if device list was not filtered before export operation.
	 * @return String[]
	 * 		  - Returns the result of the execution.
	 * @throws TestScriptException
	 */
	public String[] verifyExportedDeviceList(String path_filename, String expectedColumnNames, String filteredColumn, String filterCritieria) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the exported device list has the expected device data.";
		testStep[1] = "Exported device list should have the expected device data.";
		testStep[2] = "Exported device list has the expected device data.";
		testStep[4] = null;
		String status = "Pass";
		try{
			String[] path_fName = path_filename.split(",");
			File theNewestFile = null;
			String path = path_fName[0];
		    File dir = new File(path_fName[0]);
		    String fName = path_fName[1];
		    File[] files = dir.listFiles();
		    String expectedFileName ="";
    		
		    if (files.length > 0) {
		        /** The newest file comes first **/
		        Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		        theNewestFile = files[0];
		        expectedFileName = theNewestFile.getName();
		    }
		    else{
		    	status = "Fail";
		    	testStep[2] = "No file exists at the mentioned location, please check the path and try again";
		    	testStep[4] = "";
		    	methodFailureMessage = testStep[2];
		    	throw new Exception();
		    }
	        if(!expectedFileName.contains(fName)){
	        	status = "Fail";
		    	testStep[2] = "Device list file is not exported recently, please export the file and try again.";
		    	testStep[4] = "";
		    	methodFailureMessage = testStep[2];
		    	throw new Exception();
	        }
	        else{
	        //Reader in = new FileReader(path +"\\" +expectedFileName);	
	        BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(path +"\\" +expectedFileName), "UTF-8"));
	        
			CSVFormat format = CSVFormat.RFC4180.withFirstRecordAsHeader();
			CSVParser parser = new CSVParser(bf, format);
			List<CSVRecord> records = parser.getRecords();
			List<String> header = new ArrayList<String>();
			Map<String, Integer> headerMap = parser.getHeaderMap();
			Iterator<String> iterator = headerMap.keySet().iterator();
			while(iterator.hasNext()){
				header.add(iterator.next());
			}
			//CSVRecord header = records.get(0);
			boolean correctData = false;
			boolean wrongData = false;
			String headerString = header.toString();
			//headerString = headerString.substring(headerString.indexOf("mapping={") +"mapping={".length()+1,headerString.indexOf("}"));
			//Iterator<CSVRecord> records = format.parse(bf).iterator();
			//while(records.hasNext()){
				//CSVRecord record = records.next();
			
			List<String> expectedColNames = Arrays.asList(expectedColumnNames.split(","));
			if(headerString.split(",").length==expectedColNames.size()){
				for(String colName: expectedColNames){
					if(colName.contains("Uses")){
						colName = "Uses Last";
					}							
					if(!headerString.contains(colName)){
						status = "fail";
				    	testStep[2] = "Exported device list does not contains the expected columns.";
				    	testStep[4] = "";
				    	methodFailureMessage = testStep[2];
				    	throw new Exception();
					}
				}
			}
			else{
				status = "fail";
		    	testStep[2] = "Number of columns for exported device list is not as expected.";
		    	testStep[4] = "";
		    	methodFailureMessage = testStep[2];
		    	throw new Exception();
			}
		
			if(!filteredColumn.equals("N") && !filterCritieria.equals("N")){
				if(records.isEmpty()){
					status = "Fail";
			    	testStep[2] = "There is no data in the exported file.";
			    	testStep[4] = "";
			    	methodFailureMessage = testStep[2];
			    	throw new Exception();
				}	
				else{	
					for(CSVRecord record: records){
						String filteredColumnData = record.get(filteredColumn);
					    if(filteredColumnData.equals(filterCritieria)){
					    	correctData = true;
					    }
					    else{
					    	wrongData = true;
					    }
					  }				
					if(wrongData && correctData){
						status = "Fail";
				    	testStep[2] = "Exported device list contains information in addition to filtered criteria.";
				    	testStep[4] = "";
				    	methodFailureMessage = testStep[2];
				    	throw new Exception();
					}
					else if (wrongData && !correctData){
						status = "Fail";
				    	testStep[2] = "Exported device list does not contains information as per the filtered criteria.";
				    	testStep[4] = "";
				    	methodFailureMessage = testStep[2];
				    	throw new Exception();
					}
				}
			}
			else{
					if(records.isEmpty()){
						status = "Fail";
				    	testStep[2] = "There is no data in the exported file.";
				    	testStep[4] = "";
				    	methodFailureMessage = testStep[2];
				    	throw new Exception();
					}
			}
			if(!filteredColumn.equals("N") && !filterCritieria.equals("N")){
				testStep[2]="Exported Device List file has the expected data as per the filter criteria.";
			}
			else{
				testStep[2] = "Exported Device List file has the expected column names.";
			}
	    }	
		}catch(IllegalArgumentException iae){
			if(iae.getMessage().contains("Mapping for "+filterCritieria + " not found") && (!filteredColumn.equals("N") && !filterCritieria.equals("N"))){
				status = "fail";
		    	testStep[2] = "Exported device list does not contains the filtered column "+filteredColumn+".";
		    	testStep[4] = "";
		    	methodFailureMessage = testStep[2];
			}
			else if((iae.getMessage().contains("Mapping for ") && (filteredColumn.equals("N") && filterCritieria.equals("N"))))
			{
				status = "fail";
		    	testStep[2] = "Exported device list does not contains the expected columnnames. ";
		    	testStep[4] = "";
		    	methodFailureMessage = testStep[2];
			}
		}catch(FileNotFoundException fnfe){
			status = "fail";
	    	testStep[2] = "Exported device list file cannot be found, please specify the correct filename with the path.";
	    	testStep[4] = "";
	    	methodFailureMessage = testStep[2];
		} catch (IOException e) {
			status = "fail";
	    	testStep[2] = "Exported device list file cannot be parsed because of Exception "+e.getMessage()+".";
	    	testStep[4] = "";
	    	methodFailureMessage = testStep[2];
		}catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Exported device list cannot be verified because of some exception, please check Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equalsIgnoreCase("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify Device data in List view tabs i.e Status,Usage,History
	 * (History view will not show the same devices that are included on the
	 * Status and Usage views since the History view will not show a device if
	 * it does not have at least one Completed Maintenance Event)
	 * 
	 * @param historyDeviceSerialno
	 *            - Serial number of device that should be present in History
	 *            Tab
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewTabData(String historyDeviceSerialno) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify the device data in List view tabs";
		testStep[1] = "Device data should be same for Status, Usage and Device with Completed Maintenance Event should be present in History Tab";
		testStep[2] = "Device data in List view tabs is as expected";
		testStep[4] = null;
		String status = "Pass";
		try {
			verifyAttributeContainsText("StatusTab", "class", "active");
			waitForElementToBeVisible(60, "SerialNoValue");
			List<WebElement> statusTabElement = findElements(getLocator("SerialNoValue"));
			ArrayList<String> statusTabData = new ArrayList<String>();
			for (WebElement wElement : statusTabElement) {
				statusTabData.add(wElement.getText().trim());
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

			clickElement("UsageTab", "Link");
			verifyAttributeContainsText("UsageTab", "class", "active");
			waitForElementToBeVisible(60, "SerialNoValue");
			List<WebElement> usageTabElement = findElements(getLocator("SerialNoValue"));
			ArrayList<String> usageTabData = new ArrayList<String>();
			for (WebElement wElement : usageTabElement) {
				usageTabData.add(wElement.getText().trim());
			}
			testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

			Collections.sort(statusTabData);
			Collections.sort(usageTabData);
			if (!statusTabData.equals(usageTabData)) {
				testStep[2] = "Device in Status tab and Usage tab are not same";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

			clickElement("HistoryTab", "Link");
			verifyAttributeContainsText("HistoryTab", "class", "active");
			waitForElementToBeVisible(60, "SerialNoValue");
			List<WebElement> historyTabElement = findElements(getLocator("SerialNoValue"));
			List<String> historyTabData = new ArrayList<String>();
			for (WebElement wElement : historyTabElement) {
				historyTabData.add(wElement.getText().trim());
			}
			testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

			List<String> expectedHistoryTabData = Arrays.asList(historyDeviceSerialno.split(","));

			Collections.sort(historyTabData);
			Collections.sort(expectedHistoryTabData);
			if (!expectedHistoryTabData.equals(historyTabData)) {
				testStep[2] = "Device in History tab are not the expected one";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			waitExplicit(2000);
			clickElement("StatusTab", "Link");
			verifyAttributeContainsText("StatusTab", "class", "active");
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for device data in List view tabs cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	/**
	 * Method to verify Listview Status tab sorting functionality
	 * @param columnName
	 * 		  ColumnName(s) for which sorting needs to be tested
	 * 	      For testing sorting on multiple columns, provide all the columns names separated by comma.	
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewStatusSorting(String columnName) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify listview Status tab sorting functionality";
		testStep[1] = "Listview Status tab sorting functionality should be as expected";
		testStep[2] = "Listview Status tab sorting functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<WebElement> statusTabElement = new ArrayList<WebElement>();
		List<String> wrongAscendingOrderColumn = new ArrayList<String>();
		List<String> wrongDescendingOrderColumn = new ArrayList<String>();
		int countBlank = 0;
		try {
			String attribute = findElement(getLocator("StatusTab")).getAttribute("class");
			if(!attribute.equalsIgnoreCase("active")){
				clickElement("StatusTab", "Link");
			}	
			verifyAttributeContainsText("StatusTab", "class", "active");
			waitForElementToBeVisible(60, "SerialNoValue");

			for (int i = 0; i < columnNameList.size(); i++) {
				countBlank = 0;
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				clickElement("ColumnButton", "Button");
				waitForElementToBeVisible(60, "ColumnInAscendingOrder");
				waitExplicit(2000);
				if (columnNameList.get(i).equals("Description")) {
					statusTabElement = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					statusTabElement = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					statusTabElement = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					statusTabElement = findElements(getLocator("LastSeen"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					statusTabElement = findElements(getLocator("LastConnected"));
				} else if (columnNameList.get(i).equals("Status")) {
					statusTabElement = findElements(getLocator("Status Value"));
				} else {
					testStep[2] = columnNameList.get(i) + " doesn't exist";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				ArrayList<String> statusTabData1 = new ArrayList<String>();
				ArrayList<String> orderedStatusTabData = new ArrayList<String>();
				for (WebElement wElement : statusTabElement) {
					statusTabData1.add(wElement.getText().trim());
					if(!wElement.getText().trim().equals("")){
						orderedStatusTabData.add(wElement.getText().trim());
					}
				}
				
				countBlank = Collections.frequency(statusTabData1, "");
				Collections.sort(orderedStatusTabData);
				
				for(int n=0;n<countBlank;n++){
					orderedStatusTabData.add("");
				}
				
				if (!orderedStatusTabData.equals(statusTabData1)) {
					wrongAscendingOrderColumn.add(columnNameList.get(i));
				}
				if (testStep[4] == null) {
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				// Descending order test
				clickElement("ColumnButton", "Button");
				waitForElementToBeVisible(60, "ColumnInDescendingOrder");
				waitExplicit(2000);
				if (columnNameList.get(i).equals("Description")) {
					statusTabElement = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					statusTabElement = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					statusTabElement = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					statusTabElement = findElements(getLocator("LastSeen"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					statusTabElement = findElements(getLocator("LastConnected"));
				} else if (columnNameList.get(i).equals("Status")) {
					statusTabElement = findElements(getLocator("Status Value"));
				}

				ArrayList<String> statusTabData2 = new ArrayList<String>();
				//orderedStatusTabData = new ArrayList<String>();
				for (WebElement wElement : statusTabElement) {
					statusTabData2.add(wElement.getText().trim());
					//orderedStatusTabData.add(wElement.getText().trim());
				}
				
				Collections.reverse(orderedStatusTabData);
				//Collections.sort(orderedStatusTabData, Collections.reverseOrder());
				if (!orderedStatusTabData.equals(statusTabData2)) {
					wrongDescendingOrderColumn.add(columnNameList.get(i));
				}
				testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}

			if (wrongAscendingOrderColumn.size() > 0 && wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order and "
						+ wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongAscendingOrderColumn.size() > 0) {
				testStep[2] = wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for listview status tab sorting functionality cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify Usage Listview tab sorting functionality
	 * @param columnName
	 * 		  ColumnName(s) for which sorting needs to be tested
	 * 	      For testing sorting on multiple columns, provide all the columns names separated by comma.
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewUsageSorting(String columnName) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify listview Usage tab sorting functionality";
		testStep[1] = "Listview Usage tab sorting functionality should be as expected";
		testStep[2] = "Listview Usage tab sorting functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<WebElement> statusTabElement = new ArrayList<WebElement>();
		List<String> wrongAscendingOrderColumn = new ArrayList<String>();
		List<String> wrongDescendingOrderColumn = new ArrayList<String>();
		int countBlank = 0;
		try {
			String attribute = findElement(getLocator("UsageTab")).getAttribute("class");
			if(!attribute.equalsIgnoreCase("active")){
				clickElement("UsageTab", "Link");
				verifyAttributeContainsText("UsageTab", "class", "active");
			}

			waitForElementToBeVisible(60, "SerialNoValue");

			for (int i = 0; i < columnNameList.size(); i++) {
				countBlank = 0;
				if (columnNameList.get(i).equals("Uses Last X days")) {
					clickElement("UsesButton", "Button");
					waitForElementToBeVisible(60, "UsesInAscendingOrder");
				} else {
					config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
					clickElement("ColumnButton", "Button");
					waitForElementToBeVisible(60, "ColumnInAscendingOrder");
					waitExplicit(2000);
				}

				if (columnNameList.get(i).equals("Description")) {
					statusTabElement = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					statusTabElement = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					statusTabElement = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					statusTabElement = findElements(getLocator("LastConnected"));
				} else if (columnNameList.get(i).equals("Uses Last X days")) {
					statusTabElement = findElements(getLocator("LastXdaysValue"));
				} else if (columnNameList.get(i).equals("Age (Years)")) {
					statusTabElement = findElements(getLocator("AgeValue"));
				} else {
					testStep[2] = columnNameList.get(i) + " doesn't exist";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				ArrayList<String> statusTabData1 = new ArrayList<String>();
				ArrayList<String> orderedStatusTabData = new ArrayList<String>();
				for (WebElement wElement : statusTabElement) {
					statusTabData1.add(wElement.getText().trim());
					if(!wElement.getText().trim().equals("")){
						orderedStatusTabData.add(wElement.getText().trim());
					}
				}
				
				countBlank = Collections.frequency(statusTabData1, "");
				if(columnNameList.get(i).equals("Uses Last X days")){
					Collections.sort(orderedStatusTabData, new Comparator<String>(){
						@Override
						public int compare(String o1, String o2){
							try {
								int a = Integer.parseInt(o1);
								int b = Integer.parseInt(o2);		
								
								if(a>b){
									return 1;
								}
								else if (a==b){
									return 0;
								}
								else {
								return -1;
								}					
				            }catch(Exception e){
				            	try {
									throw new Exception(e);
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									return -1;
								}
				            }
						}
					});
				}
				else {
					Collections.sort(orderedStatusTabData);
				}
				
				for(int n=0;n<countBlank;n++){
					orderedStatusTabData.add("");
				}
				if (!orderedStatusTabData.equals(statusTabData1)) {
					wrongAscendingOrderColumn.add(columnNameList.get(i));
				}
				if (testStep[4] == null) {
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				// Descending order test
				if (columnNameList.get(i).equals("Uses Last X days")) {
					clickElement("UsesButton", "Button");
					waitForElementToBeVisible(60, "UsesInDescendingOrder");
					
				} else {
					clickElement("ColumnButton", "Button");
					waitForElementToBeVisible(60, "ColumnInDescendingOrder");
					waitExplicit(2000);
				}
				if (columnNameList.get(i).equals("Description")) {
					statusTabElement = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					statusTabElement = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					statusTabElement = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					statusTabElement = findElements(getLocator("LastConnected"));
				} else if (columnNameList.get(i).equals("Uses Last X days")) {
					statusTabElement = findElements(getLocator("LastXdaysValue"));
				} else if (columnNameList.get(i).equals("Age (Years)")) {
					statusTabElement = findElements(getLocator("AgeValue"));
				}

				ArrayList<String> statusTabData2 = new ArrayList<String>();
				//orderedStatusTabData = new ArrayList<String>();
				for (WebElement wElement : statusTabElement) {
					statusTabData2.add(wElement.getText().trim());
					//orderedStatusTabData.add(wElement.getText().trim());
				}

				Collections.reverse(orderedStatusTabData);
				//Collections.sort(orderedStatusTabData, Collections.reverseOrder());
				if (!orderedStatusTabData.equals(statusTabData2)) {
					wrongDescendingOrderColumn.add(columnNameList.get(i));
				}
				testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
			if (wrongAscendingOrderColumn.size() > 0 && wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order and "
						+ wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongAscendingOrderColumn.size() > 0) {
				testStep[2] = wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			clickElement("StatusTab", "Link");
			verifyAttributeContainsText("StatusTab", "class", "active");
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for listview tab usage sorting functionality cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Method to verify History Listview tab sorting functionality
	 * @param columnName
	 * 		  ColumnName(s) for which sorting needs to be tested
	 * 	      For testing sorting on multiple columns, provide all the columns names separated by comma.
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewHistorySorting(String columnName) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify listview History tab sorting functionality";
		testStep[1] = "Listview History tab sorting functionality should be as expected";
		testStep[2] = "Listview History tab sorting functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<WebElement> statusTabElement = new ArrayList<WebElement>();
		List<String> wrongAscendingOrderColumn = new ArrayList<String>();
		List<String> wrongDescendingOrderColumn = new ArrayList<String>();
		int countBlank = 0;
		try {
			String attribute = findElement(getLocator("HistoryTab")).getAttribute("class");
			if(!attribute.equalsIgnoreCase("active")){
				clickElement("HistoryTab", "Link");
				verifyAttributeContainsText("HistoryTab", "class", "active");
			}
			for (int i = 0; i < columnNameList.size(); i++) {
				countBlank = 0;
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				clickElement("ColumnButton", "Button");
				waitForElementToBeVisible(60, "ColumnInAscendingOrder");
				waitExplicit(2000);

				if (columnNameList.get(i).equals("Description")) {
					statusTabElement = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					statusTabElement = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					statusTabElement = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Workorder #")) {
					statusTabElement = findElements(getLocator("WorkorderValue"));
				} else if (columnNameList.get(i).equals("Start")) {
					statusTabElement = findElements(getLocator("StartDateValue"));
				} else if (columnNameList.get(i).equals("End")) {
					statusTabElement = findElements(getLocator("EndDateValue"));
				} else if (columnNameList.get(i).equals("Summary")) {
					statusTabElement = findElements(getLocator("SummaryValue"));
				} else {
					testStep[2] = columnNameList.get(i) + " doesn't exist";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				ArrayList<String> statusTabData1 = new ArrayList<String>();
				ArrayList<String> orderedStatusTabData = new ArrayList<String>();
				for (WebElement wElement : statusTabElement) {
					statusTabData1.add(wElement.getText().trim());
					if(!wElement.getText().trim().equals("")){
						orderedStatusTabData.add(wElement.getText().trim());
					}
				}
				
				countBlank = Collections.frequency(statusTabData1, "");
				if(columnNameList.get(i).equals("Workorder #")){
					Collections.sort(orderedStatusTabData, new Comparator<String>(){
						@Override
						public int compare(String o1, String o2){
							try {
								int a = Integer.parseInt(o1);
								int b = Integer.parseInt(o2);		
								
								if(a>b){
									return 1;
								}
								else if (a==b){
									return 0;
								}
								else {
									return -1;
								}					
				            }catch(Exception e){
				            	try {
									throw new Exception(e);
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									return -1;
								}
				            }
						}
					});
				}
				else {
					Collections.sort(orderedStatusTabData);
				}
				
				for(int n=0;n<countBlank;n++){
					orderedStatusTabData.add("");
				}			
				
				if (!orderedStatusTabData.equals(statusTabData1)) {
					wrongAscendingOrderColumn.add(columnNameList.get(i));
				}
				if (testStep[4] == null) {
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				// Descending order test
				clickElement("ColumnButton", "Button");
				waitForElementToBeVisible(60, "ColumnInDescendingOrder");
				waitExplicit(2000);
				if (columnNameList.get(i).equals("Description")) {
					statusTabElement = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					statusTabElement = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					statusTabElement = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Workorder #")) {
					statusTabElement = findElements(getLocator("WorkorderValue"));
				} else if (columnNameList.get(i).equals("Start")) {
					statusTabElement = findElements(getLocator("StartDateValue"));
				} else if (columnNameList.get(i).equals("End")) {
					statusTabElement = findElements(getLocator("EndDateValue"));
				} else if (columnNameList.get(i).equals("Summary")) {
					statusTabElement = findElements(getLocator("SummaryValue"));
				}

				ArrayList<String> statusTabData2 = new ArrayList<String>();
				//orderedStatusTabData = new ArrayList<String>();
				for (WebElement wElement : statusTabElement) {
					statusTabData2.add(wElement.getText().trim());
					//orderedStatusTabData.add(wElement.getText().trim());
				}
				
				Collections.reverse(orderedStatusTabData);
				//Collections.sort(orderedStatusTabData, Collections.reverseOrder());
				if (!orderedStatusTabData.equals(statusTabData2)) {
					wrongDescendingOrderColumn.add(columnNameList.get(i));
				}
				testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
			if (wrongAscendingOrderColumn.size() > 0 && wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order and "
						+ wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongAscendingOrderColumn.size() > 0) {
				testStep[2] = wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			clickElement("StatusTab", "Link");
			verifyAttributeContainsText("StatusTab", "class", "active");
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for listview History tab sorting functionality cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Method to verify filters functionality on Device List Screen
	 * 
	 * @param columnName
	 *            - List of all the columns to be tested
	 * @param columnValue
	 *            - List of all columns values for which column needs to be
	 *            filtered. 
	 *            N - No need to enter value for Last Connected Date
	 * @param commonColumn
	 *            - Name of all the column common in all the tabs
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewMultiFiltering(String columnName, String columnValue, String commonColumn)
			throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify Add/Remove filters to/from the each column on Device List Screen";
		testStep[1] = "ListView Filters Functionality should be as expected";
		testStep[2] = "ListView Filters Functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<String> columnValueList = Arrays.asList(columnValue.split(","));
		List<WebElement> column = new ArrayList<WebElement>();
		List<String> wrongValueColumn = new ArrayList<String>();
		String failScreenshots = null;
		String passScreenshots = null;

		String lastConnectedValue = null;
		String startDateValue = null;
		String endDateValue = null;
		String usesXdaysValue = null;
		String ageValue = null;
		int count = 0;

		int passed = 0;
		try {
			if (columnNameList.size() != columnValueList.size()) {
				testStep[2] = "Please enter the values for corresponding to the column name";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

			waitForElementToBeVisible(60, "SerialNoValue");

			// Adding Filter
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				count = findElements(getLocator("SerialNoValue")).size();

				if (columnNameList.get(i).equals("Status")) {
					inputData("Status Input", "text", columnValueList.get(i));
					if (count < findElements(getLocator("SerialNoValue")).size()) {
						testStep[2] = "Filter functionality is not working as expected";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					count = findElements(getLocator("SerialNoValue")).size();

				} else if (columnNameList.get(i).equals("Last Connected")) {
					List<WebElement> LastConnectedColumn = findElements(getLocator("LastConnected"));

					for (int k = 0; k < LastConnectedColumn.size(); k++) {
						lastConnectedValue = LastConnectedColumn.get(k).getText().trim();
						if (lastConnectedValue.length() > 5) {
							lastConnectedValue = lastConnectedValue.substring(0, lastConnectedValue.indexOf(" "));
							break;
						}
					}
					inputData("ColumnInput", "text", lastConnectedValue);
					if (count < findElements(getLocator("SerialNoValue")).size()) {
						testStep[2] = "Filter functionality is not working as expected";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					count = findElements(getLocator("SerialNoValue")).size();

				} else if (columnNameList.get(i).equals("Uses Last X days")) {
					List<WebElement> usesXdaysColumn = findElements(getLocator("LastXdaysValue"));

					for (int k = 0; k < usesXdaysColumn.size(); k++) {
						usesXdaysValue = usesXdaysColumn.get(k).getText().trim();
						if (usesXdaysValue.length() > 0) {
							break;
						}
					}
					inputData("UsesLastXdaysInput", "text", usesXdaysValue);
					if (count < findElements(getLocator("SerialNoValue")).size()) {
						testStep[2] = "Filter functionality is not working as expected";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					count = findElements(getLocator("SerialNoValue")).size();

				} else if (columnNameList.get(i).equals("Age (Years)")) {
					List<WebElement> ageColumn = findElements(getLocator("AgeValue"));

					for (int k = 0; k < ageColumn.size(); k++) {
						ageValue = ageColumn.get(k).getText().trim();
						if (ageValue.length() > 0) {
							break;
						}
					}
					inputData("ColumnInput", "text", ageValue);
					if (count < findElements(getLocator("SerialNoValue")).size()) {
						testStep[2] = "Filter functionality is not working as expected";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					count = findElements(getLocator("SerialNoValue")).size();

				} else if (columnNameList.get(i).equals("Start")) {
					List<WebElement> date = findElements(getLocator("StartDateValue"));

					for (int k = 0; k < date.size(); k++) {
						startDateValue = date.get(k).getText().trim();
						if (startDateValue.length() > 5) {
							break;
						}
					}
					inputData("ColumnInput", "text", startDateValue);
					if (count < findElements(getLocator("SerialNoValue")).size()) {
						testStep[2] = "Filter functionality is not working as expected";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					count = findElements(getLocator("SerialNoValue")).size();

				} else if (columnNameList.get(i).equals("End")) {
					List<WebElement> date = findElements(getLocator("EndDateValue"));

					for (int k = 0; k < date.size(); k++) {
						endDateValue = date.get(k).getText().trim();
						if (endDateValue.length() > 5) {
							break;
						}
					}
					inputData("ColumnInput", "text", endDateValue);
					if (count < findElements(getLocator("SerialNoValue")).size()) {
						testStep[2] = "Filter functionality is not working as expected";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					count = findElements(getLocator("SerialNoValue")).size();

				} else {
					inputData("ColumnInput", "text", columnValueList.get(i));
					if (count < findElements(getLocator("SerialNoValue")).size()) {
						testStep[2] = "Filter functionality is not working as expected";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					count = findElements(getLocator("SerialNoValue")).size();
				}

				if (columnNameList.get(i).equals("Description")) {
					column = findElements(getLocator("DescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					column = findElements(getLocator("SerialNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					column = findElements(getLocator("FacilityValue"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					column = findElements(getLocator("LastSeen"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					column = findElements(getLocator("LastConnected"));
				} else if (columnNameList.get(i).equals("Status")) {
					column = findElements(getLocator("Status Value"));
				} else if (columnNameList.get(i).equals("Workorder #")) {
					column = findElements(getLocator("WorkorderValue"));
				} else if (columnNameList.get(i).equals("Start")) {
					column = findElements(getLocator("StartDateValue"));
				} else if (columnNameList.get(i).equals("End")) {
					column = findElements(getLocator("EndDateValue"));
				} else if (columnNameList.get(i).equals("Summary")) {
					column = findElements(getLocator("SummaryValue"));
				} else if (columnNameList.get(i).equals("Uses Last X days")) {
					column = findElements(getLocator("LastXdaysValue"));
				} else if (columnNameList.get(i).equals("Age (Years)")) {
					column = findElements(getLocator("AgeValue"));
				} else {
					testStep[2] = "Please enter the column name in the input";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				if (column.size() == 0) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
					wrongValueColumn.add(columnNameList.get(i));
				}
				for (WebElement wElement : column) {
					if ((columnNameList.get(i).equals("Last Connected")
							&& wElement.getText().contains(lastConnectedValue))
							|| (columnNameList.get(i).equals("Start") && wElement.getText().contains(startDateValue))
							|| (columnNameList.get(i).equals("End") && wElement.getText().contains(endDateValue))
							|| (columnNameList.get(i).equals("Uses Last X days")
									&& wElement.getText().contains(usesXdaysValue))
							|| (columnNameList.get(i).equals("Age (Years)") && wElement.getText().contains(ageValue))) {

						continue;
					} else if (!wElement.getText().contains(columnValueList.get(i))) {
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						wrongValueColumn.add(columnNameList.get(i));
						break;
					}
				}

				if (passScreenshots == null) {
					passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
			}
			String[] common = commonColumn.split(",");
			passed = passed + 1;
			WebElement webElement = findElement(getLocator("StatusTab"));
			String statusattval = webElement.getAttribute("class");
			webElement = findElement(getLocator("UsageTab"));
			String usageattval = webElement.getAttribute("class");
			webElement = findElement(getLocator("HistoryTab"));
			String historyattval = webElement.getAttribute("class");
			if (statusattval.contains("active")) {

				clickElement("UsageTab", "link");
				verifyAttributeContainsText("UsageTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
				clickElement("HistoryTab", "link");
				verifyAttributeContainsText("HistoryTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
				clickElement("StatusTab", "link");
				verifyAttributeContainsText("StatusTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
			} else if (usageattval.contains("active")) {

				clickElement("StatusTab", "link");
				verifyAttributeContainsText("StatusTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
				clickElement("HistoryTab", "link");
				verifyAttributeContainsText("HistoryTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
				clickElement("UsageTab", "link");
				verifyAttributeContainsText("UsageTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
			} else if (historyattval.contains("active")) {

				clickElement("StatusTab", "link");
				verifyAttributeContainsText("StatusTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
				clickElement("UsageTab", "link");
				verifyAttributeContainsText("UsageTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
				clickElement("HistoryTab", "link");
				verifyAttributeContainsText("HistoryTab", "class", "active");
				for (int k = 0; k < common.length; k++) {
					config.setMapGlobalVariable("ColumnName", common[k]);
					verifyAttributeContainsText("ColumnInput", "class", "not-empty");
				}
			}

			passed = passed + 1;
			// Removing Filter
			clickElement("ClearFilter");

			if (failScreenshots != null) {
				testStep[2] = wrongValueColumn.toString() + "don't have appropriate filter result";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (passed != 2) {
				testStep[2] = "Please make sure that appropriate columns names are provided or the application is not functioning properly. Refer screenshot for visual evidence.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			} else if (testStep[4] == null) {
				testStep[2] = "Verification for Add/Remove filters to/from the each column on Device List Screen cannot be done because of an Exception. Please refer Logs/info.log for more details";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	public void waitIfSpinnerDisplays()
	{
		try
		{
			//WebElement webElement = findElement(getLocator("MagnifierProgressingInMiddle"));
			if(isElementExists("//*[@id='ho-sem-partial-container' and not(contains(@class,'ng-hide'))]"))
			{
				//waitForElementToBeInvisible("MagnifierProgressingInMiddle");
				waitExplicit(4000);
				
			}
			else
			{
				waitExplicit(4000);
			
			}
		}
		
		catch(Exception e)
		{
			
		}
	}
	
	/**
	 * Method to verify that the sorting for the Device ListView is persisted even if user
	 * navigate between different screens
	 * 
	 * @param commonColumn
	 *            - Name of all the column common in all the tabs
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewSortingPersistence(String commonColumn) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that sorting for the Device ListView is persisted even if user navigates between different screens";
		testStep[1] = "ListView Sorting Persistence should be present";
		testStep[2] = "ListView Sorting Persistence is present";
		testStep[4] = null;
		String status = "Pass";
		try {
			waitIfSpinnerDisplays();
			String[] common = commonColumn.split(",");
			WebElement webElement = findElement(getLocator("StatusTab"));
			String statusattval = webElement.getAttribute("class");
			webElement = findElement(getLocator("UsageTab"));
			String usageattval = webElement.getAttribute("class");
			webElement = findElement(getLocator("HistoryTab"));
			String historyattval = webElement.getAttribute("class");
			if (statusattval.contains("active")) {
				for (int k = 0; k < common.length; k++) {
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					config.setMapGlobalVariable("ColumnName", common[k]);
					clickElement("ColumnButton", "Button");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("UsageTab", "link");
					verifyAttributeContainsText("UsageTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("HistoryTab", "link");
					verifyAttributeContainsText("HistoryTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("StatusTab", "link");
					verifyAttributeContainsText("StatusTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
			} else if (usageattval.contains("active")) {
				for (int k = 0; k < common.length; k++) {
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					config.setMapGlobalVariable("ColumnName", common[k]);
					clickElement("ColumnButton", "Button");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("StatusTab", "link");
					verifyAttributeContainsText("StatusTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("HistoryTab", "link");
					verifyAttributeContainsText("HistoryTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("UsageTab", "link");
					verifyAttributeContainsText("UsageTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
			} else if (historyattval.contains("active")) {
				for (int k = 0; k < common.length; k++) {
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					config.setMapGlobalVariable("ColumnName", common[k]);
					clickElement("ColumnButton", "Button");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("StatusTab", "link");
					verifyAttributeContainsText("StatusTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("UsageTab", "link");
					verifyAttributeContainsText("UsageTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					clickElement("HistoryTab", "link");
					verifyAttributeContainsText("HistoryTab", "class", "active");
					verifyPresence("ColumnInAscendingOrder");
					testStep[4] = testStep[4] + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
			}
		} catch (Exception e) {
			testStep[2] = "ListView Sorting Persistence is not present. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify data for all the devices in the Maintenance Cart
	 * @param noRecommDevices
	 *        List of serial number of devices added to cart, for which Recommendation should not be shown 
	 *        and they should be separated by comma. For eg, 1000000011,1000000012        
	 * @param loanerDevices
	 *        List of serial number of loaner devices added to cart and separated by comma. For eg, 
	 *        1000000011,1000000012  
	 * @return String[]
	 * 		   Returns the result of the execution
	 * @throws TestScriptException
	 */
	
	public String[] verifyMaintCartDeviceData(String noRecommDevices, String loanerDevices) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that each device is displayed in the Maintenance Cart with the required information.";
		testStep[1] = "Each device should be displayed in the Maintenance Cart with the required information.";
		testStep[2] = "Each device is displayed in the Maintenance Cart with the required information.";
		testStep[4] = null;
		String status = "Pass";
		try {
			List<String> noRecommDeviceList = Arrays.asList(noRecommDevices.split(","));
			List<String> loanerDeviceList = Arrays.asList(loanerDevices.split(","));
			boolean faultyDevice = false;
			boolean noRecommDevice = false;
			boolean loanerDevice = false;
			
			
			List<String> faultyDevices = new ArrayList<String>();
			List<WebElement> deviceList = findElements(getLocator("MaintCart_SerialNo"));
			List<WebElement> loanerList = findElements(getLocator("MaintCart_LoanerSerialNo"));
			deviceList.addAll(loanerList);			
			List<String> serialNos = new ArrayList<String>();
			for(WebElement element: deviceList){
				String elementText = element.getText();
				Pattern pattern = Pattern.compile("[0-9]+");
				Matcher matcher = pattern.matcher(elementText);
				if(matcher.find()){
					serialNos.add(matcher.group(0));
				}
			}	
			
			WebElement devProdImage = null;
			WebElement devProdDesc = null;
			WebElement devSerialNo = null;
			WebElement devProdNo = null;
			WebElement devFacName = null;
			WebElement devMaintRecom = driver.findElement(By.id("CartView_MaintCart_Label"));
			WebElement devMaintRecommRadio= devMaintRecom;
			WebElement devMaintUnplanRadio= devMaintRecom;
			WebElement devLoanerReturnRadio= devMaintRecom;
			WebElement devLoanerUnPlanRadio= devMaintRecom;
					
			for(String serialNo: serialNos){
				noRecommDevice = false;
				loanerDevice = false;
				faultyDevice = false;
				
							
				config.setMapGlobalVariable("DevSerialNumber", serialNo);
				verifyPresence("MaintCart_DevCartContainer");
				if(testStep[4]==null){
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				else{
					testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				try{
					
				devProdImage = findElement(getLocator("MaintCart_DeviceProdImage"));
				devProdDesc = findElement(getLocator("MaintCart_DeviceProdDesc"));
				devSerialNo= findElement(getLocator("MaintCart_DeviceSerialNo"));
				devProdNo = findElement(getLocator("MaintCart_DeviceProdNo"));
				devFacName = findElement(getLocator("MaintCart_DeviceFacName"));
							
				if(!noRecommDeviceList.contains(serialNo) && !loanerDeviceList.contains(serialNo)){
					devMaintRecom = driver.findElement(getLocator("MaintCart_DevMaintRecomm"));
				}
				else{
					noRecommDevice = true;
				}
				
				if(loanerDeviceList.contains(serialNo)){
					devLoanerReturnRadio = driver.findElement(getLocator("MaintCart_DevLoanerReturnRadio"));
					devLoanerUnPlanRadio = driver.findElement(getLocator("MaintCart_DevLoanerUnPlanRadio"));
					loanerDevice = true;
				}
				else{
					devMaintRecommRadio = driver.findElement(getLocator("MaintCart_DevMaintRecommRadio"));
					devMaintUnplanRadio = driver.findElement(getLocator("MaintCart_DevMaintUnPlanRadio"));
					}
				}catch(Exception e){
					testStep[2] = "Maintenance Cart cannot be verified because of some exception. Please refer Logs/info.log for more details";
					status = "Fail";
					methodFailureMessage = testStep[2] + " " + e.getMessage();
					throw new Exception();
				}
				
				//
				try{
				
				if(devProdImage.isDisplayed() && devProdDesc.isDisplayed() && devSerialNo.isDisplayed() && devProdNo.isDisplayed() && devFacName.isDisplayed()){
					if(!loanerDevice && !noRecommDevice){
						if(devMaintRecom.isDisplayed() && devMaintRecommRadio.isDisplayed() && devMaintUnplanRadio.isDisplayed() && (devMaintRecommRadio.getAttribute("class").contains("checked") || devMaintUnplanRadio.getAttribute("class").contains("checked"))){
							continue;
						}
						else{
							faultyDevice = true;
							}
					}
					else if(!loanerDevice && noRecommDevice){
						if(devMaintRecommRadio.isDisplayed() && devMaintUnplanRadio.isDisplayed() && (devMaintRecommRadio.getAttribute("class").contains("checked") || devMaintUnplanRadio.getAttribute("class").contains("checked"))){
							continue;
						}
						else{
							faultyDevice = true;
							}
					}
					else if(loanerDevice){
						if(devLoanerReturnRadio.isDisplayed() && devLoanerUnPlanRadio.isDisplayed() && (devLoanerReturnRadio.getAttribute("class").contains("checked") || devLoanerUnPlanRadio.getAttribute("class").contains("checked"))){
							continue;
						}
						else{
							faultyDevice = true;
							}
					}
				}	
				else{
					faultyDevice = true;
					}
				}catch(NoSuchElementException nsee){
					faultyDevice = true;
				}catch(StaleElementReferenceException sere){
					faultyDevice = true;
				}
				
				if(faultyDevice){
					faultyDevices.add(serialNo);
				}					
			}			
			if(!faultyDevices.isEmpty()){
				testStep[2] = "Maintenance cart does not have all the devices with correct information, faulty device serial nos are "+faultyDevices.toString();
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}			
		}catch (Exception e) {
			if(testStep[4]==null){
			testStep[2] = "Maintenance Cart cannot be verified because of some exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify data for all the devices in the Search Results
	 * @return String[]
	 * 		   Returns the result of the execution
	 * @throws TestScriptException
	 */	
	public String[] verifySearchResultsDeviceData() throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that each device is displayed in the Search Results with the required information.";
		testStep[1] = "Each device should be displayed in the Search Results with the required information.";
		testStep[2] = "Each device is displayed in the Search Results with the required information.";
		testStep[4] = null;
		String status = "Pass";
		try {
			
			List<String> faultyDevices = new ArrayList<String>();
			boolean faultyDevice = false;
			
			List<WebElement> deviceList = findElements(getLocator("DeviceSearchResultSerialNo"));
			List<String> serialNos = new ArrayList<String>();
			Pattern pattern = Pattern.compile("[0-9]+");
			Matcher matcher;
			String prodNo = "";
						
			for(WebElement element: deviceList){
				String elementText = element.getText();
				matcher = pattern.matcher(elementText);
				if(matcher.find()){
					serialNos.add(matcher.group(0));
				}
			}	
			
			WebElement devProdImage ;
			WebElement devProdDesc;
			WebElement devProdNo;
			WebElement devFacName;
			WebElement devProdGrp;
			WebElement devSerialNo;
			
			pattern = Pattern.compile("[0-9]+[-][0-9]+[-][0-9]+");
					
			for(String serialNo: serialNos){
				faultyDevice = false;
				
				config.setMapGlobalVariable("SearchResultDevSerialNo", serialNo);
				verifyPresence("SearchResultTile");
				if(testStep[4]==null){
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				else{
					testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}			
				
				//
				try{
				
					devProdDesc = findElement(getLocator("IndDevSearchProductDesc"));
					devSerialNo= findElement(getLocator("IndDevSearchResultSerialNo"));
					devProdNo = findElement(getLocator("IndDevSearchResultPartNo"));
					devFacName = findElement(getLocator("IndDevSearchProductFacName"));	
										
					matcher = pattern.matcher(devProdNo.getText());
					if(matcher.find()){
						prodNo = matcher.group(0);
					}
					
					config.setMapGlobalVariable("SearchView_productNumber", prodNo);
					
					devProdImage = findElement(getLocator("IndDevSearchProductImage"));
					devProdGrp = findElement(getLocator("IndDevSearchProductGrpName"));							
					
				if(devProdDesc.isDisplayed() && devSerialNo.isDisplayed() && devProdNo.isDisplayed() && devFacName.isDisplayed() && devProdImage.isDisplayed() && devProdGrp.isDisplayed()){
						continue;
				}
				else{
					faultyDevice = true;
				}
					
				}catch(NoSuchElementException nsee){
					faultyDevice = true;
				}catch(StaleElementReferenceException sere){
					faultyDevice = true;
				}
				
				if(faultyDevice){
					faultyDevices.add(serialNo);
				}					
			}			
			if(!faultyDevices.isEmpty()){
				testStep[2] = "Search Results does not have all the devices with correct information, faulty device serial nos are "+faultyDevices.toString();
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}			
		}catch (Exception e) {
			if(testStep[4]==null){
			testStep[2] = "Search Results cannot be verified because of some exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
		
	
	/** Method to click on Dynamic Search Result More
	 * @param element
	 * 		  WebElement for More on which keyboard operations needs to be provided.
	 * @return String[]
	 * 		  Returns the result of the execution.
	 * @throws TestScriptException
	 */
	public String[] clickDynamicResultMore(String element, String keyBoardAction) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that user can click on More for the dynamic search results.";
		testStep[1] = "More from the dynamic search results should be clicked.";
		testStep[2] = "More from the dynamic search results is clicked.";
		testStep[4] = null;
		String status = "Pass";
		try {
		WebElement wElement = findElement(getLocator(element));
			/*String[] actions = keyBoardAction.split(",");
			for(String action:actions){
				if(action.contains("DOWN"))
					wElement.sendKeys(Keys.DOWN);
				else
					wElement.sendKeys(Keys.ENTER);
			}*/
			
			/*			wElement.sendKeys(Keys.DOWN);
			wElement.sendKeys(Keys.DOWN);
			wElement.sendKeys(Keys.DOWN);
			wElement.sendKeys(Keys.DOWN);
			wElement.sendKeys(Keys.DOWN);
			wElement.sendKeys(Keys.DOWN);			
			wElement.sendKeys(Keys.ENTER);*/
		
			wElement.sendKeys(Keys.ARROW_DOWN);
			wElement.sendKeys(Keys.ARROW_DOWN);
			wElement.sendKeys(Keys.ARROW_DOWN);
			wElement.sendKeys(Keys.ARROW_DOWN);
			wElement.sendKeys(Keys.ARROW_DOWN);
			wElement.sendKeys(Keys.ARROW_DOWN);
			waitExplicit(1000);
			wElement.sendKeys(Keys.RETURN);
			
							
			//wElement.sendKeys(Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ENTER);
		
		}catch (Exception e) {
			if(testStep[4]==null){
			testStep[2] = "More cannot be clicked because of some exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/** Method to click on Dynamic Search Result Data
	 * @param element
	 * 		  WebElement for First Result data on which keyboard operations needs to be provided.
	 * @return String[]
	 * 		  Returns the result of the execution.
	 * @throws TestScriptException
	 */
	public String[] clickFirstDynamicResult(String element) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that user  click on First Element for the dynamic search results.";
		testStep[1] = "First Result data from the dynamic search results should be clicked.";
		testStep[2] = "First Result Data from the dynamic search results is clicked.";
		testStep[4] = null;
		String status = "Pass";
		try {
		WebElement wElement = findElement(getLocator(element));
		//wElement.click();
			wElement.sendKeys(Keys.ARROW_DOWN);
			waitExplicit(1000);
			wElement.sendKeys(Keys.RETURN);
		}catch (Exception e) {
			if(testStep[4]==null){
			testStep[2] = "First Result data cannot be clicked because of some exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	public void waitUntilMagnifierInvisible()
	{
		try
		{
			//WebElement webElement = findElement(getLocator("MagnifierProgressingInMiddle"));
			if(isElementExists("//div[@id='ho-sem-search-icon-container']/i"))
			{
				//waitForElementToBeInvisible("MagnifierProgressingInMiddle");
				waitExplicit(4000);
				
			}
			else
			{
				waitExplicit(4000);
			
			}
		}
		
		catch(Exception e)
		{
			
		}
	}
	
	/**
	 * Method to verify Dynamic Search Result as per the format and value
	 * 
	 * @param format
	 *            xxxx-xxx-xxx - Product Number format i.e 4505-000-000
	 *            xxxxxxxxxx - Product Number format i.e 4505000000 xxxx-xxx -
	 *            Product Number format i.e 4505-000 xxxxxxx - Product Number
	 *            format i.e 4505000 xx - Product Number format i.e 45
	 *            MoreResult - Product Number >= 3 characters in the search
	 *            criteria i.e 7208 (More.. option will show up)
	 * 
	 * @param value
	 *            Value as per the defined format
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyDynamicSearchResult(String format, String value) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify the Dynamic Search Result as per the format and value";
		testStep[1] = "Dynamic Search Result should be present as per the format and value";
		testStep[2] = "Dynamic Search Result is present as per the format and value";
		testStep[4] = null;
		String status = "Pass";
		Boolean flag = false;
		try {
			waitForElementToBeVisible(60, "SEM_SearchBox");
			inputData("SEM_SearchBox", "Text", value);
			waitForElementInVisible("SearchSpinner");
			if (format.equals("xx")) {
				waitExplicit(5000);
				WebElement webElement = driver.findElement(getLocator("SearchResultData"));
				if (webElement != null) {
					flag = true;
				}
			} else {
				waitExplicit(3000);
				waitForElementToBeVisible(60, "SearchResultData");
				List<WebElement> searchDataElement = findElements(getLocator("SearchResultData"));
				ArrayList<String> searchData = new ArrayList<String>();

				for (WebElement wElement : searchDataElement) {
					searchData.add(wElement.getText().trim());
				}

				if (format.equals("xxxx-xxx-xxx") || format.equals("xxxx-xxx")) {
					for (String str : searchData) {
						if (!str.contains(value)) {
							flag = true;
							break;
						}
					}
				} else if (format.equals("xxxxxxxxxx") || format.equals("xxxxxxx")) {
					for (String str : searchData) {
						str = str.replaceAll("-", "");
						if (!str.contains(value)) {
							flag = true;
							break;
						}
					}
				} else if (format.equals("MoreResult")) {
					for (int i=0;i<searchData.size();i++) {
						if ((searchData.get(i).contains("More")) || (searchData.get(i).contains(value))) {
							continue;
						}
						else{
							flag = true;
							break;
						}
					}
				} else {
					testStep[2] = "Please enter correct format";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}

			if (flag) {
				testStep[2] = "Dynamic Search Result is not as expected, please refer screenshot";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Dynamic Search Result cannot be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify Listview devices have default sorting based on Product
	 * group name, then Description and then Serial number
	 * 
	 * @param productgrpSeq
	 *            Sequence in which the Product Group should be present
	 * 
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyListViewDefaultSorting(String productgrpSeq) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that Listview devices have default sorting based on Product group name, Description and Serial number";
		testStep[1] = "Listview devices should have default sorting based on Product group name, Description and Serial number";
		testStep[2] = "Listview devices have default sorting based on Product group name, Description and Serial number";
		testStep[4] = null;
		String status = "Pass";
		String[] pgSeq = productgrpSeq.split(",");
		String presentTab = "";
		//List<WebElement> historyTabStartDate = new ArrayList<WebElement>();
		List<WebElement> historyTabEndDate = new ArrayList<WebElement>();
		try {
			WebElement webElement = findElement(getLocator("StatusTab"));
			String statusattval = webElement.getAttribute("class");
			webElement = findElement(getLocator("UsageTab"));
			String usageattval = webElement.getAttribute("class");
			webElement = findElement(getLocator("HistoryTab"));
			String historyattval = webElement.getAttribute("class");

			if (statusattval.contains("active")) {
				presentTab = "Status Tab";
			} else if (usageattval.contains("active")) {
				presentTab = "Usage Tab";
			} else if (historyattval.contains("active")) {
				presentTab = "History Tab";
				//historyTabStartDate = findElements(getLocator("StartDateValue"));
				historyTabEndDate = findElements(getLocator("EndDateValue"));
			} else {
				testStep[2] = "Please open Listview Tab before calling this method";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

			waitForElementToBeVisible(60, "SerialNoValue");
			List<WebElement> listViewImgs = findElements(getLocator("ListviewProductGroupImg"));
			List<WebElement> listViewDesc = findElements(getLocator("DescriptionValue"));
			List<WebElement> listViewSerial = findElements(getLocator("SerialNoValue"));

			ArrayList<String> Img = new ArrayList<String>();
			ArrayList<String> Description = new ArrayList<String>();
			ArrayList<String> Serial = new ArrayList<String>();
			String imgSource;
			HashMap<String, List<String>> descriptionList = new HashMap<String, List<String>>();
			HashMap<String, List<String>> serialList = new HashMap<String, List<String>>();

			for (WebElement wElement : listViewImgs) {
				imgSource = wElement.getAttribute("data-ng-src");
				imgSource = imgSource.substring(imgSource.lastIndexOf("/") + 1, imgSource.lastIndexOf("."));
				Img.add(imgSource);
			}

			for (WebElement wElement : listViewSerial) {
				Serial.add(wElement.getText().trim());
			}

			for (WebElement wElement : listViewDesc) {
				Description.add(wElement.getText().trim());

			}

			ArrayList<String> temp = new ArrayList<String>();
			ArrayList<String> displayedpg = new ArrayList<String>();
			String pg = Img.get(0);
			for (int i = 0; i < Img.size(); i++) {
				if (!pg.equals(Img.get(i))) {
					descriptionList.put(pg, temp);
					displayedpg.add(pg);
					temp = new ArrayList<String>();
					temp.add(Description.get(i));
					pg = Img.get(i);
				} else {
					temp.add(Description.get(i));
				}
			}
			descriptionList.put(pg, temp);
			displayedpg.add(pg);

			temp = new ArrayList<String>();
 
			ArrayList<String> displayedDescription = new ArrayList<String>();

			String desc = Description.get(0);
			for (int i = 0; i < Description.size(); i++) {
				if (!desc.equals(Description.get(i))) {
					serialList.put(desc, temp);
					displayedDescription.add(desc);
					temp = new ArrayList<String>();
					temp.add(Serial.get(i));
					desc = Description.get(i);
				} else {
					temp.add(Serial.get(i));
				}
			}
			serialList.put(desc, temp);
			displayedDescription.add(desc);

			if (displayedpg.size() != pgSeq.length) {
				testStep[2] = "Product Groups displayed on the application are not as expected. Please refer screenshot for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

			if (!displayedpg.toString().equalsIgnoreCase(Arrays.asList(pgSeq).toString())) {
				testStep[2] = presentTab + " have 1st Level of sorting is incorrect :" + Img.toString();
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

			int c = 0;
			for (String str : displayedpg) {
				List<String> sort = descriptionList.get(str);
				Collections.sort(sort);
				for (String des : sort) {
					if (!Description.get(c++).equals(des)) {
						testStep[2] = presentTab
								+ " have 2nd Level of sorting is incorrect, please refer the screenshot.";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
				}
			}

			c = 0;
			for (String str : displayedDescription) {
				List<String> sort = serialList.get(str);
				Collections.sort(sort);
				for (String des : sort) {
					if (!Serial.get(c++).equals(des)) {
						testStep[2] = presentTab
								+ " have 3rd Level of sorting is incorrect, please refer the screenshot.";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
				}
			}
			
			if(presentTab.equals("History Tab")) {
				//HashMap<String, List<String>> startDateList = new HashMap<String, List<String>>();
				HashMap<String, List<String>> endDateList = new HashMap<String, List<String>>();
				
				//ArrayList<String> Start = new ArrayList<String>();
				ArrayList<String> End = new ArrayList<String>();
				
				/*for (WebElement wElement : historyTabStartDate) {
					Start.add(wElement.getText().trim());
				}*/
				
				for (WebElement wElement : historyTabEndDate) {
					End.add(wElement.getText().trim());
				}
				
				temp = new ArrayList<String>();

				ArrayList<String> displayedSerialNo = new ArrayList<String>();

				String serial = Serial.get(0);
				for (int i = 0; i < Serial.size(); i++) {
					if (!serial.equals(Serial.get(i))) {
						//startDateList.put(serial, temp);
						endDateList.put(serial, temp);
						displayedSerialNo.add(serial);
						temp = new ArrayList<String>();
						//temp.add(Start.get(i));
						temp.add(End.get(i));
						serial = Serial.get(i);
					} else {
						//temp.add(Start.get(i));
						  temp.add(End.get(i));
					}
				}
				//startDateList.put(serial, temp);
				endDateList.put(serial, temp);
				
				displayedSerialNo.add(serial);
				
				c = 0;
				for (String strdate : displayedSerialNo) {
					//List<String> sort = startDateList.get(strdate);
					List<String> sort = endDateList.get(strdate);
					
					Collections.sort(sort, Collections.reverseOrder());
					for (String str : sort) {
						//if (!Start.get(c++).equals(str)) {
						if (!End.get(c++).equals(str)) {
							testStep[2] = presentTab
									+ " have 4th Level of sorting is incorrect, please refer the screenshot";
							status = "Fail";
							testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
							methodFailureMessage = testStep[2];
							throw new Exception();
						}
					}
				}
			}
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Listview devices default sorting cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/** Method to verify that Search Result label contains the Search Criteria
	 * @param element
	 * 		  UI element identifier for Search Result Label
	 * @param expectedSearchCriteria
	 * 		  Expected Search Criteria in the Search Result Label
	 * @return String[]
	 * 		  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifySearchResultCritLabel(String element, String expectedSearchCriteria) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that Search Result label contains the Search Criteria";
		testStep[1] = "Search Result label should contain the Search Criteria";
		testStep[2] = "Search Result label contains the Search Criteria";
		testStep[4] = null;
		String status = "Pass";
		try {
		WebElement wElement = findElement(getLocator(element));
		String text = wElement.getText().trim();
		String actualSearchCriteria = text.substring(text.indexOf("\"")+1, text.lastIndexOf("\""));
		if(!actualSearchCriteria.equals(expectedSearchCriteria)){
			testStep[2] = "Search Result label does not contain the expected search criteria";
			status = "Fail";
			methodFailureMessage = testStep[2];
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		}		
		testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());			
		
		}catch (Exception e) {
			if(testStep[4]==null){
			testStep[2] = "Search Result Criteria cannot be verified because of some exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify the individual column filters functionality on Loaner Device List Screen
	 * 
	 * @param columnName
	 *            - List of all the columns to be tested.
	 *            Please note we should not provide any column name which will have blank value 
	 *            for all the records in the list view 
	 * @param columnValue
	 *            - List of all columns values for which column needs to be
	 *            filtered and N for Last Connected Date and Status since its value will be 
	 *            picked on runtime for filtering
	 *            Please note that method is expecting atleast 1 result for any column filtering
	 *            so please provide the column filter input accordingly.
	 * @return String[]
	 *           - Returns the result of the execution           
	 * @throws TestScriptException
	 */
	public String[] verifyLoanerListViewFilters(String columnName, String columnValue) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify Add/Remove filters to/from the each column on Loaner Device List Screen";
		testStep[1] = "Loaners ListView Filters Functionality should be as expected";
		testStep[2] = "Loaners ListView Filters Functionality is as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<String> columnValueList = Arrays.asList(columnValue.split(","));
		List<WebElement> column = new ArrayList<WebElement>();
		List<String> wrongValueColumn = new ArrayList<String>();
		String failScreenshots = null;
		String passScreenshots = null;
		String listViewColumnValue = null;
		String lastConnValue = null;
		String statusValue = null;
		
		
		int count = 0;
		try {
			if (columnNameList.size() != columnValueList.size()) {
				testStep[2] = "Please enter the values for corresponding to the column name";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			// Count of result before adding filter
			waitForElementToBeVisible(60, "LoanerSerialNoValue");
			column = findElements(getLocator("LoanerSerialNoValue"));
			count = column.size();

			// Adding Filter
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				if (columnNameList.get(i).equals("Last Connected")) {
					List<WebElement> lastConnectedColumn = findElements(getLocator("LoanerLastConnected"));
					List<String> lastConnectedValues = new ArrayList<String>();
					
					for(WebElement wElement: lastConnectedColumn)
					{
						lastConnectedValues.add(wElement.getText().trim());
					}					
										
					int equalDateCount = 0;
					int greaterThanDateCount = 0;
					int lessThanDateCount = 0;
					boolean elementFound = false;
					
					for (int k = 0; k < lastConnectedValues.size(); k++) {						
						listViewColumnValue = lastConnectedValues.get(k);
						if (listViewColumnValue.length() > 5) {
							listViewColumnValue = listViewColumnValue.substring(0, listViewColumnValue.indexOf(" "));
							inputData("ColumnInput", "text", listViewColumnValue);
							clickElement("LastConnectedFilterIcon", "link");
							waitForElementToBeVisible(60, "LoanerLastConnEqualsFilter");
							clickElement("LoanerLastConnEqualsFilter", "link");
							waitExplicit(3000);
							waitForElementToBeVisible(60, "LoanerSerialNoValue");
							column = findElements(getLocator("LoanerSerialNoValue"));
							equalDateCount = column.size();
														
							clickElement("LastConnectedFilterIcon", "link");
							waitForElementToBeVisible(60, "LoanerLastConnGreaterThanFilter");
							clickElement("LoanerLastConnGreaterThanFilter", "link");
							waitExplicit(3000);
							column = findElements(getLocator("LoanerSerialNoValue"));
							greaterThanDateCount = column.size();
							
							clickElement("LastConnectedFilterIcon", "link");
							waitForElementToBeVisible(60, "LoanerLastConnLessThanFilter");
							clickElement("LoanerLastConnLessThanFilter", "link");
							waitExplicit(3000);
							column = findElements(getLocator("LoanerSerialNoValue"));
							lessThanDateCount = column.size();
							
							if(equalDateCount > 0 && greaterThanDateCount > 0 && lessThanDateCount > 0){
								clickElement("LastConnectedFilterIcon", "link");
								waitForElementToBeVisible(60, "LoanerLastConnEqualsFilter");
								clickElement("LoanerLastConnEqualsFilter", "link");
								waitExplicit(3000);
								elementFound = true;
								lastConnValue = listViewColumnValue;
								break;
							}
						}
					}
					if(!elementFound){
						listViewColumnValue = null;						
					}
					else{
						verifyPresence("ColumnRemoveFilter");
					}
					
				} else if (columnNameList.get(i).equals("Status")) {
					List<WebElement> statusColumnValues = findElements(getLocator("Status Value"));

					for (int k = 0; k < statusColumnValues.size(); k++) {
						listViewColumnValue = statusColumnValues.get(k).getText().trim();
						if (listViewColumnValue.length() > 0) {
							break;
						}
					}
					inputData("ColumnInput", "text", listViewColumnValue);
					statusValue = listViewColumnValue;
					waitForElementToBeVisible(60, "ColumnRemoveFilter");
					verifyPresence("ColumnRemoveFilter");
				} 				
				else {
					inputData("ColumnInput", "text", columnValueList.get(i));
					waitForElementToBeVisible(60, "ColumnRemoveFilter");
					verifyPresence("ColumnRemoveFilter");
				}

				if (columnNameList.get(i).equals("Description")) {
					column = findElements(getLocator("LoanerDescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					column = findElements(getLocator("LoanerSerialNoValue"));
				} else if (columnNameList.get(i).equals("Order #")) {
					column = findElements(getLocator("LoanerOrderNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					column = findElements(getLocator("LoanerFacilityValue"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					column = findElements(getLocator("LoanerLastConnected"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					column = findElements(getLocator("LoanerLastSeen"));
				}  else if (columnNameList.get(i).equals("Status")) {
					column = findElements(getLocator("Status Value"));
				} else {
					testStep[2] = "Please enter the column name in the input";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				if (column.size() == 0) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
					wrongValueColumn.add(columnNameList.get(i));
				}
				for (WebElement wElement : column) {
					if (columnNameList.get(i).equals("Last Connected")
							&& wElement.getText().contains(listViewColumnValue)) {

						continue;
					} else if (columnNameList.get(i).equals("Status")
							&& wElement.getText().contains(listViewColumnValue)) {

						continue;
					} else if (!wElement.getText().contains(columnValueList.get(i))) {
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						wrongValueColumn.add(columnNameList.get(i));
						break;
					}
				}
				if (passScreenshots == null) {
					passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				// Removing Filter
				clickElement("LoanerViewClearFilter");
				waitExplicit(3000);
				column = findElements(getLocator("LoanerSerialNoValue"));
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

				if (count != column.size()) {
					testStep[2] = "Removing Filter functionality is not working";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}
			
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				if (columnNameList.get(i).equals("Last Connected")) {
					inputData("ColumnInput", "text", lastConnValue);
					waitExplicit(3000);
					//waitForElementToBeVisible(60, "LoanerSerialNoValue");
				}
				else if (columnNameList.get(i).equals("Status")){
					inputData("ColumnInput", "text", statusValue);
					waitExplicit(3000);
					//waitForElementToBeVisible(60, "LoanerSerialNoValue");
					}
				else {
					inputData("ColumnInput", "text", columnValueList.get(i));
					waitExplicit(3000);
					//waitForElementToBeVisible(60, "LoanerSerialNoValue");
				}
			}
			
			if (passScreenshots == null) {
				passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			} else {
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}			
			
			clickElement("LoanerViewClearFilter");
			waitExplicit(3000);
			
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				if (columnNameList.get(i).equals("Last Connected")) {
					verifyBlankText("ColumnInput");
				}
				else {
					verifyAbsence("ColumnRemoveFilter");
					}
			}
			passScreenshots = passScreenshots + ","
					+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

			if (failScreenshots != null) {
				testStep[2] = wrongValueColumn.toString() + "don't have appropriate filter result";
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for Add/Remove filters to/from the each column on Loaner Device List Screen cannot be done because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());;
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Method to verify Loaner List View Columns sorting functionality
	 * @param columnName
	 * 		  ColumnName(s) for which sorting needs to be tested
	 * 	      For testing sorting on multiple columns, provide all the columns names separated by comma.
	 * @param lastConnectedDateFormat
	 * 		  Format in which Last Connected Date is displayed, eg, MM/dd/yyyy
	 * @return String[] 
	 * 		  - Returns the result of the execution
	 * @throws TestScriptException
	 */
	
	public String[] verifyLoanerListViewSorting(String columnName, String lastConnectedDateFormat) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify the loaner list view columns sorting functionality";
		testStep[1] = "Columns for Loaner List View tabs should be sorted as expected";
		testStep[2] = "Columns for Loaner List View tabs are sorted as expected";
		testStep[4] = null;
		String status = "Pass";
		List<String> columnNameList = Arrays.asList(columnName.split(","));
		List<WebElement> listViewColumnValue = new ArrayList<WebElement>();
		List<String> wrongAscendingOrderColumn = new ArrayList<String>();
		List<String> wrongDescendingOrderColumn = new ArrayList<String>();
		int countBlankOthers = 0;
		try {
			
			waitForElementToBeVisible(60, "LoanerSerialNoValue");

			for (int i = 0; i < columnNameList.size(); i++) {
				countBlankOthers = 0;
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				clickElement("ColumnButton", "Button");
				waitForElementToBeVisible(60, "ColumnInAscendingOrder");
				waitExplicit(2000);
				if (columnNameList.get(i).equals("Description")) {
					listViewColumnValue = findElements(getLocator("LoanerDescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					listViewColumnValue = findElements(getLocator("LoanerSerialNoValue"));
				} else if (columnNameList.get(i).equals("Order #")) {
					listViewColumnValue = findElements(getLocator("LoanerOrderNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					listViewColumnValue = findElements(getLocator("LoanerFacilityValue"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					listViewColumnValue = findElements(getLocator("LoanerLastSeen"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					listViewColumnValue = findElements(getLocator("LoanerLastConnected"));
				} else if (columnNameList.get(i).equals("Status")) {
					listViewColumnValue = findElements(getLocator("Status Value"));
				} else {
					testStep[2] = columnNameList.get(i) + " doesn't exist";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				ArrayList<String> listViewColumnData = new ArrayList<String>();
				ArrayList<String> orderedData = new ArrayList<String>();
				List<String> statList = new ArrayList<String>();
				for (WebElement wElement : listViewColumnValue) {
					listViewColumnData.add(wElement.getText().trim());
					orderedData.add(wElement.getText().trim());
				}
				
				if(columnNameList.get(i).equals("Status")){
					List<String> emptyList = new ArrayList<String>();
					emptyList.add("");
					for(int s=0;s<listViewColumnData.size();s++){
						statList.add(s, listViewColumnData.get(s));
					}
				
					List<String> dueList = new ArrayList<String>();
					int countBlank = 0;				
					
					for(int j = 0; j<statList.size();j++){
						if(statList.get(j).contains("DUE:")){
							dueList.add(statList.get(j).substring("DUE: ".length()));
						}
						else if(statList.get(j).isEmpty()){
							countBlank++;
						}
					}
					
					Collections.sort(statList);
					statList.removeAll(emptyList);
					
					SimpleDateFormat sdf = new SimpleDateFormat(lastConnectedDateFormat);
					for(int k=0;k<dueList.size();k++){
						dueList.set(k,sdf.format(sdf.parse(dueList.get(k))));
					}
					
					Collections.sort(dueList, new Comparator<String>(){
						DateFormat df = new SimpleDateFormat(lastConnectedDateFormat);
						@Override
						public int compare(String o1, String o2){
							try {
				                return df.parse(o1).compareTo(df.parse(o2));
				            } catch (ParseException e) {
				                throw new IllegalArgumentException(e);
				            }
						}
					});
					
					for(int l=0;l<dueList.size();l++){
						dueList.set(l, "DUE: "+dueList.get(l));
					}
					
					Iterator<String> iterator = dueList.iterator();
	
					for(int m=0;m<statList.size();m++){
						if(statList.get(m).contains("DUE:")){
							if(iterator.hasNext()){
								statList.set(m,iterator.next());
							}
						}
					}
					
					for(int n=0;n<countBlank;n++){
						statList.add("");
					}
					
					//Comparing the displayed and expected status values corresponding to each description
					for(int o=0;o<statList.size();o++){
						if(!statList.get(o).equals(listViewColumnData.get(o))){
							if(!wrongAscendingOrderColumn.contains(columnNameList.get(i)))
							wrongAscendingOrderColumn.add(columnNameList.get(i));
						}
					}
					
					if (testStep[4] == null) {
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						testStep[4] = testStep[4] + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}	
				
				else{		
					countBlankOthers = Collections.frequency(orderedData, "");
					List<String> emptyList = new ArrayList<String>();
					emptyList.add("");
					orderedData.removeAll(emptyList);			
					
					Collections.sort(orderedData);
					for(int n=0;n<countBlankOthers;n++){
						orderedData.add("");
					}
					
					if (!orderedData.equals(listViewColumnData)) {
						if(!wrongAscendingOrderColumn.contains(columnNameList.get(i)))
						wrongAscendingOrderColumn.add(columnNameList.get(i));
					}
					if (testStep[4] == null) {
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						testStep[4] = testStep[4] + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}
				// Descending order test
				clickElement("ColumnButton", "Button");
				waitForElementToBeVisible(60, "ColumnInDescendingOrder");
				waitExplicit(2000);
				if (columnNameList.get(i).equals("Description")) {
					listViewColumnValue = findElements(getLocator("LoanerDescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					listViewColumnValue = findElements(getLocator("LoanerSerialNoValue"));
				} else if (columnNameList.get(i).equals("Order #")) {
					listViewColumnValue = findElements(getLocator("LoanerOrderNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					listViewColumnValue = findElements(getLocator("LoanerFacilityValue"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					listViewColumnValue = findElements(getLocator("LoanerLastSeen"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					listViewColumnValue = findElements(getLocator("LoanerLastConnected"));
				} else if (columnNameList.get(i).equals("Status")) {
					listViewColumnValue = findElements(getLocator("Status Value"));
				}

				ArrayList<String> listViewColumnData2 = new ArrayList<String>();
				//orderedData = new ArrayList<String>();
				for (WebElement wElement : listViewColumnValue) {
					listViewColumnData2.add(wElement.getText().trim());
					//orderedData.add(wElement.getText().trim());
				}
				
				if(columnNameList.get(i).equals("Status")){
					Collections.reverse(statList);
					for(int o=0;o<statList.size();o++){
						if(!statList.get(o).equals(listViewColumnData2.get(o))){
							wrongDescendingOrderColumn.add(columnNameList.get(i));
						}
					}
					if (testStep[4] == null) {
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						testStep[4] = testStep[4] + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}
				
				else {	
					Collections.reverse(orderedData);
					//Collections.sort(orderedData, Collections.reverseOrder());
					if (!orderedData.equals(listViewColumnData2)) {
						if(!wrongDescendingOrderColumn.contains(columnNameList.get(i)))
						wrongDescendingOrderColumn.add(columnNameList.get(i));
					}
					testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
			}

			if (wrongAscendingOrderColumn.size() > 0 && wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order and "
						+ wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				//testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongAscendingOrderColumn.size() > 0) {
				testStep[2] = wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order.";
				status = "Fail";
				//testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				//testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}

		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for loaner list view column sorting functionality cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());;
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify Loaner List View Default Sorting functionality
	 * @return String[] 
	 * 		  - Returns the result of the execution
	 * @throws TestScriptException
	 */
	
	public String[] verifyLoanerListViewDefaultSorting(String lastConnectedDateFormat) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify the loaner list view columns are sorting by default";
		testStep[1] = "Columns for Loaner List View tabs should be sorted by default";
		testStep[2] = "Columns for Loaner List View tabs are sorted by default";
		testStep[4] = null;
		String status = "Pass";
		
		try {
			waitForElementToBeVisible(60, "LoanerSerialNoValue");
			ArrayList<String> statusValueList = new ArrayList<String>();
			ArrayList<String> descriptionValueList = new ArrayList<String>();
			ArrayList<String> serialNoValueList = new ArrayList<String>();
			List<WebElement> listViewElements = findElements(getLocator("Status Value"));
			
			for(WebElement element:listViewElements){
				statusValueList.add(element.getText().trim());
			}
			
			listViewElements = findElements(getLocator("LoanerDescriptionValue"));
			
			for(WebElement element:listViewElements){
				descriptionValueList.add(element.getText().trim());
			}
			
			listViewElements = findElements(getLocator("LoanerSerialNoValue"));
			
			for(WebElement element:listViewElements){
				serialNoValueList.add(element.getText().trim());
			}
			
			HashMap<String, List<String>> descWithDueDate = new HashMap<String, List<String>>();
			HashMap<String, HashMap<String,List<String>>> dueDateWithSerialNo = new HashMap<String, HashMap<String,List<String>>>();
			
			List<String> tempList = new ArrayList<String>();
			String tmp = null;
			String tmpStatus = null;
			HashMap<String, List<String>> tempStatusSerialNos = new HashMap<String, List<String>>();
			List<String> tempSerialNos = new ArrayList<String>();
			
			//Storing all the status values corresponding to each description in a Map
			tmp = descriptionValueList.get(0);
			tmpStatus = statusValueList.get(0);
			
			
			for(int i=0; i<descriptionValueList.size(); i++){
				if(!tmp.equals(descriptionValueList.get(i))){
					descWithDueDate.put(tmp, tempList);
					tempStatusSerialNos.put(tmpStatus, tempSerialNos);
					dueDateWithSerialNo.put(tmp, tempStatusSerialNos);
					tempList =  new ArrayList<String>();
					tempSerialNos =  new ArrayList<String>();
					tempStatusSerialNos = new HashMap<String, List<String>>();
					tempList.add(statusValueList.get(i));
					tempSerialNos.add(serialNoValueList.get(i));
					tmp = descriptionValueList.get(i);
					tmpStatus = statusValueList.get(i);
				}
				else {
					tempList.add(statusValueList.get(i));
					if(!tmpStatus.equals(statusValueList.get(i))){
						tempStatusSerialNos.put(tmpStatus, tempSerialNos);
						tempSerialNos =  new ArrayList<String>();
						tempSerialNos.add(serialNoValueList.get(i));
						tmpStatus = statusValueList.get(i);
					}
					else{
						tempSerialNos.add(serialNoValueList.get(i));
					}
				}
			}
			tempStatusSerialNos.put(tmpStatus, tempSerialNos);
			descWithDueDate.put(tmp, tempList);
			dueDateWithSerialNo.put(tmp, tempStatusSerialNos);
			
						
			//Storing all the serial numbers corresponding to each status value in a Map
			
			/*tempList = new ArrayList<String>();
			
			for(int i=0;i<descWithDueDate.size();i++){
				List<String> tempDescList = Arrays.asList(descWithDueDate.keySet().toArray(new String[0]));
				List<String> tempStatus = descWithDueDate.get(tempDescList.get(i));
				HashMap<String, List<String>> tempStatusWithSerialNo = new HashMap<String, List<String>>();
				tmp = tempStatus.get(0);
				int index = 0;
				
				tempList =  new ArrayList<String>();
				for(int k=0; k<tempStatus.size(); k++){
				  index = statusValueList.indexOf(tempStatus.get(k));	
				  if(tmp.contains("DUE") || tmp.isEmpty()){
					if(!tmp.equals(tempStatus.get(k))){
						tempStatusWithSerialNo.put(tmp, tempList);
						tempList =  new ArrayList<String>();
						tempList.add(serialNoValueList.get(index));
						tmp = tempStatus.get(k);
					}
					else{
						tempList.add(serialNoValueList.get(index));
					}
				  }
				}
				tempStatusWithSerialNo.put(tmp, tempList);				
				dueDateWithSerialNo.put(tempDescList.get(i), tempStatusWithSerialNo);				
			}*/
			
			//Sorting all the Description Values
			List<String> sortedDescList = Arrays.asList(descriptionValueList.toArray(new String[0]));
			Collections.sort(sortedDescList);
			
			//Comparing the displayed and expected descriptionValue
			for(int i=0;i<descriptionValueList.size();i++){
				if(!descriptionValueList.get(i).equals(sortedDescList.get(i))){
					testStep[2] = "Loaner List View is not sorted by default on description";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}
						
			List<String> wronglySortedElements = new ArrayList<String>();		
			for(int d=0;d<descWithDueDate.size();d++){
				List<String> descList = Arrays.asList(descWithDueDate.keySet().toArray(new String[0]));
				List<String> statList = new ArrayList<String>();
				List<String> sortedStatList = new ArrayList<String>();
				tempList = new ArrayList<String>();
				statList = descWithDueDate.get(descList.get(d));
				List<String> dueList = new ArrayList<String>();
				LinkedList<String> maintStateList = new LinkedList<String>();
				int countBlank = 0;				
				
				for(int i = 0; i<statList.size();i++){
					if(statList.get(i).contains("DUE:")){
						tempList.add(statList.get(i).substring("DUE: ".length()));
					}
					else if(statList.get(i).isEmpty()){
						countBlank++;
					}
					else {
						maintStateList.add(statList.get(i));
					}
				}
				
				SimpleDateFormat sdf = new SimpleDateFormat(lastConnectedDateFormat);
				for(int i=0;i<tempList.size();i++){
					dueList.add(sdf.format(sdf.parse(tempList.get(i))));
				}
				
				Collections.sort(dueList, new Comparator<String>(){
					DateFormat df = new SimpleDateFormat(lastConnectedDateFormat);
					@Override
					public int compare(String o1, String o2){
						try {
			                return df.parse(o1).compareTo(df.parse(o2));
			            } catch (ParseException e) {
			                throw new IllegalArgumentException(e);
			            }
					}
				});
				
				for(int i=0;i<tempList.size();i++){
					tempList.set(i, "DUE: "+dueList.get(i));
				}
				
				Iterator<String> iterator = tempList.iterator();

				for(int i=0;i<statList.size();i++){
					if(!statList.get(i).contains("DUE:") && !statList.get(i).isEmpty()){
						if(!maintStateList.isEmpty()){
							sortedStatList.add(maintStateList.getFirst());
							maintStateList.removeFirst();
						}
					}
					else if((statList.get(i).isEmpty() || statList.get(i).contains("DUE:")) && iterator.hasNext()){
						sortedStatList.add(iterator.next());
					}
					else if((statList.get(i).isEmpty() || statList.get(i).contains("DUE:")) && !iterator.hasNext() && !maintStateList.isEmpty()){
						sortedStatList.add(maintStateList.getFirst());
						maintStateList.removeFirst();
					}
				}
				
				for(int i=0;i<countBlank;i++){
					sortedStatList.add("");
				}
				
				//Comparing the displayed and expected status values corresponding to each description
				for(int i=0;i<statList.size();i++){
					if(!statList.get(i).equals(sortedStatList.get(i))){
						wronglySortedElements.add(descList.get(d));
					}
				}
			}
			
			if(!wronglySortedElements.isEmpty()){
				testStep[2] = "Loaner List View is not sorted properly by default for status values corresponding to following descriptions "+wronglySortedElements.toString();
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			
			wronglySortedElements = new ArrayList<String>();	
			
			//Sorting all the SerialNos Values with respect to each Status Value	
			for(int i=0;i<descriptionValueList.size();i++){
				HashMap<String, List<String>> tempMap = dueDateWithSerialNo.get(descriptionValueList.get(i));
				List<String> tempStatus = new ArrayList<String>();
						
				for(String statusValue: tempMap.keySet()){
				List<String> serialNos = tempMap.get(statusValue);
				List<String> sortedSerialNo = Arrays.asList(serialNos.toArray(new String[0]));
				Collections.sort(sortedSerialNo,new Comparator<String>() {
			        @Override
			        public int compare(String o1, String o2) {
			            return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
			        }
			    });							
				
				//Comparing the displayed and expected serialNo values corresponding to each statusValue
				for(int k=0;k<serialNos.size();k++){
					if(!serialNos.get(k).equals(sortedSerialNo.get(k))){
						wronglySortedElements.add(statusValue);
					}
				}
			  }
			}	
						
			if(!wronglySortedElements.isEmpty()){
				testStep[2] = "Loaner List View is not sorted properly by default for serialNos values corresponding to following statusValue "+wronglySortedElements.toString();
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
						
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());			

		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for loaner list view column sorting functionality cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());;
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * Method to verify that the filters for Loaner List View columns is persisted even 
	 * when user navigates back from Home screen to Loaner List View using browser back button
	 * @param screenName
	 * 		  ProductGroup - It user wants to test from Product Group screen
	 * 		  ProductType - It user wants to test from Product Type screen
	 * 		  ProductTypeSummary - It user wants to test from ProductTypeSummary screen
	 * @param productDetails
	 * 		  Product Group name and Product Type separated by comma. Eg Handpieces,Sagital Saw
	 * @param columnName
	 * 		  List View columns separated by comma for which filters needs to be applied
	 * @param columnValue
	 * 		  Column Values separated by comma corresponding to List View columns mentioned in previous argument
	 * @return String[]
	 * 		  Return the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyLoanerListViewFilterPersistence(String screenName, String productDetails, String columnName, String columnValue ) throws TestScriptException{
	   String[] testStep = getStepInstance();
		testStep[0] = "Verify the the filter for loaner list view columns are persists whenever uses browser back button";
		testStep[1] = "Filter for loaner list view columns should be persisted for "+screenName;
		testStep[2] = "Filter for loaner list view columns are persists for "+screenName;
		testStep[4] = null;
		String status = "Pass";
		
		try {
			List<WebElement> column = new ArrayList<WebElement>();
			List<String> wrongValueColumn = new ArrayList<String>();
			String failScreenshots = null;
			String passScreenshots = null;
			String listViewColumnValue = null;
			String lastConnValue = null;
			String statusValue = null;
			boolean lastConnGreaterFilterTested = false;
			boolean lastConnLesserFilterTested = false;
			int count = 0;
			
			List<String> columnNameList = Arrays.asList(columnName.split(","));
			List<String> columnValueList = Arrays.asList(columnValue.split(","));
			String[] productDet = productDetails.split(",");
			if(!(productDet.length > 1)){
				testStep[2] = "Please provide Product Group and Product Type separated by comma as 2nd argument.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			
			if (columnNameList.size() != columnValueList.size()) {
				testStep[2] = "Please enter the values as 4th argument corresponding to each column names specified in 3rd argument. ";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			
			if (!screenName.equalsIgnoreCase("ProductGroup") && !screenName.equalsIgnoreCase("ProductType") && !screenName.equalsIgnoreCase("ProductTypeSummary")) {
				testStep[2] = "Please enter the screen name as either ProductGroup, ProductType or ProductTypeSummary";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}						
			config.setMapGlobalVariable("ProductGroup", productDet[0]);
			config.setMapGlobalVariable("ProductType", productDet[1]);
			navigateToScreen(screenName, productDet[0], productDet[1], "N");
			
			if(screenName.equalsIgnoreCase("ProductGroup")){
				clickElement("Pg_LoanerLabel", "Link");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
			}			
			else if(screenName.equalsIgnoreCase("ProductType")){
				clickElement("Pt_LoanerLabel", "Link");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
			}
			else if(screenName.equalsIgnoreCase("ProductTypeSummary")){
				clickElement("PT_Loaners_label", "Link");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
			}
			
			// Count of result before adding filter
			waitForElementToBeVisible(60, "LoanerSerialNoValue");
			column = findElements(getLocator("LoanerSerialNoValue"));
			count = column.size();
			
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				if (columnNameList.get(i).equals("Last Connected")) {
					List<WebElement> lastConnectedColumn = findElements(getLocator("LoanerLastConnected"));
					List<String> lastConnectedValues = new ArrayList<String>();
					
					for(WebElement wElement: lastConnectedColumn)
					{
						lastConnectedValues.add(wElement.getText().trim());
					}					
										
					int equalDateCount = 0;
					int greaterThanDateCount = 0;
					int lessThanDateCount = 0;
					boolean elementFound = false;
					
					for (int k = 0; k < lastConnectedValues.size(); k++) {						
						listViewColumnValue = lastConnectedValues.get(k);
						if (listViewColumnValue.length() > 5) {
							listViewColumnValue = listViewColumnValue.substring(0, listViewColumnValue.indexOf(" "));
							inputData("ColumnInput", "text", listViewColumnValue);
							clickElement("LastConnectedFilterIcon", "link");
							waitForElementToBeVisible(60, "LoanerLastConnEqualsFilter");
							clickElement("LoanerLastConnEqualsFilter", "link");
							waitExplicit(3000);
							waitForElementToBeVisible(60, "LoanerSerialNoValue");
							column = findElements(getLocator("LoanerSerialNoValue"));
							equalDateCount = column.size();
														
							clickElement("LastConnectedFilterIcon", "link");
							waitForElementToBeVisible(60, "LoanerLastConnGreaterThanFilter");
							clickElement("LoanerLastConnGreaterThanFilter", "link");
							waitExplicit(3000);
							column = findElements(getLocator("LoanerSerialNoValue"));
							greaterThanDateCount = column.size();
							
							clickElement("LastConnectedFilterIcon", "link");
							waitForElementToBeVisible(60, "LoanerLastConnLessThanFilter");
							clickElement("LoanerLastConnLessThanFilter", "link");
							waitExplicit(3000);
							column = findElements(getLocator("LoanerSerialNoValue"));
							lessThanDateCount = column.size();
							
							if(equalDateCount > 0 && greaterThanDateCount > 0 && lessThanDateCount > 0){
								clickElement("LastConnectedFilterIcon", "link");
								waitForElementToBeVisible(60, "LoanerLastConnEqualsFilter");
								clickElement("LoanerLastConnEqualsFilter", "link");
								waitExplicit(3000);
								elementFound = true;
								lastConnValue = listViewColumnValue;
								break;
							}
						}
					}
					if(!elementFound){
						listViewColumnValue = null;						
					}
					else{
						verifyPresence("ColumnRemoveFilter");
					}
					
				} else if (columnNameList.get(i).equals("Status")) {
					List<WebElement> statusColumnValues = findElements(getLocator("Status Value"));

					for (int k = 0; k < statusColumnValues.size(); k++) {
						listViewColumnValue = statusColumnValues.get(k).getText().trim();
						if (listViewColumnValue.length() > 0) {
							break;
						}
					}
					inputData("ColumnInput", "text", listViewColumnValue);
					statusValue = listViewColumnValue;
					waitForElementToBeVisible(60, "ColumnRemoveFilter");
					verifyPresence("ColumnRemoveFilter");
				} 				
				else {
					inputData("ColumnInput", "text", columnValueList.get(i));
					waitForElementToBeVisible(60, "ColumnRemoveFilter");
					verifyPresence("ColumnRemoveFilter");
				}

				if (columnNameList.get(i).equals("Description")) {
					column = findElements(getLocator("LoanerDescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					column = findElements(getLocator("LoanerSerialNoValue"));
				} else if (columnNameList.get(i).equals("Order #")) {
					column = findElements(getLocator("LoanerOrderNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					column = findElements(getLocator("LoanerFacilityValue"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					column = findElements(getLocator("LoanerLastConnected"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					column = findElements(getLocator("LoanerLastSeen"));
				}  else if (columnNameList.get(i).equals("Status")) {
					column = findElements(getLocator("Status Value"));
				} else {
					testStep[2] = "Please enter the column name in the input";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				if (column.size() == 0) {
					if (failScreenshots == null) {
						failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						failScreenshots = failScreenshots + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
					wrongValueColumn.add(columnNameList.get(i));
				}
				for (WebElement wElement : column) {
					if (columnNameList.get(i).equals("Last Connected")
							&& wElement.getText().contains(listViewColumnValue)) {

						continue;
					} else if (columnNameList.get(i).equals("Status")
							&& wElement.getText().contains(listViewColumnValue)) {

						continue;
					} else if (!wElement.getText().contains(columnValueList.get(i))) {
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						wrongValueColumn.add(columnNameList.get(i));
						break;
					}
				}
				if (passScreenshots == null) {
					passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				} else {
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				
				navigateToScreen("ProductGroup", productDet[0], productDet[1], "N");
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				
				
				browserNavigation("Back");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				
				if (columnNameList.get(i).equals("Last Connected")) {
					verifyAttributeContainsText("ColumnInput", "class", "ng-not-empty");
				}
				else {
					verifyPresence("ColumnRemoveFilter");
				}
				
				//Code for verifying that Less than and Greater than Filter for Last Connected Column is also persisted when Browser back button is clicked
				// Code to be added once the related issue is resolved
				
				if(columnNameList.get(i).equals("Last Connected")){
				while (true){
					List<String> dueList = new ArrayList<String>();
					List<String> split =  new ArrayList();
					if(!lastConnGreaterFilterTested){
						clickElement("LastConnectedFilterIcon", "link");
						waitForElementToBeVisible(60, "LoanerLastConnGreaterThanFilter");
						clickElement("LoanerLastConnGreaterThanFilter", "link");
						waitExplicit(3000);
						column = findElements(getLocator("LoanerLastConnected"));
					}
					else if(!lastConnLesserFilterTested){
						clickElement("LastConnectedFilterIcon", "link");
						waitForElementToBeVisible(60, "LoanerLastConnLessThanFilter");
						clickElement("LoanerLastConnLessThanFilter", "link");
						waitExplicit(3000);
						column = findElements(getLocator("LoanerLastConnected"));
					}
					
					navigateToScreen("ProductGroup", productDet[0], productDet[1], "N");
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					
					
					browserNavigation("Back");
					waitForElementToBeVisible(60, "LoanerSerialNoValue");
					passScreenshots = passScreenshots + ","
							+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				
					verifyAttributeContainsText("ColumnInput", "class", "ng-not-empty");
					
					if(!lastConnGreaterFilterTested){
						lastConnGreaterFilterTested = true;
						verifyAttributeContainsText("LoanerLastConnFilterSymbol", "class","right");
					}
					else if(!lastConnLesserFilterTested){
						lastConnLesserFilterTested = true;
						verifyAttributeContainsText("LoanerLastConnFilterSymbol", "class","left");
					}
					
					column = findElements(getLocator("LoanerSerialNoValue"));
					if(column.size()==0){
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						wrongValueColumn.add(columnNameList.get(i));
					}
					
					if(lastConnGreaterFilterTested && lastConnLesserFilterTested){
						break;
					}
					
				  }
				}
				
				// Removing Filter
				clickElement("LoanerViewClearFilter");
				waitExplicit(3000);
				column = findElements(getLocator("LoanerSerialNoValue"));
				passScreenshots = passScreenshots + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());

				if (count != column.size()) {
					testStep[2] = "Loaner List View is not reset to the default state after clicking the Clear Filter.";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}
			
			if (failScreenshots != null) {
				testStep[2] = "List view was not properly filtered for the columns "+wrongValueColumn.toString();
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;
		} catch (Exception e) {
				if (testStep[4] == null) {
					testStep[2] = "Verification for persistence of the filters for loaner list view column cannot be done because of an exception. Please refer Logs/info.log for more details.";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2] + " " + e.getMessage();
				}
			} finally {
				testStep[3] = status;
			}
			if (status.equals("Fail")) {
				config.getLogger().error(methodFailureMessage);
				throw new TestScriptException(methodFailureMessage, testStep);
			}
			return testStep;
		}

	 /**
	 *  
	 * Method to verify that the sorting for Loaner List View columns is persisted even 
	 * when user navigates back from Home screen to Loaner List View using browser back button
	 * @param screenName
	 * 		  ProductGroup - It user wants to test from Product Group screen
	 * 		  ProductType - It user wants to test from Product Type screen
	 * 		  ProductTypeSummary - It user wants to test from ProductTypeSummary screen
	 * @param productDetails
	 * 		  Product Group name and Product Type separated by comma. Eg Handpieces,Sagital Saw
	 * @param columnName
	 * 		  List View columns separated by comma for which sorting needs to be applied
	 * @param lastConnectedDateFormat
	 * 		  Format in which Last Connected Date is displayed, eg, MM/dd/yyyy
	 * @return String[]
	 * 		  Return the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyLoanerListViewSortingPersistence(String screenName, String productDetails, String columnName, String lastConnectedDateFormat) throws TestScriptException{
	   String[] testStep = getStepInstance();
		testStep[0] = "Verify the the sorting for loaner list view columns are persists whenever uses browser back button";
		testStep[1] = "Sorting for loaner list view columns should be persisted for "+screenName;
		testStep[2] = "Sorting for loaner list view columns are persists for "+screenName;
		testStep[4] = null;
		String status = "Pass";
		
		try {
			List<WebElement> listViewColumnValue = new ArrayList<WebElement>();
			List<String> wrongAscendingOrderColumn = new ArrayList<String>();
			List<String> wrongDescendingOrderColumn = new ArrayList<String>();
			List<String> columnNameList = Arrays.asList(columnName.split(","));
			
			String[] productDet = productDetails.split(",");
			if(!(productDet.length > 1)){
				testStep[2] = "Please provide Product Group and Product Type separated by comma as 2nd argument.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			
			if (!screenName.equalsIgnoreCase("ProductGroup") && !screenName.equalsIgnoreCase("ProductType") && !screenName.equalsIgnoreCase("ProductTypeSummary")) {
				testStep[2] = "Please enter the screen name as either ProductGroup, ProductType or ProductTypeSummary";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}						
			config.setMapGlobalVariable("ProductGroup", productDet[0]);
			config.setMapGlobalVariable("ProductType", productDet[1]);
			navigateToScreen(screenName, productDet[0], productDet[1], "N");
			
			if(screenName.equalsIgnoreCase("ProductGroup")){
				clickElement("Pg_LoanerLabel", "Link");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
			}			
			else if(screenName.equalsIgnoreCase("ProductType")){
				clickElement("Pt_LoanerLabel", "Link");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
			}
			else if(screenName.equalsIgnoreCase("ProductTypeSummary")){
				clickElement("PT_Loaners_label", "Link");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
			}
			
			for (int i = 0; i < columnNameList.size(); i++) {
				config.setMapGlobalVariable("ColumnName", columnNameList.get(i));
				clickElement("ColumnButton", "Button");
				waitForElementToBeVisible(60, "ColumnInAscendingOrder");
				waitExplicit(2000);
				if (columnNameList.get(i).equals("Description")) {
					listViewColumnValue = findElements(getLocator("LoanerDescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					listViewColumnValue = findElements(getLocator("LoanerSerialNoValue"));
				} else if (columnNameList.get(i).equals("Order #")) {
					listViewColumnValue = findElements(getLocator("LoanerOrderNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					listViewColumnValue = findElements(getLocator("LoanerFacilityValue"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					listViewColumnValue = findElements(getLocator("LoanerLastSeen"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					listViewColumnValue = findElements(getLocator("LoanerLastConnected"));
				} else if (columnNameList.get(i).equals("Status")) {
					listViewColumnValue = findElements(getLocator("Status Value"));
				} else {
					testStep[2] = columnNameList.get(i) + " doesn't exist";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}

				ArrayList<String> listViewColumnData = new ArrayList<String>();
				ArrayList<String> orderedData = new ArrayList<String>();
				List<String> statList = new ArrayList<String>();
				for (WebElement wElement : listViewColumnValue) {
					listViewColumnData.add(wElement.getText().trim());
					orderedData.add(wElement.getText().trim());
				}
				
				if(columnNameList.get(i).equals("Status")){
					List<String> emptyList = new ArrayList<String>();
					emptyList.add("");
					for(int s=0;s<listViewColumnData.size();s++){
						statList.add(s, listViewColumnData.get(s));
					}
				
					List<String> dueList = new ArrayList<String>();
					int countBlank = 0;				
					
					for(int j = 0; j<statList.size();j++){
						if(statList.get(j).contains("DUE:")){
							dueList.add(statList.get(j).substring("DUE: ".length()));
						}
						else if(statList.get(j).isEmpty()){
							countBlank++;
						}
					}
					
					Collections.sort(statList);
					statList.removeAll(emptyList);
					
					SimpleDateFormat sdf = new SimpleDateFormat(lastConnectedDateFormat);
					for(int k=0;k<dueList.size();k++){
						dueList.set(k,sdf.format(sdf.parse(dueList.get(k))));
					}
					
					Collections.sort(dueList, new Comparator<String>(){
						DateFormat df = new SimpleDateFormat(lastConnectedDateFormat);
						@Override
						public int compare(String o1, String o2){
							try {
				                return df.parse(o1).compareTo(df.parse(o2));
				            } catch (ParseException e) {
				                throw new IllegalArgumentException(e);
				            }
						}
					});
					
					for(int l=0;l<dueList.size();l++){
						dueList.set(l, "DUE: "+dueList.get(l));
					}
					
					Iterator<String> iterator = dueList.iterator();
	
					for(int m=0;m<statList.size();m++){
						if(statList.get(m).contains("DUE:")){
							if(iterator.hasNext()){
								statList.set(m,iterator.next());
							}
						}
					}
					
					for(int n=0;n<countBlank;n++){
						statList.add("");
					}
					
					//Comparing the displayed and expected status values corresponding to each description
					for(int o=0;o<statList.size();o++){
						if(!statList.get(o).equals(listViewColumnData.get(o))){
							if(!wrongAscendingOrderColumn.contains(columnNameList.get(i)))
							wrongAscendingOrderColumn.add(columnNameList.get(i));
						}
					}
					
					if (testStep[4] == null) {
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						testStep[4] = testStep[4] + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}	
				
				else{				
					Collections.sort(orderedData);
					if (!orderedData.equals(listViewColumnData)) {
						if(!wrongAscendingOrderColumn.contains(columnNameList.get(i)))
						wrongAscendingOrderColumn.add(columnNameList.get(i));
					}
					if (testStep[4] == null) {
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					} else {
						testStep[4] = testStep[4] + ","
								+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					}
				}
				
				navigateToScreen("ProductGroup", productDet[0], productDet[1], "N");
				testStep[4] = testStep[4] + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				
				browserNavigation("Back");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
				testStep[4] = testStep[4] + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				
				verifyPresence("ColumnInAscendingOrder");
				
				// Descending order test
				clickElement("ColumnButton", "Button");
				waitForElementToBeVisible(60, "ColumnInDescendingOrder");
				waitExplicit(2000);
				if (columnNameList.get(i).equals("Description")) {
					listViewColumnValue = findElements(getLocator("LoanerDescriptionValue"));
				} else if (columnNameList.get(i).equals("Serial #")) {
					listViewColumnValue = findElements(getLocator("LoanerSerialNoValue"));
				} else if (columnNameList.get(i).equals("Order #")) {
					listViewColumnValue = findElements(getLocator("LoanerOrderNoValue"));
				} else if (columnNameList.get(i).equals("Facility")) {
					listViewColumnValue = findElements(getLocator("LoanerFacilityValue"));
				} else if (columnNameList.get(i).equals("Last Seen")) {
					listViewColumnValue = findElements(getLocator("LoanerLastSeen"));
				} else if (columnNameList.get(i).equals("Last Connected")) {
					listViewColumnValue = findElements(getLocator("LoanerLastConnected"));
				} else if (columnNameList.get(i).equals("Status")) {
					listViewColumnValue = findElements(getLocator("Status Value"));
				}

				ArrayList<String> listViewColumnData2 = new ArrayList<String>();
				orderedData = new ArrayList<String>();
				for (WebElement wElement : listViewColumnValue) {
					listViewColumnData2.add(wElement.getText().trim());
					orderedData.add(wElement.getText().trim());
				}
				
				
				if(columnNameList.get(i).equals("Status")){
					Collections.reverse(statList);
					for(int o=0;o<statList.size();o++){
						if(!statList.get(o).equals(listViewColumnData2.get(o))){
							if(!wrongDescendingOrderColumn.contains(columnNameList.get(i)))
							wrongDescendingOrderColumn.add(columnNameList.get(i));
						}
					}
				}
				else {	
					Collections.sort(orderedData, Collections.reverseOrder());
					if (!orderedData.equals(listViewColumnData2)) {
						if(!wrongDescendingOrderColumn.contains(columnNameList.get(i)))
						wrongDescendingOrderColumn.add(columnNameList.get(i));
					}
					testStep[4] = testStep[4] + "," + ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				}
				navigateToScreen("ProductGroup", productDet[0], productDet[1], "N");
				testStep[4] = testStep[4] + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				
				browserNavigation("Back");
				waitForElementToBeVisible(60, "LoanerSerialNoValue");
				testStep[4] = testStep[4] + ","
						+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				
				verifyPresence("ColumnInDescendingOrder");			
			}

			if (wrongAscendingOrderColumn.size() > 0 && wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = "Columns "+wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order and "
						+ wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongAscendingOrderColumn.size() > 0) {
				testStep[2] = "Columns "+wrongAscendingOrderColumn.toString() + " cannot be sorted in ascending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			} else if (wrongDescendingOrderColumn.size() > 0) {
				testStep[2] = "Columns "+wrongDescendingOrderColumn.toString() + " cannot be sorted in descending order.";
				status = "Fail";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}			
		} catch (Exception e) {
				if (testStep[4] == null) {
					testStep[2] = "Verification for persistence of the filters for loaner list view column cannot be done because of an exception. Please refer Logs/info.log for more details.";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2] + " " + e.getMessage();
				}
			} finally {
				testStep[3] = status;
			}
			if (status.equals("Fail")) {
				config.getLogger().error(methodFailureMessage);
				throw new TestScriptException(methodFailureMessage, testStep);
			}
			return testStep;
		}	
	
	/** Method to verify that all the Product Group and Product Type have the Colored Ring
	 * with the configured color for available devices, NOW devices, SOON devices, OUT devices
	 * @param screenName
	 * 		  ProductGroup - Provide this as argument if we want to test the colored ring for all
	 * 		  the product groups on the product group screen
	 * 		  ProductType - Provide this as argument if we want to test the colored ring for all
	 * 		  the product types on the product type screen
	 * @param productGrp
	 *        N - If we are testing for product group
	 *        Product Group Name for testing on product type. For eg HANDPIECES
	 * @param ringElement
	 * 		  UI element identifier for the colored ring element
	 * @param colorOrder
	 * 		  Order in which color should be displayed in the colored separated by semicolon. For eg, rgb(255, 0, 0);rgb(255, 119, 0);rgb(28, 86, 135);rgb(0, 128, 0)
	 * @return String[]
	 * 		  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyMaintSummaryRingColorOrder(String screenName, String productGrp, String ringElement, String colorOrder) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the color for the maintenance summary ring is as per the expected device status";
		testStep[1] = "Color for maintenance summary ring is as per the expected device status for the "+screenName;
		testStep[2] = "Color for maintenance summary ring should be as per the expected device status for the "+screenName;
		testStep[4] = null;
		String status = "Pass";
		
		try {
			
			String[] colors = colorOrder.split(";");
			String failScreenshots = null;
			String passScreenshots = null;
			List<String> faultyElements = new ArrayList<String>();
			String productGroupName = "";
			
			if (!screenName.equalsIgnoreCase("ProductGroup") && !screenName.equalsIgnoreCase("ProductType")) {
				testStep[2] = "Please enter the screen name as either ProductGroup, ProductType";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}						
			
			config.setMapGlobalVariable("ProductGroup",productGrp);
			clickElement("SEM_HomeLink", "Link");
			waitForElementToBeInvisible("SEM_Loading");
			waitForElementToBeVisible(60, "SEM_ProductGrpName");
			
			if(screenName.equalsIgnoreCase("ProductGroup")){
				List<WebElement> productGroupTiles = findElements(getLocator("SEM_ProductGrpTile"));
				for (WebElement wElement : productGroupTiles) {
					WebElement productGrpName = wElement.findElement(getLocator("SEM_ProductGrpName"));
					WebElement productGrpImage = wElement.findElement(getLocator("SEM_ProductGrpImg"));
					productGroupName = productGrpName.getText().toLowerCase();

					if (!productGrpName.getText().isEmpty() && productGrpImage.getAttribute("src").toLowerCase()
							.contains(productGrpName.getText().toLowerCase())) {

						new Actions(driver).moveToElement(productGrpName).perform();
						config.setMapGlobalVariable("ProductGroup",productGrpName.getText().toLowerCase());
					}
				
				List<WebElement> ringElements = findElements(getLocator(ringElement));
				List<String> ringElementColors = new ArrayList<String>();
							
				if(ringElements.size()==0 ){
					testStep[2] = "Please provide the correct UI identifier for ring and try again.";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
				else {
					for(int i =0;i<ringElements.size();i++){
						ringElementColors.add(ringElements.get(i).getCssValue("fill"));
					}
					
				if(productGrpName.getText().toLowerCase().equalsIgnoreCase("Chargers")){
					
					if((!colors[2].equals(ringElementColors.get(0))) && (!colors[3].equals(ringElementColors.get(1)))){
						testStep[2] = "Color ring does not displays the correct color for the maintenance summary information for Chargers .";
						status = "Fail";
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						methodFailureMessage = testStep[2];
						faultyElements.add(productGrpName.getText().trim());
						}
					else{
						if (passScreenshots == null) {
							passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							passScreenshots = passScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
					}
					
				}
				else{
					if(colors.length!=ringElements.size()){
						testStep[2] = "Color ring does not displays the maintenance summary information for all the device status";
						status = "Fail";
						if (failScreenshots == null) {
							failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							failScreenshots = failScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
						methodFailureMessage = testStep[2];
						faultyElements.add(productGrpName.getText().trim());
					}
					
					
					else{
						for(int i =0;i<colors.length;i++){
							if(!colors[i].equals(ringElementColors.get(i))){
								testStep[2] = "Color ring does not displays the correct color for the product group maintenance summary information.";
								status = "Fail";
								if (failScreenshots == null) {
									failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
								} else {
									failScreenshots = failScreenshots + ","
											+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
								}
								methodFailureMessage = testStep[2];
								faultyElements.add(productGrpName.getText().trim());
								break;
							}
						}
						
						if (passScreenshots == null) {
							passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						} else {
							passScreenshots = passScreenshots + ","
									+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						}
					  }					
				    }
				  }
				}
			  }
			  else{
				    
				  	clickElement("SEM_ProductGroupLink", "Link");
					waitForElementToBeInvisible("SEM_Loading");
					waitForElementToBeVisible(60, "Pg_Name");
					List<WebElement> productTypeTiles = findElements(getLocator("ProductTypeTile"));
					config.setMapGlobalVariable("ProductGroup",productGrp.toLowerCase());
					for (WebElement wElement : productTypeTiles) {
						WebElement productTypeName = wElement.findElement(getLocator("ProductName"));
						WebElement productTypeImage = wElement.findElement(getLocator("ProductImage"));

						if (!productTypeName.getText().isEmpty()
								&& productTypeImage.getAttribute("src").toLowerCase().contains(productGrp.toLowerCase())) {

							new Actions(driver).moveToElement(productTypeName).perform();
						}
						
						config.setMapGlobalVariable("ProductType",productTypeName.getText().toLowerCase());
						
						List<WebElement> ringElements = findElements(getLocator(ringElement));
						List<String> ringElementColors = new ArrayList<String>();
									
						if(ringElements.size()==0 ){
							testStep[2] = "Please provide the correct UI identifier for ring and try again.";
							status = "Fail";
							testStep[4] = "";
							methodFailureMessage = testStep[2];
							throw new Exception();
						}
						else {
							for(int i =0;i<ringElements.size();i++){
								ringElementColors.add(ringElements.get(i).getCssValue("fill"));
							}
							
							if(productGrp.toLowerCase().equalsIgnoreCase("Chargers")){
								
								if((!colors[2].equals(ringElementColors.get(0))) && (!colors[3].equals(ringElementColors.get(1)))){
									testStep[2] = "Color ring does not displays the correct color for the maintenance summary information for Chargers .";
									status = "Fail";
									if (failScreenshots == null) {
										failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
									} else {
										failScreenshots = failScreenshots + ","
												+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
									}
									methodFailureMessage = testStep[2];
									faultyElements.add(productTypeName.getText().trim());
									}
								else{
									if (passScreenshots == null) {
										passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
									} else {
										passScreenshots = passScreenshots + ","
												+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
									}
								}
								
							}
						else {		
							
							if(colors.length!=ringElements.size()){
								testStep[2] = "Color ring does not displays the maintenance summary information for all the device status";
								status = "Fail";
								if (failScreenshots == null) {
									failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
								} else {
									failScreenshots = failScreenshots + ","
											+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
								}
								methodFailureMessage = testStep[2];
								faultyElements.add(productTypeName.getText().trim());
							}		
							else{
								for(int i =0;i<colors.length;i++){
									if(!colors[i].equals(ringElementColors.get(i))){
										testStep[2] = "Color ring does not displays the correct color for the product type maintenance summary information.";
										status = "Fail";
										if (failScreenshots == null) {
											failScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
										} else {
											failScreenshots = failScreenshots + ","
													+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
										}
										methodFailureMessage = testStep[2];
										faultyElements.add(productTypeName.getText().trim());
										break;
									}
								}
								if (passScreenshots == null) {
									passScreenshots = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
								} else {
									passScreenshots = passScreenshots + ","
											+ ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
								}
							}
						 }
					   }		
					}	
				}
			if (failScreenshots != null) {
				testStep[2] = "Color ring was not displayed with correct information on  "+screenName+ " for "+faultyElements.toString();
				status = "Fail";
				testStep[4] = failScreenshots;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			testStep[4] = passScreenshots;			
			
		}catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Verification for colors of the maintenance summary information cannot be done because of an exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
		
	}	
	
	/** Method to verify that the bars displayed for the Device Usage graph is filled with the colors
	 * @param barElement
	 * 		  UI element identifier for the bars displayed for the Device Usage graph
	 * @param colorProperty
	 * 		  css property which provides the color for the bars 
	 * @param color
	 * 		  color that should be displayed for the 
	 * @return String[]
	 * 		   Returns the result of the execution
	 * @throws TestScriptException 
	 */
	public String[] verifyDeviceGraphColors(String barElement, String colorProperty, String color) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that all the bars for the Device Usage graph is filled with color";
		testStep[1] = "All the bars for the Device Usage graph should be filled with color";
		testStep[2] = "All the bars for the Device Usage graph is filled with color";
		testStep[4] = null;
		String status = "Pass";		
		try {
			if(!findElement(getLocator("SelectByDevice")).getCssValue("class").contains("active")){
				clickElement("SelectByDevice", "link");
				waitForElementToHaveAttributeValue("SelectByDevice", "class", "active");
			}
				
			mouseHover("UsesGraphSection");
			List<WebElement> bars = findElements(getLocator(barElement));
			List<String> barColors = new ArrayList<String>();
			
			for(WebElement barColorElement: bars){
				barColors.add(barColorElement.getCssValue(colorProperty));
			}
			
			WebElement least = findElement(getLocator("ByDevice_LeastUses"));
			int leastCount = 0;
			Pattern pattern1 = Pattern.compile("Least\\((.+?)\\)");
			Matcher matcher1;
			matcher1 = pattern1.matcher(least.getText().trim());
			if(matcher1.find()){
				leastCount = Integer.parseInt(matcher1.group(1)); 
			}
			
			WebElement most = findElement(getLocator("ByDevice_MostUses"));
			int mostCount = 0;
			pattern1 = Pattern.compile("Most\\((.+?)\\)");
			matcher1 = pattern1.matcher(most.getText().trim());
			if(matcher1.find()){
				mostCount = Integer.parseInt(matcher1.group(1)); 
			}			
				
			Pattern pattern = Pattern.compile("rgb\\((.+?)\\)");
			Matcher matcher;
			String rgb = null;
			int count = 0;
			
			for(String barColor: barColors){
				matcher = pattern.matcher(barColor);
				if(matcher.find()){
					rgb = matcher.group(1);
					if(rgb.equals("0,0,0") || rgb.equals("255,255,255"))
					{
						testStep[2] = "Bars for the device usage  graph are not filled with colors";
						status = "Fail";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						throw new Exception();
					}
					else if(rgb.equals(color)){
						count ++;
					}
				}
			}			
			if(count != (leastCount + mostCount)){
				testStep[2] = "Total Bars with the mentioned color "+color+" for Least/Most count devices are not as expected.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}			
			
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());;
			
		}catch (Exception e) {
				if (testStep[4] == null) {
					testStep[2] = "Verification for colors of the bars of the device usage graph cannot be done because of an exception. Please refer Logs/info.log for more details.";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2] + " " + e.getMessage();
				}
		} finally {
				testStep[3] = status;
			}
			if (status.equals("Fail")) {
				config.getLogger().error(methodFailureMessage);
				throw new TestScriptException(methodFailureMessage, testStep);
			}
			return testStep;			
	}	
	/**
	 * Method to verify that if any dynamic element is displayed or not. If no, Explicit waits are added
	 * 
	 *  @param locator
	 *            locator of element which should be displayed
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	
	public String[] verifyIfProgressSpinnerDisplayed(String locator) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that Loading animation is appearing";
		testStep[1] = "The loading animation should appear";
	
		testStep[4] = null;
		String status = "Pass";	
		try {
			WebElement elt=driver.findElement(getLocator(locator));
			if(elt.isDisplayed())
			{	testStep[2] = "The loading animation appeared";
				mouseHover(locator);
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		}
		
		catch(NoSuchElementException e1)
		{
			testStep[2] = "The loading animation didn't appear, Explicit Waits are added";
			waitExplicit(3000);
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());;
		}
		catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2]= "Verification for Spinner did not work Properly. Please refer Logs/info";
				status="Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = "verifyIfElementDisplayed method failed because of exception " + e.getMessage();
			}
			}
			finally {
				testStep[3] = status;
			}
			if(status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
			}
		
		return testStep;
	}

	
	/**
	 * Method to verify that Device Usage Graph Bar Width is consistent for all the devices
	 * 
	 *  @param value
	 *            Value for Selecting 
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyDeviceUsageGraphBarWidth(String value) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify Device Usage Graph Bar Width is consistent for all the devices";
		testStep[1] = "Device Usage Graph Bar Width should be consistent for all the devices";
		testStep[2] = "Device Usage Graph Bar Width is consistent for all the devices";
		testStep[4] = null;
		String status = "Pass";
		boolean change = false;
		String currentday = "";
		try {
			verifyPresence("DaysDropdown");
			saveElementData("DaysDropdown", "CurrentDayValue");
			currentday = config.getGlobalVariable("CurrentDayValue").trim();
			
			if(!currentday.equals(value)){
				clickElement("DaysDropdown", "Button");
				config.setMapGlobalVariable("days", value);
				waitExplicit(4000);
				waitForElementToBeVisible(60, "DaysDropdownValue");
				clickElement("DaysDropdownValue", "Link");
				waitExplicit(2000);
				change = true;
			}
			
			mouseHover("ByDevice_LeastUses");
			List<WebElement> barGraph = findElements(getLocator("ByDeviceBarGraph"));
			String width =null;
			for(int i=0; i< barGraph.size();i++) {
				if(width == null) {
					width = barGraph.get(i).getAttribute("width");
					i++;
				}
				if (!width.equals(barGraph.get(i).getAttribute("width"))) {
					
					if(change){
						clickElement("DaysDropdown", "Button");
						config.setMapGlobalVariable("days", currentday);
						waitForElementToBeVisible(60, "DaysDropdownValue");
						clickElement("DaysDropdownValue", "Link");
						waitExplicit(2000);
					}
					
					testStep[2] = "Device Usage Graph Bar Width is not consistent for all the devices";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
			}
			
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			
			if(change){
				clickElement("DaysDropdown", "Button");
				config.setMapGlobalVariable("days", currentday);
				waitForElementToBeVisible(60, "DaysDropdownValue");
				clickElement("DaysDropdownValue", "Link");
				waitExplicit(2000);
			}
		} catch (Exception e) {
			if (testStep[4] == null) {
				config.setMapGlobalVariable("days", currentday);
				testStep[2] = "Device Usage Graph Bar Width cannot be verified because of an Exception. Please refer Logs/info.log for more details.";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	public String[] getBuildNumber(String element) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the current build number for the application is fetched. ";
		testStep[1] = "Current build number for the application should be fetched and stored.";
		testStep[2] = "Current build number for the application is fetched and stored.";
		testStep[4] = null;
		String status = "Pass";
		try{
			clickElement("SEM_About");
			waitForElementToBeVisible(60, element);
			String aboutVersion = findElement(getLocator(element)).getText();
			String buildVersion = aboutVersion.substring(aboutVersion.indexOf("Version:"));
			config.setMapGlobalVariable("BuildVersion", buildVersion);			
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		}catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Build number cannot be fetched because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Captures the hidden value of an element
	 * 
	 * @param objectElement
	 *            Name of the element
	 * @param value
	 *            Hidden value of that element
	 * @return String[] Returns true if method is executed successfully
	 *         otherwise returns the testResult with the reason for method
	 *         failure
	 * @throws TestScriptException
	 */
	public String[] extractHiddenValue(String objectElement, String value) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Extract the hidden value of an element" + objectElement;
		testStep[1] = "Hidden value should be extracted ";
		testStep[2] = "Hidden value is extracted";
		testStep[4] = null;
		String status = "Pass";
		try {
			WebElement element = driver.findElement(getLocator(objectElement));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			String hiddenValue = (String) (executor.executeScript("return arguments[0].value;", element));
			hiddenValue = hiddenValue.trim();
			config.setMapGlobalVariable(value, hiddenValue);
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Hidden value could not be saved because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
				config.getLogger().error(methodFailureMessage);
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to store the css or any other attribute for the web element
	 * 
	 * @param webelement
	 *            locator alias for the web element.
	 * @param propertytype 
	 * 			  css - for storing the css property
	 * 			  attribute - for storing any attribute other than css properties	
	 * @param property
	 * 			  property to be stored	           
	 * @param variable
	 * 			  variable for storing the property value			
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	
	public String[] storeElementProperty(String locator, String type, String property, String variable) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that element "+locator +" property "+property+ " is stored";
		testStep[1] = "Property "+property+ " should be stored";
		testStep[2] = "Property "+property+ " is stored";
		testStep[4] = null;
		String status = "Pass";	
		String propertyValue;
		try {
			WebElement elt=driver.findElement(getLocator(locator));
			if(type.equalsIgnoreCase("css")){
				propertyValue = elt.getCssValue(property);
			}
			else{
				propertyValue = elt.getAttribute(property);
			}
			config.setMapGlobalVariable(variable, propertyValue);
	
		}catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2]= "Property for the eleement "+locator+" cannot be stored due to an exception. Please refer Logs/info";
				status="Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = "storeElementProperty method failed because of exception " + e.getMessage();
			}
			}
			finally {
				testStep[3] = status;
			}
			if(status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
			}
		
		return testStep;
	}
	
	/**
	 * Method to store the visibility status of the webelement in the current view port
	 * 
	 * @param webelement
	 *            locator alias for the web element.   
	 * @param variable
	 * 			  variable for storing the visibility		
	 * @return String[] - Returns the result of the execution
	 * @throws TestScriptException
	 */
	 public String[] storeWebElementVisibilityInViewPort(String locator, String variable) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element "+locator +" visibility is stored in the "+variable;
		testStep[1] = "Element visibility should be stored";
		testStep[2] = "Element visibility is stored";
		testStep[4] = null;
		String status = "Pass";	
		String propertyValue;
		try {
		 	WebElement w = findElement(getLocator(locator));
		    Dimension weD = w.getSize();
		    Point weP = w.getLocation();
		    Dimension d = driver.manage().window().getSize();

		    int x = d.getWidth();
		    int y = d.getHeight();
		    int x2 = weD.getWidth() + weP.getX();
		    int y2 = weD.getHeight() + weP.getY();

		    boolean result = x2 <= x && y2 <= y;
		    config.setMapGlobalVariable(variable, String.valueOf(result));
		    		    
		}catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2]= "Visibility for the eleement "+locator+" cannot be stored due to an exception. Please refer Logs/info";
				status="Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = "storeWebElementVisibilityInViewPort method failed because of exception " + e.getMessage();
			}
			}
			finally {
				testStep[3] = status;
			}
			if(status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
			}
		
		return testStep;
		    
		  }
	
}