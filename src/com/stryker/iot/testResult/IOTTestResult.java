package com.stryker.iot.testResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.appirio.automation.configuration.Configuration;
import com.appirio.automation.configuration.TestCase;
import com.appirio.automation.result.TestResult;

public class IOTTestResult extends TestResult {

	private PrintWriter testResultStream;
	private PrintWriter testCaseResultStream;
	
	private long startTime;
	private long endTime;
	public static List<String[]> tStep;
	//public String testResultStatus = "Pass";
	private StringBuffer header = new StringBuffer("");
	private StringBuffer resultsTable = new StringBuffer("");
	private StringBuffer testCaseResult = new StringBuffer("");
	private StringBuffer testCaseScreenshot = new StringBuffer("");	
	private StringBuffer buildNumber = new StringBuffer("");

	private StringBuffer testCaseResultHeader;
	private StringBuffer testCaseSeparateResult;
	private StringBuffer testCaseSeparateScreenshot; 
	
	private static Configuration config;
	private int testCaseIndex;
	
	public void setIOTTestResultConfig(Configuration config){
		TestResult.setConfig(config);
		this.config = config;
	}
	
	public Configuration getIOTTestResultConfig(){
		return config;
	}
	
	public int getTestCaseIndex() {
		return testCaseIndex;
	}

	public void setTestCaseIndex(int testCaseIndex) {
		this.testCaseIndex = testCaseIndex;
	}
	
	public void setTestResultStatus(String testResultStatus){
		this.testResultStatus = testResultStatus;
	}
	
	public String getTestResultStatus(){
		return testResultStatus;
	}
	

	public void configureTestResult(Configuration config){
		try {
			setIOTTestResultConfig(config);
			String fName = String.format("%s/TestResult_%s.html", config.getTestResultFolderPath(),
			new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(System.currentTimeMillis())));
			testResultStream = new PrintWriter(new File(fName));
			startTime = System.currentTimeMillis();
			if (tStep == null)
				tStep = new ArrayList<String[]>();
		} catch (FileNotFoundException fNotFound) {
			System.out.println("File not found-----------");
		}
	}	
	
	public static void addItemToReport(String[] tStepResult) {
		tStep.add(tStepResult);

	}

	public void closeTestResult() {
		testResultStream.close();
	}

	
	public void generateHeader() {

		header.append("<HTML><HEAD><Title>Test ran at "
				+ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(startTime))
				+ "</Title><style>table{table-layout: fixed;width:100%;word-wrap: break-word;} table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 5px;text-align: left;} img{width: 100%;}</style></HEAD><Body><h2 align='center'>Test execution started at <i>"
				+ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(startTime)) + " and ended at ");
	}

	public void generateResultSummary(int index, TestCase testCase) {
		if (index == 0) {
			resultsTable.append(
					"<Table id='Result' border='2'><THEAD><TR style='background-color:lightgrey'><TH style='width:25%;'>Test Case Number</TH><TH style='width:60%;'>Test Case Name</TH><TH colspan='2'>Status</TH> </TR></THEAD><TBODY>");
		}

		resultsTable.append("<TR><TD><a href='#" + testCase.getTestCaseNumber() + "'>" + testCase.getTestCaseNumber() + "</TD><TD>"+ testCase.getTestCaseName() + "</TD><TD colspan='2'>"
				+ testResultStatus + "</TD></TR>");

	}

	public void generateTestResult(int startIndex, TestCase testCase) {
		if (startIndex == 0) {
			setTestCaseIndex(1);
			generateHeader();
		} 
		
		if(startIndex!=0){
			setTestCaseIndex(getTestCaseIndex() + 1); 
		}
			
		generateResultSummary(startIndex, testCase);

		// testResultStream.println("");
		
		testCaseScreenshot.append("<div class='screenshot' id='" +testCase.getTestCaseName() +"' style='margin: 7%;'><span><b><a href='#"+ testCase.getTestCaseNumber() +"'>"+ getTestCaseIndex() +". " +testCase.getTestCaseName()+"</a></b></span><br><br>");		

		testCaseResult.append("<br><br>");
		testCaseResult.append("<Table id='" + testCase.getTestCaseNumber()
				+ "' border='2' ><TR style='background-color:lightgrey'><TH style='width: 10%;'>Step #</TH><TH>Test Step</TH><TH>Expected Result</TH><TH>Actual Result</TH><TH>Actual = Expected ?</TH>");
				
		testCaseResult.append("<TH style='width: 15%;'>Screenshot</TH>");
		if(config.isRequirementReference()){
			testCaseResult.append("<TH style='width: 10%;'>Requirement Reference</TH>");
		}
		
		testCaseResult.append("</TR>");
		testCaseResult.append("<TR>");

		int stepNumber = 1;
		String[] stepResult;
		String previousScreenshot="";
		Boolean printScreenshot = false;
		int length = 0;
		String testStepNumber = "";
		
		for (int i = startIndex; i < tStep.size(); i++) {
			String[] screenshots = new String[]{""};
			testStepNumber = getTestCaseIndex()+ ". " + stepNumber;
			stepResult = tStep.get(i);
			length = stepResult.length + 1;
			testCaseResult.append("<TD>");
			testCaseResult.append( getTestCaseIndex()+ ". " + stepNumber);
			testCaseResult.append("</TD>");
			String status = "";
			// stepResult[0]="";
			for (int column = 0; column < stepResult.length; column++) {				
				if (column == 0) {
					testCaseResult.append("<TD>");
					testCaseResult.append(stepResult[column]);

					// testResultStream.println("<TD>");
					// testResultStream.println(stepNumber+".
					// "+stepResult[column]+".");
					stepNumber++;
					testCaseResult.append("</TD>");

					// testResultStream.println("</TD>");
				} else if ((config.isRequirementReference()==false && column == stepResult.length - 1) || (config.isRequirementReference() && column == stepResult.length - 2)) {

					testCaseResult.append("<TD>");

					// testResultStream.println("<TD>");

					if (stepResult[column] != null && !stepResult[column].isEmpty()) {
						testCaseResult.append("<a href='#" + testStepNumber + " Screenshot" + "' id='" + testStepNumber+"'>");
						if(stepResult[column].contains(",")){		
							screenshots = stepResult[column].split(",");		
						}
						else{
							screenshots = new String[]{stepResult[column]};
						}
						
						testCaseResult.append("Link");						
											
						testCaseResult.append("</a>"); 
						/*if(i==startIndex){
							previousScreenshot = stepResult[column];
							printScreenshot = true;
						}
						
						else if(i!=startIndex  && !previousScreenshot.contentEquals(stepResult[column])){
							printScreenshot = true;
						}
						else{
							printScreenshot = false;
						}
						
						if(printScreenshot == true){*/
							testCaseScreenshot.append("<span style='font-weight:bold'>Step #"+ testStepNumber +"</span>");
							testCaseScreenshot.append("<div><a href='#"+ testStepNumber + "'>");
							
								for(int count=0; count<screenshots.length; count++){		
									testCaseScreenshot.append("<img src='" + config.getMainSnapshotFolderName() + config.getPathJunction()		
									+ screenshots[count] + "' id='" + testStepNumber + " Screenshot" +"'></img>");		
								}										
																
							testCaseScreenshot.append("<a><br><br></div>");							
							
							//testCaseScreenshot.append("<div><a href='#"+ testCase.getTestCaseName() + "'><img src='" + config.getMainSnapshotFolderName() + config.getPathJunction()
											//+ stepResult[column] + "' id='" + testStepNumber +"'></img><a><br><br></div>");
						//}
					// testResultStream.println("<a
					// href='"+stepResult[column]+"'>"+stepResult[column]+"</a>");
					}
					testCaseResult.append("</TD>");
					// testResultStream.println("</TD></TR>");
					// testResultStream.println("</TD>");
					
				} else {

					testCaseResult.append("<TD>");
					// testResultStream.println("<TD>");

					if (stepResult[column] != null || !stepResult[column].isEmpty()) {

						if(stepResult[column].equalsIgnoreCase("Pass")){
							status = "Yes";
							testCaseResult.append(status);
						}
						else if(stepResult[column].equalsIgnoreCase("Fail")){
							status = "No";
							testCaseResult.append(status);
						}
						else{
						testCaseResult.append(stepResult[column]);
						}
					}
					
					
					// testResultStream.println(stepResult[column]);

					testCaseResult.append("</TD>");
				
					// testResultStream.println("</TD>");
				}
				
				if(column == stepResult.length - 1){
					testCaseResult.append("</TR>");
				}
			}
		}
		// update to include requirement reference
		testCaseResult.append("<TR><TD colspan='" +length+"'");
		// testResultStream.println("<TR><TD colspan='5'");

		if (testResultStatus.equalsIgnoreCase("Pass")) {

			testCaseResult.append("style='background-color:lightgreen;text-align:center'>");
			// testResultStream.println("style='background-color:lightgreen'>");
		} else {

			testCaseResult.append("style='background-color:red;text-align:center'>");
			// testResultStream.println("style='background-color:red'>");
		}

		testCaseResult.append("<a href='#Result'><b align='Center'>" + testCase.getTestCaseNumber() + " Result: " + testResultStatus
				+ "</b></a></TD></TR>");
		testCaseResult.append("</Table>");
		testCaseResult.append("<br><br>");
		
		testCaseScreenshot.append("</div>");
		testCaseScreenshot.append("<br>");
		// testResultStream.println("<b align='Center'><i>"+testCaseName +"
		// Result: "+testResultStatus+"</i></b></TD></TR>");
	}

	public int getTestStep() {
		if (tStep == null)
			return -1;

		return tStep.size();
	}

	public void generateHTMLResultReport() {
		endTime = System.currentTimeMillis();
		header.append(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(endTime))
				+ "</i>.</h2><div style='margin: 7%;text-align:center;'>");
		resultsTable.append("</TBODY></Table><br>");
		testCaseResult.append("</div></Body></HTML>");
		buildNumber.append("<h3 align='center'>Build validated - "+"<i>"+config.getGlobalVariable("BuildVersion")+"</i></h3>");
		
		testResultStream.println(header);
		testResultStream.println(buildNumber);

		testResultStream.println(resultsTable);
		testResultStream.println(testCaseResult);
		testResultStream.println(testCaseScreenshot);
	}

	public void configureTestCaseResult(String testCaseNumber){
		try {
			String fName = String.format("%s/"+testCaseNumber+".html", config.getTestResultFolderPath());
			testCaseResultStream = new PrintWriter(new File(fName));
			startTime = System.currentTimeMillis();
			if (tStep == null)
				tStep = new ArrayList<String[]>();
			testCaseResultHeader = new StringBuffer("");	
			testCaseResultHeader.append("<HTML><HEAD><Title>Test ran at "
					+ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(startTime))
					+ "</Title><style>table{table-layout: fixed;width:100%;word-wrap: break-word;} table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 5px;text-align: left;} img{width: 100%;}</style></HEAD><Body><h2 align='center'>Test execution started at <i>"
					+ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(startTime)) + " and ended at ");	
			testCaseSeparateResult = new StringBuffer("");
			testCaseSeparateScreenshot = new StringBuffer("");
		} catch (FileNotFoundException fNotFound) {
			System.out.println("File not found-----------");
		}
	}
	
	public void generateTestCaseResult(int startIndex, TestCase testCase){
		endTime = System.currentTimeMillis();
		testCaseResultHeader.append(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(endTime))
				+ "</i>.</h2><div style='margin: 7%;text-align:center;'>");
		
		
		testCaseSeparateScreenshot.append("<div class='screenshot' id='" +testCase.getTestCaseName() +"' style='margin: 7%;'><span><b><a href='#"+ testCase.getTestCaseNumber() +"'>"+testCase.getTestCaseName()+"</a></b></span><br><br>");
		//testCaseSeparateScreenshot.append("<div class='screenshot' id='" +testCase.getTestCaseName() +"' style='margin: 7%;'><span><b><a href='#"+ testCase.getTestCaseNumber() +"'>"+ getTestCaseIndex() +". " +testCase.getTestCaseName()+"</a></b></span><br><br>");		

		testCaseSeparateResult.append("<br><br>");
		testCaseSeparateResult.append("<Table id='" + testCase.getTestCaseNumber()
				+ "' border='2' ><TR style='background-color:lightgrey'><TH style='width: 10%;'>Step #</TH><TH>Test Step</TH><TH>Expected Result</TH><TH>Actual Result</TH><TH>Actual = Expected ?</TH>");
				
		testCaseSeparateResult.append("<TH style='width: 15%;'>Screenshot</TH>");
		if(config.isRequirementReference()){
			testCaseSeparateResult.append("<TH style='width: 10%;'>Requirement Reference</TH>");
		}
		
		testCaseSeparateResult.append("</TR>");
		testCaseSeparateResult.append("<TR>");

		int stepNumber = 1;
		String[] stepResult;
		String previousScreenshot="";
		Boolean printScreenshot = false;
		int length = 0;
		String testStepNumber = "";
		
		for (int i = startIndex; i < tStep.size(); i++) {
			String[] screenshots = new String[]{""};
			testStepNumber = "1. " + stepNumber;
			stepResult = tStep.get(i);
			length = stepResult.length + 1;
			testCaseSeparateResult.append("<TD>");
			testCaseSeparateResult.append( "1. " + stepNumber);
			//testCaseSeparateResult.append( getTestCaseIndex()+ ". " + stepNumber);
			testCaseSeparateResult.append("</TD>");
			String status = "";
			for (int column = 0; column < stepResult.length; column++) {				
				if (column == 0) {
					testCaseSeparateResult.append("<TD>");
					testCaseSeparateResult.append(stepResult[column]);
					stepNumber++;
					testCaseSeparateResult.append("</TD>");
				} else if ((config.isRequirementReference()==false && column == stepResult.length - 1) || (config.isRequirementReference() && column == stepResult.length - 2)) {

					testCaseSeparateResult.append("<TD>");

					if (stepResult[column] != null && !stepResult[column].isEmpty()) {
						testCaseSeparateResult.append("<a href='#" + testStepNumber + " Screenshot" + "' id='" + testStepNumber+"'>");
						if(stepResult[column].contains(",")){		
							screenshots = stepResult[column].split(",");		
						}
						else{
							screenshots = new String[]{stepResult[column]};
						}
						
						testCaseSeparateResult.append("Link");						
											
						testCaseSeparateResult.append("</a>"); 
						if(i==startIndex){
							previousScreenshot = stepResult[column];
							printScreenshot = true;
						}
						
						else if(i!=startIndex  && !previousScreenshot.contentEquals(stepResult[column])){
							printScreenshot = true;
						}
						else{
							printScreenshot = false;
						}
						
						if(printScreenshot == true){
							testCaseSeparateScreenshot.append("<span style='font-weight:bold'>Step #"+ testStepNumber +"</span>");
							testCaseSeparateScreenshot.append("<div><a href='#"+ testStepNumber + "'>");
							
								for(int count=0; count<screenshots.length; count++){		
									testCaseSeparateScreenshot.append("<img src='" + config.getMainSnapshotFolderName() + config.getPathJunction()		
									+ screenshots[count] + "' id='" + testStepNumber + " Screenshot" +"'></img>");		
								}					
																
								testCaseSeparateScreenshot.append("<a><br><br></div>");		
						}
					}
					testCaseSeparateResult.append("</TD>");
										
				} else {
					testCaseSeparateResult.append("<TD>");
					if (stepResult[column] != null || !stepResult[column].isEmpty()) {

						if(stepResult[column].equalsIgnoreCase("Pass")){
							status = "Yes";
							testCaseSeparateResult.append(status);
						}
						else if(stepResult[column].equalsIgnoreCase("Fail")){
							status = "No";
							testCaseSeparateResult.append(status);
						}
						else{
							testCaseSeparateResult.append(stepResult[column]);
						}
					}
					testCaseSeparateResult.append("</TD>");
					}
				
				if(column == stepResult.length - 1){
					testCaseSeparateResult.append("</TR>");
				}
			}
		}
		// update to include requirement reference
		testCaseSeparateResult.append("<TR><TD colspan='" +length+"'");


		if (testResultStatus.equalsIgnoreCase("Pass")) {
			testCaseSeparateResult.append("style='background-color:lightgreen;text-align:center'>");
			} else {
				testCaseSeparateResult.append("style='background-color:red;text-align:center'>");
		}
		
		testCaseSeparateResult.append("<div><b align='Center'>" + testCase.getTestCaseNumber() + " Result: " + testResultStatus
				+ "</b></div></TD></TR>");

		//testCaseSeparateResult.append("<a href='#Result'><b align='Center'>" + testCase.getTestCaseNumber() + " Result: " + testResultStatus
				//+ "</b></a></TD></TR>");
		testCaseSeparateResult.append("</Table>");
		testCaseSeparateResult.append("<br><br>");
		
		testCaseSeparateScreenshot.append("</div>");
		testCaseSeparateScreenshot.append("<br>");
		
		testCaseSeparateResult.append("</div></Body></HTML>");

		testCaseResultStream.println(testCaseResultHeader);

		testCaseResultStream.println(testCaseSeparateResult);
		testCaseResultStream.println(testCaseSeparateScreenshot);
		
		testCaseResultStream.close();
	}

}
