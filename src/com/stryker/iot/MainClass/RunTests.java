package com.stryker.iot.MainClass;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.appirio.automation.configuration.Configuration;
import com.appirio.automation.configuration.TestCase;
import com.appirio.automation.configuration.TestStep;
import com.appirio.automation.exception.TestScriptException;
import com.appirio.automation.result.TestResult;
import com.appirio.automation.utility.Utility;
import com.stryker.iot.testCommands.IOTTestCommands;
import com.stryker.iot.testResult.IOTTestResult;

import jxl.read.biff.BiffException;

public class RunTests {
	public static final Logger logger = Logger.getLogger(RunTests.class.getName());
	private static Configuration config;
	private static IOTTestCommands iotTestCommands;
	private static String executionStatus = "Pass";
	private int testCaseStartIndex;
	private TestCase executingTestCase;
	private HashMap<Integer, TestStep> executedStepsMap;

	public Configuration getConfig() {
		return config;
	}

	public static void setConfig(Configuration config) {
		RunTests.config = config;
	}

	public static void main(String[] args) throws BiffException, IOException {
		//System.setSecurityManager(null);
		iotTestCommands = new IOTTestCommands();
		IOTTestResult testResult = new IOTTestResult();
		RunTests rt = null;
		try {
			if (args.length < 1) {
				System.err.println("Configure file is not supplied as argument. "
						+ "Please provide the path for the config file.");
				return;
			}
			else if (args.length > 1){
				System.err.println("Please provide only config file as the arguments and remove unnecessary files.");
				return;
			}
			else{
				String configureFile = args[0];
				if (configureFile.trim().length() == 0) {
					logger.error("config file name not appropriate, please provide proper file name.");
					return;
				}						
				rt = new RunTests();
				config = new Configuration(configureFile, iotTestCommands, logger,(TestResult)testResult);
				testResult.configureTestResult(config);
				iotTestCommands.setStrykerIOTConfig(config);
				rt.runAllTestCases();
			}	
		} catch (TestScriptException e) {
			logger.error(e.getMessage());
			// e.printStackTrace();
		} catch(Exception e){
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if(config!=null){
				if (config.getTestResult() != null && (config.getTestResult().getTestStep() > 0)) {
					config.getTestResult().closeTestResult();
				}
			}
		}

	}

	/**
	 * run all test cases in spread sheet list.
	 * 
	 * @throws DriveScriptException
	 *             - if there is any problem running test.
	 */
	public void runAllTestCases() throws TestScriptException {
		if (config.getSpreadSheetList() == null) {
			throw new TestScriptException("spread sheet list is null. Please run configure first.");
		}
		try {
			for (int i = 0; i < config.getSpreadSheetList().length; i++) {
				if (i == 0) {
					testCaseStartIndex = 0;
				} else {
					testCaseStartIndex = IOTTestResult.tStep.size();
				}
				try {
					RunTests.executionStatus = "Pass";
					config.getLogger().info("");
					config.getLogger().info("------------------------------Started execution for the test case "+config.getSpreadSheetList()[i] +"------------------------------");
					config.getLogger().info("");
					runTestCase(config.getSpreadSheetList()[i]);
					// testResult.testResultStatus = "Pass";
				} catch (Exception e) {
					RunTests.executionStatus = "Fail";
					// testResult.testResultStatus = "Fail";
										
					if(e.getMessage()!=null){
						logger.info(e.getMessage());
					}
					else{
						e.printStackTrace();
					}
					iotTestCommands.closeBrowser("N");
					continue;
				} finally {
					config.getTestResult().setTestResulStatus(RunTests.executionStatus);
					if (config.getTestResult().getTestResulStatus().equalsIgnoreCase("Fail")) {
						logger.info("Test case for: " + config.getSpreadSheetList()[i] + " has failed.");
					}
					if (config.getTestResult().getTestResulStatus().equalsIgnoreCase("Pass")) {
						logger.info("Test case for: " + config.getSpreadSheetList()[i] + " has passed.");
					}
					if (config.getTestResult() != null && config.getTestResult().getTestStep() > 0) {
						config.getTestResult().generateTestResult(testCaseStartIndex, executingTestCase);
						config.getTestResult().generateTestCaseResult(testCaseStartIndex, executingTestCase);
					}
				}
			}
		} finally {
			config.getTestResult().generateHTMLResultReport();
		}
	}

	public void runTestCase(String filename) throws TestScriptException, BiffException, IOException {
		config.setTestStepInstance(null);
		Object object = null;
		int nextTestCommandNumber = 1;
		boolean stepToBeExecuted = true;
		Utility util = new Utility(config);
		String testCaseName ="";
		boolean exception = false;
		List<String> exceptionResult = new ArrayList<String>();
		String[] exceptionTestResult = null;
		// try{
		HashMap<Integer, TestStep> testCase = TestStep.readData(filename, "Sheet1");
		Configuration.setTestCaseLocalInputData(filename);
		testCaseName = filename.substring("resources/".length(), filename.indexOf("."));
		if(testCaseName.lastIndexOf("/")!=-1){
		config.setTestCaseNumber(testCaseName.substring(testCaseName.lastIndexOf("/") + 1));
		executingTestCase = Configuration.getTestCases().get(config.getTestCaseNumber());
		}
		else{
			config.setTestCaseNumber(testCaseName);
			executingTestCase = Configuration.getTestCases().get(config.getTestCaseNumber());
		}
		executedStepsMap = new HashMap<Integer, TestStep>();
		
		config.setExecutedStepsMap(executedStepsMap);
		config.getTestResult().configureTestCaseResult(config.getTestCaseNumber());
		TestStep testStep = null;
		while (stepToBeExecuted) {
			String[] testStepInstanceOld = config.getTestStepInstance();
			testStep = testCase.get(nextTestCommandNumber);
			String command = testStep.getCommand().toUpperCase();
			command = command.trim();
			if (config.getMethodsMap().get(command) == null) {
				iotTestCommands.closeBrowser("N");
				
				exceptionResult.add("Verify that the keyword " + command + " is present in mapRuleFile " + config.getMethodMappingFile());
				exceptionResult.add("Keyword for the" + command + " should be found in mapRuleFile " + config.getMethodMappingFile());
				exceptionResult.add("Could not find keyword " + command + " in mapRuleFile " + config.getMethodMappingFile());
				exceptionResult.add("Fail");
				exceptionResult.add("");
				if(config.isRequirementReference()){
					exceptionResult.add("");
					exceptionTestResult = exceptionResult.toArray(new String[exceptionResult.size()]);
				}
				else {
					exceptionTestResult = exceptionResult.toArray(new String[exceptionResult.size()]);
				}
				IOTTestResult.addItemToReport(exceptionTestResult);
				throw new TestScriptException(
						"Could not find keyword " + command + " in mapRuleFile " + config.getMethodMappingFile());
			}
			Class<?>[] argsTypes = config.getMethodsMap().get(command).getParameterTypes();
			Object[] args = new Object[config.getMethodsMap().get(command).getGenericParameterTypes().length];
			
			/*if(args.length!=testStep.getArgumentList().size()){
				throw new TestScriptException("Argument list for the command " +command+" is not correct, please review it and try again.");
			}
			*/
			// fill args
			try{
			for (int argIdx = 0; argIdx < args.length; argIdx++) {
				String argValue = testStep.getArgumentList().get(argIdx);
				argValue = util.replaceHashTag(argValue);

				if (argValue.startsWith("#")) {
					if (argValue.contains(",")) {
						String[] tempArray = argValue.split(",");
						argValue = "";
						String temp = "";
						for (int i = 0; i < tempArray.length; i++) {
							temp = config.getGlobalVariable(tempArray[i].substring(1));
							if (i == 0) {
								argValue = temp;
							} else {
								argValue = argValue + "," + temp;
							}
						}

					} else {
						argValue = config.getGlobalVariable(argValue.substring(1));
					}
				}
				try {
					Constructor<?> cons = argsTypes[argIdx].getConstructor(String.class);
					if (argsTypes[argIdx] == Integer.class) {
						DecimalFormat formatter = new DecimalFormat("0");
						// convert number.
						argValue = formatter.format(Double.valueOf(argValue)).toString();
					}
					args[argIdx] = cons.newInstance(argValue);
				} catch (NoSuchMethodException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException(
							"No such construct method: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (SecurityException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException("Security violation. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (InstantiationException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException("New instance error. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (IllegalAccessException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException("Illegal Access. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (IllegalArgumentException e) {
					// salesforceCommands.closeBrowser();

					throw new TestScriptException("Illegal Argument. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (InvocationTargetException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException("Invocation Target error. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				}
			}

			try {
				int derivedNextCommandNumber = 0;
				object = config.getMethodsMap().get(command).invoke(iotTestCommands, args);

				derivedNextCommandNumber = iotTestCommands.getNextTestCommandRow();
				if (derivedNextCommandNumber != -1) {
					iotTestCommands.resetNextTestCommandRow();
					nextTestCommandNumber = derivedNextCommandNumber;
				} else {
					// this is to make sure that test result is generated for
					// first command
					if (nextTestCommandNumber == 1) {
						if (!object.getClass().equals(Boolean.class)) {
							config.setTestStepInstance((String[]) object);
						if(config.isRequirementReference()){
							config.getTestStepInstance()[5] = testStep.getReqReference();
						}
						IOTTestResult.addItemToReport(config.getTestStepInstance());
						
						}						
					}
					
					nextTestCommandNumber++;
				}
				if (command.equalsIgnoreCase("closeBrowser")) {
					stepToBeExecuted = false;
				}
				testStep.setArguments(args);

				config.getExecutedStepsMap().put(testStep.getTestStepNumber(), testStep);
				logger.info(command + " args= " + Arrays.toString(args));

			} catch (IllegalAccessException e) {
				throw new TestScriptException("Illegal Access error. COMMAND = " + command + " method = "
						+ config.getMethodsMap().get(command).getName() + e.getMessage(), e);

			} catch (IllegalArgumentException e) {
				String exceptionMsg = "Illegal Argument error. COMMAND = " + command + " method = "
						+ config.getMethodsMap().get(command).getName();
				exceptionMsg += "argument list: ";
				for (int j = 0; j < args.length; j++) {
					exceptionMsg += args[j].toString() + ",";
				}
				exceptionMsg += e.getMessage();
				throw new TestScriptException(exceptionMsg, e);
			} catch (InvocationTargetException e) {
				// For continuing execution on next command
				object = TestScriptException.object;
				if (!testStep.getToBeAborted().equalsIgnoreCase("N")) {
					throw new TestScriptException(
							"Invocation Target error.An exception thrown by an invoked method. COMMAND = " + command
									+ " method = " + config.getMethodsMap().get(command).getName() + " "
									+ e.getTargetException().getMessage());
				}
				RunTests.executionStatus = "Fail";
				nextTestCommandNumber++;
			} catch (Exception e) {
				throw new TestScriptException("Following exception occured " + e.getMessage());
			} finally {

				if (!object.getClass().equals(Boolean.class)) {
					config.setTestStepInstance((String[]) object);
				}

				if (testStepInstanceOld != null && !testStepInstanceOld.equals(config.getTestStepInstance())) {
					if(config.isRequirementReference()){
						config.getTestStepInstance()[5] = testStep.getReqReference();
					}
					IOTTestResult.addItemToReport(config.getTestStepInstance());
				}
			}
		} catch (Exception e) {
			exception = true;
			throw new TestScriptException("Following exception occured " + e.getMessage());
		} finally{
			if(exception){
				exceptionResult.add("Verify that the command "+command + " is executed");
				exceptionResult.add(command + " should be executed");
				exceptionResult.add(command + " cannot be executed because of an exception. Please refer logs/info.log for more information");
				exceptionResult.add("Fail");
				exceptionResult.add("");
				if(config.isRequirementReference()){
					exceptionResult.add("");
					exceptionTestResult = exceptionResult.toArray(new String[exceptionResult.size()]);
				}
				else {
					exceptionTestResult = exceptionResult.toArray(new String[exceptionResult.size()]);
				}
				IOTTestResult.addItemToReport(exceptionTestResult);
			}
		}	
	  }
	}	
	
	
}